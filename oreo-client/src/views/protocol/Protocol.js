// VENDORS
import React, { Component } from "react";
import { connect } from "react-redux";
import { Container, Card, Form, InputGroup, Button } from "react-bootstrap";

// SERVICES
import { CHANGE_PROTOCOL_ACTION, fetchProtocols, saveProtocol } from "../../redux/protocol";

// COMPONENTS
import ProtocolAdvanced from "./ProtocolAdvanced";

const mapStateToProps = state => ({
    protocols: state.protocol.protocols
});

const mapDispatchToProps = dispatch => ({
    fetchProtocols: () => dispatch(fetchProtocols()),
    setIp: (key, value, index) => dispatch({
        type: CHANGE_PROTOCOL_ACTION,
        payload: {
            key,
            value,
            index
        }
    }),
    saveProtocol: (body, id) => dispatch(saveProtocol(body, id))
});

/**
 * Composant permettant d'afficher la liste des protocoles et leur configuration
 */
class Protocol extends Component {
    componentDidMount() {
        this.props.fetchProtocols();
    }

    handleChange(protocol, index) {
        const key = protocol.info.ip ? "ip" : "uri";
        return e => this.props.setIp(key, e.target.value, index);
    }

    handleSubmit(protocol) {
        return e => {
            e.preventDefault();
            this.props.saveProtocol(protocol.info, protocol.id);
        };
    }

    render() {
        return (
            <Container className="my-4">
                <h2>Gérer les configurations protocoles</h2>
                {this.props.protocols
                    .map((protocol, i) =>
                    <Card key={i} className="my-4">
                        <Card.Header>{"Configuration " + protocol.id.toUpperCase()}</Card.Header>
                        <Card.Body>
                            <Form>
                                <Form.Group controlId="exampleForm.ControlTextarea1">
                                    <InputGroup>
                                        <InputGroup.Prepend>
                                            <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
                                        </InputGroup.Prepend>
                                        <Form.Control
                                            onChange={this.handleChange(protocol, i)}
                                            aria-describedby="basic-addon1"
                                            value={protocol.info.ip ? protocol.info.ip : protocol.info.uri}
                                            required
                                        />
                                    </InputGroup>
                                    <small>Receiver IP address or URI</small>
                                    <ProtocolAdvanced index={i} protocol={protocol} />
                                    <Button
                                        id="protocolIDValidation1"
                                        variant="outline-primary"
                                        type="submit"
                                        onClick={this.handleSubmit(protocol)}
                                    >
                                        Confirmer
                                    </Button>
                                </Form.Group>
                            </Form>
                        </Card.Body>
                    </Card>
                )}
            </Container>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(Protocol);
