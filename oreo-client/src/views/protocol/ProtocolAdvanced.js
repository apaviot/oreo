// VENDORS
import React, { Component } from "react";
import { connect } from "react-redux";
import { Card, Form, FormControl, InputGroup } from "react-bootstrap";

// SERVICES
import { CHANGE_PROTOCOL_ACTION } from "../../redux/protocol";

const mapDispatchToProps = dispatch => ({
    changeProtocol: params => dispatch({
        type: CHANGE_PROTOCOL_ACTION,
        payload: params
    })
});

/**
 * Composant comportant l'ensemble des parametres liés à un protocole
 */
class ProtocolAdvanced extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapse: false,
            className: ""
        };
    }

    buildFields() {
        return Object.keys(this.props.protocol.info)
            .filter(key => this.props.protocol.info[key] !== undefined)
            .map(key => ({
                key,
                value: this.props.protocol.info[key],
                index: this.props.index
            }));
    }

    toggle() {
        let className = "is-visible";
        if (this.state.className === "is-visible") {
            className = "";
        }
        this.setState({ className });
    }

    handleChange(protocol) {
        return e => this.props.changeProtocol({
            ...protocol,
            value: e.target.value
        });
    }

    render() {
        const fields = this.buildFields();
        return (
            <div>
                {fields.length === 0 ? <div /> :
                    <div>
                        <a href="#" onClick={this.toggle.bind(this)}>
                            <small>Paramétrage avancé</small>
                        </a>
                        <Card className={"my-1 toggle-content " + this.state.className}>
                            <Card.Body>
                                {fields.map((field, i) =>
                                    <Form.Group key={i} controlId="exampleForm.ControlTextarea1">
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text id="basic-addon1">{field.key}</InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <FormControl
                                                onChange={this.handleChange(field)}
                                                aria-describedby="basic-addon1"
                                                value={field.value}
                                            />
                                        </InputGroup>
                                    </Form.Group>
                                )}
                            </Card.Body>
                        </Card>
                    </div>
                }
            </div>
        );
    }
}

export default connect(() => ({}), mapDispatchToProps)(ProtocolAdvanced);
