// VENDORS
import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal, Button, Card, Form, InputGroup, ProgressBar } from "react-bootstrap";
import * as superAgent from "superagent";
import { toast } from "react-toastify";
import { fetchExperiments } from "../../redux/experiment";

const mapStateToProps = state => ({
    benches: state.bench.benches,
    campaigns: state.campaign.campaigns,
    url: state.config.apiURL
});

const mapDispatchToProps = dispatch => ({
    fetchExperiments: opts => dispatch(fetchExperiments(opts)),
});

/**
 * Modale permettant l'insertion en base d'un nouvel essai à l'aide d'un fichier .csv
 */
class InsertModal extends Component {
    constructor(props) {
        super(props);

        // Définition des states
        this.state = {
            loadingClassName: "",
            parseClassName: "",
            formClassName: "is-visible",

            benchClassName: "is-visible",
            benchChoice: "",
            benchName: "",
            benchRequired: true,

            campaignClassName: "is-visible",
            campaignChoice: "",
            campaignId: "",
            campaignRequired: true,

            experimentName: "",
            experimentReference: "",

            SampleCSVFile: null,
            alarmCSVFile: null,
            progress : 0,

            fileNameExperiment : "Choisir un fichier",
            fileNameAlarm : "Choisir un fichier"
        };

        // Binding des handlers
        this.sampleCSVFileChangedHandler = this.sampleCSVFileChangedHandler.bind(this);
        this.alarmCSVFileChangedHandler = this.alarmCSVFileChangedHandler.bind(this);
        this.handleUserInput = this.handleUserInput.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        event.stopPropagation();
        this.setState({ validated: true });

        const form = event.currentTarget;
        if (form.checkValidity() === true) {
            this.setState({
                formClassName: "",
                parseClassName: "",
                loadingClassName: "is-visible",
                benchClassName: "is-visible",
                campaignClassName: "is-visible"
            });

            const formData = this.getFormData(event);

            let sAgent = superAgent.agent();
            sAgent
                .post(this.props.url + "/import")
                .send(formData)
                .on("progress", function (e) {
                    this.setState({ progress: e.percent });

                    if (e.percent === 100) {
                        this.setState({ parseClassName: "is-visible", loadingClassName: "" });
                    }
                }.bind(this))
                .then(function (r) {
                    this.props.onHide();
                    toast.success("Import terminé");
                    this.props.fetchExperiments();
                    this.setState({ formClassName: "is-visible", progress: 0, loadingClassName: "", parseClassName: "", validated: false });
                }.bind(this), function (e) {
                    this.setState({ formClassName: "is-visible", progress: 0, loadingClassName: "", parseClassName: "", validated: false });
                    toast.error(e.message);
                }.bind(this));
        }
    }

    getFormData(event) {
        const formData = new FormData();
        formData.append("benchChoice", event.target.benchChoice.value === "newBench" ? "0" : event.target.benchChoice.value);
        formData.append("benchName", event.target.benchName.value);
        formData.append("campaignChoice", event.target.campaignChoice.value === "newCampaign" ? "0" : event.target.campaignChoice.value);
        formData.append("campaignId", event.target.campaignId.value);
        formData.append("experimentName", event.target.experimentName.value);
        formData.append("experimentReference", event.target.experimentReference.value);
        formData.append("sampleFile", this.state.SampleCSVFile);
        formData.append("alarmFile", this.state.alarmCSVFile);
        return formData;
    }

    handleUserInput(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    sampleCSVFileChangedHandler(event) {
        if (event.target.files[0] === undefined) {
            this.setState({ fileNameExperiment: "Choisir un fichier" });
        } else {
            this.setState({
                SampleCSVFile: event.target.files[0],
                fileNameExperiment: event.target.files[0].name
            });
        }
    };

    onBenchChange(e) {
        if (e.target.value === "newBench") {
            this.setState({ benchClassName: "is-visible", benchRequired: true });
        } else {
            this.setState({ benchClassName: "", benchRequired: false });
        }
    }

    onCampaignChange(e) {
        if (e.target.value === "newCampaign") {
            this.setState({ campaignClassName: "is-visible", campaignRequired: true });
        } else {
            this.setState({ campaignClassName: "", campaignRequired: false });
        }
    }

    alarmCSVFileChangedHandler(event) {
        if (event.target.files[0] === undefined) {
            this.setState({ fileNameAlarm:"Choisir un fichier" });
        } else {
            this.setState({ alarmCSVFile: event.target.files[0], fileNameAlarm: event.target.files[0].name });
        }
    };

    onEnter() {
        this.setState({
            campaignClassName: "is-visible",
            campaignRequired: true,
            benchClassName: "is-visible",
            benchRequired: true,
            fileNameExperiment: "Choisir un fichier",
            fileNameAlarm: "Choisir un fichier"
        });
    }

    render() {
        const { validated } = this.state;

        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="import-file-modal-title-vcenter"
                centered
                onEnter={this.onEnter.bind(this)}
            >
                <Form
                    noValidate
                    validated={validated}
                    content-type="multipart/form-data"
                    onSubmit={e => this.handleSubmit(e)}
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="import-file-modal-title-vcenter">
                            Importer un essai
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>


                        <Form.Group id="formImport" className={"toggle-content " + this.state.formClassName}>
                            {/*****************************************************************
                             *
                             *                           BANC
                             *
                             *****************************************************************/}
                            <Card className="mb-4">
                                <Card.Header>
                                    Banc
                                </Card.Header>
                                <Card.Body>
                                    {/* Sélection du banc */}
                                    <Form.Control as="select" name="benchChoice" className="mb-2" onChange={this.onBenchChange.bind(this)}>
                                        <option value="newBench">Nouveau Banc</option>
                                        {this.props.benches.map((e, i) => (
                                            <option key={i} value={e.id}>{e.name}</option>
                                        ))}
                                    </Form.Control>

                                    {/* Si le banc n'existe pas, il est crée par le champs suivant */}
                                    <InputGroup className={"toggle-content " + this.state.benchClassName}>
                                        {/* Nom du banc*/}
                                        <Form.Control
                                            id="benchTextName"
                                            name="benchName"
                                            type="text"
                                            placeholder="Nom du nouveau banc*"
                                            required={this.state.benchRequired}
                                            onChange={this.handleUserInput}
                                        />
                                    </InputGroup>
                                </Card.Body>
                            </Card>

                            {/*****************************************************************
                             *
                             *                           CAMPAGNE
                             *
                             *****************************************************************/}
                            <Card className="mb-4">
                                <Card.Header>
                                    Campagne
                                </Card.Header>
                                <Card.Body>

                                    {/* Selection d'une campagne */}
                                    <Form.Control as="select" name="campaignChoice" className="mb-2" onChange={this.onCampaignChange.bind(this)}>
                                        <option value="newCampaign" >Nouvelle Campagne</option>
                                        {this.props.campaigns.map((e, i) => (
                                            <option key={i} value={e.id}>{e.id12c}</option>
                                        ))}
                                    </Form.Control>

                                    {/* Si la campagne n'existe pas, elle est créee par les champs suivants */}
                                    <InputGroup className={"toggle-content " + this.state.campaignClassName}>
                                        {/* Id12C de la campagne */}
                                        <Form.Control
                                            id="campaignTextId"
                                            name="campaignId"
                                            type="text"
                                            required={this.state.campaignRequired}
                                            maxLength="12"
                                            placeholder="Id12C*"
                                            onChange={this.handleUserInput}
                                        />
                                    </InputGroup>
                                </Card.Body>
                            </Card>
                            {/*****************************************************************
                             *
                             *                           ESSAI
                             *
                             *****************************************************************/}
                            <Card>
                                <Card.Header>
                                    Essai
                                </Card.Header>
                                <Card.Body>
                                    <InputGroup>
                                        {/* Nom de l'essai */}
                                        <Form.Control
                                            id="experimentTextName"
                                            name="experimentName"
                                            type="text"
                                            placeholder="Nom*"
                                            required
                                            onChange={this.handleUserInput}
                                            //required
                                        />
                                        {/* Référence de l'essai */}
                                        <Form.Control
                                            id="experimentTextReference"
                                            name="experimentReference"
                                            type="text"
                                            placeholder="Référence*"
                                            aria-describedby="inputGroupPrepend"
                                            required
                                        />

                                        {/* Fichier CSV des mesures et échantillons */}
                                        <div className="input-group my-3">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text" id="inputGroupFileAddon01">Essai*</span>
                                            </div>
                                            <div className="custom-file">
                                                <input id="experimentFile" type="file" onChange={this.sampleCSVFileChangedHandler} className="custom-file-input" required aria-describedby="inputMesure" />
                                                <label className="custom-file-label" htmlFor="inputMesure">{this.state.fileNameExperiment}</label>
                                            </div>
                                        </div>

                                        <div className="input-group my-1">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text" id="inputGroupFileAddon01">Alarme</span>
                                            </div>
                                            <div className="custom-file">
                                                <input id="alarmFile" type="file" onChange={this.alarmCSVFileChangedHandler} className="custom-file-input"  aria-describedby="inputAlarme" />
                                                <label className="custom-file-label" htmlFor="inputAlarme">{this.state.fileNameAlarm}</label>
                                            </div>
                                        </div>


                                    </InputGroup>
                                </Card.Body>
                            </Card>

                        </Form.Group>

                        <Card id="chargement" className={"toggle-content " + this.state.loadingClassName}>
                            <Card.Header>
                                Chargement en cours
                            </Card.Header>
                            <Card.Body>
                                <ProgressBar animated now={this.state.progress} label={Math.round(this.state.progress) + "%"} />
                            </Card.Body>
                        </Card>

                        <Card id="chargement" className={"toggle-content " + this.state.parseClassName}>
                            <Card.Header>
                                Parsing des fichiers et insertion dans la base de données.
                            </Card.Header>
                            <Card.Body className="text-center">
                                <img width="100px" src="/loading.gif" />
                            </Card.Body>
                        </Card>


                        <small className={"toggle-content " + this.state.formClassName}>* champs obligatoires</small>

                    </Modal.Body>
                    <Modal.Footer className={"toggle-content " + this.state.formClassName}>
                        <Button id="buttonImport" type="submit">Importer</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(InsertModal);
