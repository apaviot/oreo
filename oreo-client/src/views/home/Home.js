// VENDORS
import React, { Component } from "react";
import { Container, Button, Row, Col } from "react-bootstrap";
import { connect } from "react-redux";

// SERVICES
import { fetchExperiments, searchExperiments } from "../../redux/experiment";

// COMPONENTS
import AppPagination from "../common/AppPagination";
import Search from "./Search";
import InsertModal from "./InsertModal";
import Result from "./Result";

const mapStateToProps = state => ({
    pages: state.experiment.page,
});

const mapDispatchToProps = dispatch => ({
    fetchExperiments: opts => dispatch(fetchExperiments(opts)),
    fetchExperimentsSearch: opts => dispatch(searchExperiments(opts))
});

/**
 * Composant qui affiche la liste des essais presents en base
 */
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalShow: false,
            opts: null
        };
    }

    componentDidMount() {
        this.props.fetchExperiments();
    }

    modalClose() {
        this.setState({
            modalShow: false
        });
    }

    modalOpen() {
        this.setState({
            modalShow: true
        });
    }

    onSearch(opts) {
        this.setState({ opts });
        this.props.fetchExperimentsSearch({ ...opts });
    }

    onPage(page) {
        this.props.fetchExperimentsSearch({ page, ...this.state.opts });
    }

    render() {
        return (
            <Container className="my-4">
                <Row>
                    <Col md={10}>
                        <h2>Liste des essais</h2>
                    </Col>
                    <Col md={2}>
                        <Button
                            variant="outline-secondary"
                            className="float-right"
                            onClick={this.modalOpen.bind(this)}
                        >
                            Importer un essai
                        </Button>
                    </Col>
                </Row>

                <InsertModal
                    show={this.state.modalShow}
                    onHide={this.modalClose.bind(this)}
                />

                <Search onSearch={this.onSearch.bind(this)} />
                <Result />

                <AppPagination pages={this.props.pages} onClick={this.onPage.bind(this)} />
            </Container>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(Home);
