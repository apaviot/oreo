// VENDORS
import React, { Component } from "react";
import { connect } from "react-redux";
import { FormControl, Button, InputGroup, Card, Form } from "react-bootstrap";

// SERVICES
import { fetchBenches } from "../../redux/bench";
import { fetchCampaigns } from "../../redux/campaign";
import { parseDate } from "../../services/date.js";

const mapStateToProps = state => ({
    benches : state.bench.benches,
    campaigns : state.campaign.campaigns
});

const mapDispatchToProps = dispatch => ({
    fetchBenches: () => dispatch(fetchBenches()),
    fetchCampaigns: () => dispatch(fetchCampaigns())
});

/**
 * Composant permettant d'affiner la recherche d'un essai
 */
class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            collapse: false,
            className: "",
            label: "Recherche",
            validated: false,
            bdd: null,
            bench: null,
            campaign: null,
            date: null,
            searchBar: null
        };
    }

    componentDidMount() {
        this.props.fetchBenches();
        this.props.fetchCampaigns();
    }

    toggle() {
        let state = {
            className: "is-visible",
            label: "Recherche avancée"
        };

        if (this.state.className === "is-visible") {
            state = {
                className: "",
                label: "Recherche"
            };
        }

        this.setState(state);
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({
            validated: true,
            className: "",
            label: "Recherche"
        });
        
        let opts = {
            campaignId : this.state.campaign,
            database : this.state.bdd,
            benchId: this.state.bench,
            lastUse: (this.state.date === null) ? null : parseDate(new Date(this.state.date)),
            name : this.state.searchBar
        };

        this.props.onSearch(opts);
    }

    onChange(key) {
        return e => this.setState({
            [key]: e.target.value
        });
    }

    render() {
        return (
            <Form
                className="my-2"
                noValidate
                validated={this.state.validated}
                onSubmit={e => this.handleSubmit(e)}
            >
                <InputGroup>
                    <FormControl placeholder="..." type="text" onChange={this.onChange("searchBar").bind(this)} />
                    <InputGroup.Append>
                        <Button type="submit">{this.state.label}</Button>
                    </InputGroup.Append>
                </InputGroup>

                <p>
                    <a href="#" onClick={this.toggle.bind(this)} className="float-right">
                    <small>Recherche avancée</small>
                    </a>
                </p>

                <Card className={"my-4 toggle-content " + this.state.className}>
                    <Card.Body>
                        <h3>Filtres</h3>

                        <Form.Group onChange={this.onChange("campaign").bind(this)}>
                            <Form.Label>Choisir une campagne :</Form.Label>
                            <Form.Control as="select" defaultValue="">
                                <option value="">N/A</option>
                                {this.props.campaigns.map((e, i) => (
                                    <option key={i} value={e.id}>{e.id12c}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>

                        <Form.Group onChange={this.onChange("bench").bind(this)}>
                            <Form.Label>Choisir un banc :</Form.Label>
                            <Form.Control as="select" defaultValue="">
                                <option value="" >N/A</option>
                                {this.props.benches.map((e, i) => (
                                    <option key={i} value={e.id}>{e.name}</option>
                                ))}
                            </Form.Control>
                        </Form.Group>

                        <Form.Group onChange={this.onChange("date").bind(this)}>
                            <Form.Label>Choisir une date</Form.Label>
                            <Form.Control type="date" />
                        </Form.Group>

                        <Form.Group onChange={this.onChange("bdd").bind(this)}>
                            <Form.Label>Choisir une base de donnée :</Form.Label>
                            <Form.Control as="select" defaultValue="">
                                <option value="" disabled>N/A</option>
                                <option value="local">Locale</option>
                                <option value="remote">Globale</option>
                            </Form.Control>
                        </Form.Group>
                    </Card.Body>
                </Card>
            </Form>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(Search);
