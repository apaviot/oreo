// VENDORS
import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";

/**
 * Modale de confirmation de suppression
 */
class DeleteModal extends Component {
    onValidateClick() {
        this.props.onHide();
        this.props.onDelete();
    }

    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.onHide} >
                <Modal.Header closeButton>
                    <Modal.Title>Suppression</Modal.Title>
                </Modal.Header>
                <Modal.Body>Souhaitez-vous vraiment supprimer ?</Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary" onClick={() => this.onValidateClick()}>
                        Oui
                    </Button>
                    <Button variant="primary" onClick={this.props.onHide}>
                        Non
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default DeleteModal;
