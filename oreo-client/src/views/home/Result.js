// VENDORS
import React, { Component } from "react";
import { connect } from "react-redux";
import { Badge, Table } from "react-bootstrap";
import { FaTrashAlt } from "react-icons/fa";

// SERVICES
import { deleteExperiment } from "../../redux/experiment";

// COMPONENTS
import DeleteModal from "./DeleteModal";

const mapStateToProps = state => ({
    experiments: state.experiment.experiments
});

const mapDispatchToProps = dispatch => ({
    onClickDelete: id => dispatch(deleteExperiment(id))
});

/**
 * Composant contenant le liste des essais résultant d'une recherche
 */
class Result extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deleteModalShow: false
        };
    }

    showModal(id) {
        this.setState({ deleteModalShow: true, id });
    }

    render() {
        return (
            <div className="my-2">
                <Table className="mt-3" responsive>
                    <thead>
                        <tr>
                            <th scope="col">Nom</th>
                            <th scope="col">BDD</th>
                            <th scope="col">Date</th>
                            <th scope="col">Référence</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.experiments.map((e, i) => (
                            <tr key={i}>
                                <td className="experiments"><a href={"/experiment/" + e.id}>{e.name}</a></td>
                                {e.isLocal
                                    ?
                                        <td><Badge variant="success">Locale</Badge></td>
                                    :
                                        <td><Badge variant="info">Globale</Badge></td>
                                }
                                <td className="resultDate">{e.begin ? e.begin.toLocaleString() : ""}</td>
                                <td>{e.reference}</td>
                                <td><a href="#" onClick={this.showModal.bind(this, e.id)} ><FaTrashAlt className="align-text-top float-right" /></a></td>
                            </tr>
                        ))}
                    </tbody>
                </Table>

                <DeleteModal
                    show={this.state.deleteModalShow}
                    onHide={() => this.setState({ deleteModalShow: false })}
                    onDelete={() => this.props.onClickDelete(this.state.id)}
                />
            </div>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(Result);
