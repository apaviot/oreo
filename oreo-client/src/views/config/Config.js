// VENDORS
import React,  { Component } from "react";
import { connect } from "react-redux";
import { Container, Card, Form, Button } from "react-bootstrap";

// SERVICES
import { UPDATE_API_URL } from "../../redux/config";

const mapStateToProps = state => ({
    apiURL: state.config.apiURL
});

const mapDispatchToProps = dispatch => ({
    updateAPIURL: apiURL => dispatch({
        type: UPDATE_API_URL,
        payload: apiURL
    })
});

/**
 * Composant qui permet de parametrer l'URL de l'API
 */
class Config extends Component {
    constructor(props) {
        super(props);
        this.state = {
            apiURL: props.apiURL
        };
    }

    handleChange(e) {
        this.setState({
            apiURL: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.updateAPIURL(this.state.apiURL);
    }

    render() {
        return (
            <Container className="my-4">
                <h2>Gérer la config</h2>
                <Card className="my-4">
                    <Card.Header>Config API</Card.Header>
                    <Card.Body>
                        <Form onSubmit={this.handleSubmit.bind(this)}>
                            <Form.Group>
                                <Form.Label>API URL</Form.Label>
                                <Form.Control onChange={this.handleChange.bind(this)} value={this.state.apiURL} required />
                                <Button variant="outline-primary my-2" type="submit">
                                    Confirmer
                                </Button>
                            </Form.Group>
                        </Form>
                    </Card.Body>
                </Card>
            </Container>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(Config);
