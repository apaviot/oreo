// VENDORS
import React, { Component } from "react";
import { Pagination } from "react-bootstrap";

/**
 * Systeme de pagination qui permet de naviguer entre differentes pages
 */
class AppPagination extends Component {
    createPagination() {
        const pages = [];

        let start = 0;
        if (this.props.pages.number === 0) {
            start = this.props.pages.number;
        } else if (this.props.pages.number === 1) {
            start = this.props.pages.number - 1 ;
        } else {
            start = this.props.pages.number - 2;
        }

        let end =  0;
        if (this.props.pages.totalPages === 0) {
            end = 0;
        } else if (this.props.pages.number + 1 === this.props.pages.totalPages) {
            end = this.props.pages.number;
        } else if (this.props.pages.number + 2 === this.props.pages.totalPages) {
            end = this.props.pages.number + 1;
        } else {
            end = this.props.pages.number + 2;
        }

        for (let i = start; i <= end; i++) {
            pages.push(<Pagination.Item active={i === this.props.pages.number} key={i} onClick={this.props.onClick.bind(this, i)}>{i + 1}</Pagination.Item>);
        }
        return pages;
    }

    render() {
        return (
            <React.Fragment>
                {this.props.pages ?
                    <Pagination className="d-flex justify-content-center align-items-center">
                        {this.props.pages.number > 1 ?
                            <Pagination.First onClick={this.props.onClick.bind(this, 0)} /> : <React.Fragment></React.Fragment>
                        }
                        {this.props.pages.number > 0 ?
                            <Pagination.Prev onClick={this.props.onClick.bind(this, this.props.pages.number - 1)} /> : <React.Fragment></React.Fragment>
                        }

                        {this.createPagination()}

                        {this.props.pages.number <= this.props.pages.totalPages-2 ?
                            <Pagination.Next onClick={this.props.onClick.bind(this, this.props.pages.number + 1)} /> : <React.Fragment></React.Fragment>
                        }
                        {this.props.pages.number <= this.props.pages.totalPages-3 ?
                            <Pagination.Last onClick={this.props.onClick.bind(this, this.props.pages.totalPages - 1)} /> : <React.Fragment></React.Fragment>
                        }
                    </Pagination>
                    :
                    null
                }
            </React.Fragment>
        );
    }
}

export default AppPagination;
