// VENDORS
import React, { Component } from "react";
import { connect } from "react-redux";
import { Badge, Nav, Navbar } from "react-bootstrap";

const mapStateToProps = (state) => ({
    apiURL: state.config.apiURL
});

/**
 * Barre de navigation de l'application
 */
class AppNav extends Component {
    render() {
        return (
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand href="/">OREO</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link href="/protocol">Protocole</Nav.Link>
                    <Nav.Link href="/sql">SQL</Nav.Link>
                    <Nav.Link href="/config">Config</Nav.Link>
                </Nav>
            
                <Navbar.Collapse className="justify-content-end">
                    <Badge variant="success">API : {this.props.apiURL}</Badge>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default connect(() => mapStateToProps, ({}))(AppNav);
