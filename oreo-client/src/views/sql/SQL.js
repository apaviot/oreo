// VENDORS
import React, { Component } from "react";
import { Button, Card, Container, Form } from "react-bootstrap";
import { connect } from "react-redux";

// SERVICES
import { sendSQLRequest, exportSQLRequest } from "../../redux/sql";

const mapDispatchToProps = dispatch => ({
    onSendClick: request => dispatch(sendSQLRequest(request)),
    onExportClick: request => dispatch(exportSQLRequest(request))
});

const mapStateToProps = state => ({
    sqlResult: state.sql.sqlResult
});

/**
 * Composant permettant d'effectuer des requetes SQL (SELECT seulement)
 */
class SQL extends Component {
    constructor(props) {
        super(props);
        this.state = {
            validated: false
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.state.request) {
            this.setState({
                validated: true
            });
            this.props.onSendClick(this.state.request);
        }
    }

    handleExport() {
        if (this.state.request) {
            this.setState({
                validated: true
            });
            this.props.onExportClick(this.state.request);
        }
    }

    onChange(key) {
        return e => this.setState({
            [key]: e.target.value
        });
    }

    render() {
        return (
            <Container className="my-4">
                <h2>Requête SQL</h2>
                <Card className="my-4">
                    <Card.Header>Requête SQL</Card.Header>
                    <Card.Body>
                        <Form
                            noValidate
                            validated={this.state.validated}
                            onSubmit={this.handleSubmit.bind(this)}
                        >
                            <Form.Group>
                                <Form.Label>Entrez votre requête SQL.</Form.Label>
                                <Form.Control onChange={this.onChange("request").bind(this)} as="textarea" rows="3" required />
                            </Form.Group>
                            <Button type="submit" variant="outline-primary">Envoyer</Button>
                            <Button type="button" variant="outline-primary" className="ml-2" onClick={this.handleExport.bind(this)}>Exporter</Button>
                        </Form>

                    </Card.Body>
                </Card>

                { this.props.sqlResult !== null ?
                    <Card className="my-4">
                        <Card.Header>Résultat</Card.Header>
                        <Card.Body>
                            <pre>{JSON.stringify(this.props.sqlResult, undefined, 2)}</pre>
                        </Card.Body>
                    </Card>
                    : <React.Fragment />
                }
            </Container>
        );
    }
}
//onClick={this.handleSubmit(protocol)}
export default connect(() => mapStateToProps, mapDispatchToProps)(SQL);
