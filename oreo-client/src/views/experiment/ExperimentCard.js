import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, ButtonGroup, Col, FormControl, InputGroup, Row, ToggleButton } from "react-bootstrap";
import { FaPlay, FaStop } from "react-icons/fa";

import { UPDATE_TIME_ACTION } from "../../redux/time";
import { stopProtocol } from "../../redux/protocol";
import { convertDateToString, getDateFromDateHours } from "../../services/date";

const mapStateToProps = state => ({
    experiment: state.experiment.experiment,
    startTime: state.time.startTime,
    endTime: state.time.endTime,
    chosenProtocol : state.protocol.chosenProtocol,
    playState: state.experiment.playState
});

const mapDispatchToProps = dispatch => ({
    stopProtocol: id => dispatch(stopProtocol(id)),
    updateTime: payload => dispatch({
        type: UPDATE_TIME_ACTION,
        payload
    })
});

/**
 * Composant permettant l'affichage du graphique et des tableaux
 */
class ExperimentCard extends Component {
    onPlay(e) {
        if (e.target.disabled) {
            return;
        }
        this.props.show("settingsModalShow")();
    }

    onStop() {
        this.props.chartService.stopTimeline();

        if (this.props.chosenProtocol !== "") {
            this.props.stopProtocol(this.props.chosenProtocol);
        }
    }

    onChange(key) {
        return e => {
            const newDate = getDateFromDateHours(this.props.experiment.begin, e.target.value);
            if (newDate instanceof Date && !isNaN(newDate)) {
                this.props.updateTime({
                    [key]: newDate
                });
                this.props.chartService.updateScrollbar();
            }
        };
    }

    render() {
        return (
            <Row>
                <Col className="mt-3 text-left">
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputGroupPrepend">Début</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            type="time"
                            step="0.001"
                            min="00:00:00"
                            max="24:00:00"
                            value={convertDateToString(this.props.startTime)}
                            onChange={this.onChange("startTime").bind(this)}
                        />
                    </InputGroup>
                </Col>

                <Col xs={5} className="text-center">
                    <ButtonGroup className="mr-2 mt-3" aria-label="First group">
                        <Button variant="outline-secondary" onClick={this.props.show("globalModificationModalShow")}>Modifications</Button>
                    </ButtonGroup>
                    <ButtonGroup className="mr-2 mt-3" aria-label="First group">
                        <Button variant="outline-secondary" onClick={this.props.show("measureModalShow")}>Mesures</Button>
                    </ButtonGroup>

                    <ButtonGroup toggle className="mt-3">
                        <ToggleButton
                            type="radio"
                            name="radio"
                            defaultChecked
                            value="1"
                            variant="outline-secondary"
                            onChange={this.onPlay.bind(this)}
                            disabled={this.props.playState}
                        >
                            <FaPlay />
                        </ToggleButton>

                        <ToggleButton type="radio" name="radio" value="3" variant="outline-secondary" onChange={this.onStop.bind(this)} >
                            <FaStop />
                        </ToggleButton>
                    </ButtonGroup>
                </Col>

                <Col className="mt-3 text-right">
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputGroupPrepend">Fin</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            type="time"
                            step="0.001"
                            min="00:00:00"
                            max="24:00:00"
                            value={convertDateToString(this.props.endTime)}
                            onChange={this.onChange("endTime").bind(this)}
                        />
                    </InputGroup>
                </Col>
            </Row>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(ExperimentCard);
