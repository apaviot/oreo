// VENDORS
import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal, Button, Row, Col, ListGroup } from "react-bootstrap";
import { FaTrashAlt, FaRegEye, FaEyeSlash } from "react-icons/fa";

// SERVICES
import { deleteModification, fetchMeasuresModifiedNames, fetchModifications, TOGGLE_DETAILS_MODIFICATION_ACTION, TOGGLE_PARTIAL_MODIFICATION_ACTION, UPDATE_MODIFICATION_ID } from "../../redux/modification";

// COMPONENTS
import PartialModificationsContent from "./PartialModificationsContent";
import DeleteModal from "../home/DeleteModal";

const mapStateToProps = state => ({
    global_modification: state.modification.global,
    partial_modification: state.modification.partial,
    id: state.experiment.experiment.id,
    selectedMeasures: state.measure.selectedMeasures,
    modificationID: state.modification.modificationID
});

const mapDispatchToProps = dispatch => ({
    onClickDelete: id => dispatch(deleteModification(id)),
    onLoad: id => dispatch(fetchModifications(id)),
    togglePartial: () => dispatch({
        type: TOGGLE_PARTIAL_MODIFICATION_ACTION
    }),
    toggleDetail: modification => dispatch({
        type: TOGGLE_DETAILS_MODIFICATION_ACTION,
        payload: modification
    }),
    updateModificationID: id => dispatch({
        type: UPDATE_MODIFICATION_ID,
        payload: id
    }),
    fetchMeasuresModifiedNames: modifications => dispatch(fetchMeasuresModifiedNames(modifications))
});

/**
 * Modale permettant le parametrage des modifications liées aux mesures
 */
class GlobalModificationsModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentModalType: "",
            deleteModalShow: false
        };
    }

    onApply(e) {
        this.props.updateModificationID(e.id);
        this.props.chartService.update();
        this.props.onHide();
    }

    onRemove(e) {
        e.preventDefault();
        this.props.updateModificationID(null);
        this.props.chartService.update();
    }

    onValidateCreation() {
        this.toggleModalNewPartial();
    }

    toggleModalViewPartial(i, e) {
        e.preventDefault();

        this.toggleCurrentModalType("global");
        this.props.toggleDetail(this.props.global_modification[i]);
        this.props.fetchMeasuresModifiedNames(this.props.global_modification);
    }

    toggleModalNewPartial() {
        this.toggleCurrentModalType("partial");
        this.props.togglePartial();
    }

    toggleCurrentModalType(type) {
        if (this.state.currentModalType === type) {
            this.setState({ currentModalType : "" });
        } else {
            this.setState({ currentModalType : type });
        }
    }

    showModal(id,e) {
        e.preventDefault();
        this.setState({ deleteModalShow: true, id });
    }

    showModificationsLines() {
        return this.props.global_modification.map((e, i) =>
            <ListGroup.Item key={i}>
                <Row>
                    <Col className="mt-2">{e.name}</Col>
                    <Col className="mt-2">{e.description}</Col>
                    <Col className="mt-2" md="auto" >
                        {
                            e.showPartial === "" ?
                                <a href="" ><FaRegEye className="text-secondary" onClick={this.toggleModalViewPartial.bind(this, i)} /></a>
                                :
                                <a href="" ><FaEyeSlash className="text-secondary" onClick={this.toggleModalViewPartial.bind(this, i)} /></a>
                        }
                    </Col>
                    <Col className="mt-2" md="auto" >
                        <a href="" ><FaTrashAlt className="text-secondary" onClick={this.showModal.bind(this, e.id)} /></a>
                    </Col>
                    {
                        this.props.modificationID !== null && this.props.modificationID === e.id?
                            <Col md="auto" ><Button variant="outline-secondary" onClick={this.onRemove.bind(this)}>Retirer</Button></Col>
                            :
                            <Col md="auto" ><Button variant="outline-primary" onClick={this.onApply.bind(this, e)}>Appliquer</Button></Col>
                    }
                </Row>

                <div className={"toggle-content " + e.showPartial}>
                    <PartialModificationsContent type={this.state.currentModalType} modifications={e.partials} />
                </div>
                <DeleteModal
                    show={this.state.deleteModalShow}
                    onHide={() => this.setState({ deleteModalShow: false })}
                    onDelete={() => this.props.onClickDelete(e.id)}
                />

            </ListGroup.Item>
        );
    }

    showNewModificationLine() {
        return (
            <ListGroup.Item>
                {
                    this.state.currentModalType !== "partial" ?
                        <Button block onClick={this.toggleModalNewPartial.bind(this)} disabled={this.props.selectedMeasures.length === 0} >
                            Créer une nouvelle modification
                        </Button> : <React.Fragment />
                }
                <div className={"toggle-content " + this.props.partial_modification.showPartial}>

                    <PartialModificationsContent
                        type={this.state.currentModalType}
                        modifications={this.props.partial_modification.partials}
                        details={this.props.partial_modification}
                        onValidateCreation={this.onValidateCreation.bind(this)}
                    />
                    <Button block onClick={this.toggleModalNewPartial.bind(this)} variant="secondary" >Annuler la création</Button>
                </div>
            </ListGroup.Item>
        );
    }


    onEnter() {
        // close partial
        if (this.props.partial_modification.showPartial === "is-visible") {
            this.toggleCurrentModalType("partial");
            this.props.togglePartial();
        }

        this.props.onLoad(this.props.id);
    }
    render() {
        return (
            <Modal
                show={this.props.show}
                onHide={this.props.onHide}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                onEnter={this.onEnter.bind(this)}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="edit-modal-title-vcenter">
                        Modifications
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body className="my-0 pt-0">
                    <ListGroup variant="flush">
                        {this.showModificationsLines()}
                        {this.showNewModificationLine()}
                    </ListGroup>
                </Modal.Body>

            </Modal>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(GlobalModificationsModal);
