import React, { Component } from "react";
import { connect } from "react-redux";

import { sendProtocol } from "../../redux/protocol";
import { parseDate, getDateFromDateHours, convertDateToString } from "../../services/date";

import GlobalModificationsModal from "./GlobalModificationsModal";
import MeasureModal from "./MeasureModal";
import SettingsModal from "./SettingsModal";

const mapStateToProps = state => ({
    experiment: state.experiment.experiment,
    computeTime: state.protocol.computeTime,
    chosenProtocol: state.protocol.chosenProtocol,
    modificationId: state.modification.modificationID,
    selectedMeasures: state.measure.selectedMeasures,
    startTime: state.time.startTime,
    endTime: state.time.endTime
});

const mapDispatchToProps = dispatch => ({
    sendProtocol: (experimentId, id, payload) => dispatch(sendProtocol(experimentId, id, payload))
});

/**
 * Container permettant l'affichage des modales de parametrage
 */
class ExperimentModal extends Component {
    onHideModal() {
        this.props.onHide("settingsModalShow")();
        this.props.chartService.startTimeline();
        if (this.props.chosenProtocol === "") {
            return;
        }

        this.props.sendProtocol(
            this.props.experiment.id,
            this.props.chosenProtocol,
            {
                measures: this.props.selectedMeasures.map(m => m.id),
                computeTime: this.props.computeTime,
                start: parseDate(getDateFromDateHours(this.props.experiment.begin , convertDateToString(this.props.startTime))),
                end: parseDate(getDateFromDateHours(this.props.experiment.end , convertDateToString(this.props.endTime))),
                modificationId: this.props.modificationId
            }
        );
    }

    render() {
        return (
            <React.Fragment>
                <SettingsModal
                    show={this.props.settingsModalShow}
                    onHide={this.props.onHide("settingsModalShow")}
                    onHideModal={this.onHideModal.bind(this)}
                />

                <GlobalModificationsModal
                    show={this.props.globalModificationModalShow}
                    onHide={this.props.onHide("globalModificationModalShow")}
                    chartService={this.props.chartService}
                />

                <MeasureModal
                    show={this.props.measureModalShow}
                    onHide={this.props.onHide("measureModalShow")}
                    chartService={this.props.chartService}
                />
            </React.Fragment>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(ExperimentModal);
