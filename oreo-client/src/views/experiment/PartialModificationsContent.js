// VENDORS
import React, { Component } from "react";
import { Button, FormControl, InputGroup, Form, Table, Card, Row, Col } from "react-bootstrap";
import { FaTrashAlt } from "react-icons/fa";
import { connect } from "react-redux";
import { toast } from "react-toastify";

// SERVICES
import { createModification, DELETE_PARTIAL_MODIFICATION_ACTION, UPDATE_PARTIAL_MODIFICATIONS_ACTION } from "../../redux/modification";
import { getDateFromDateHours, parseDate, convertDateToString, checkHoursGrowing } from "../../services/date";


const mapStateToProps = state => ({
    measures: state.measure.measures,
    selectedMeasures: state.measure.selectedMeasures,
    experiment: state.experiment.experiment,
    partial_modification: state.modification.partial,
    startTime: state.time.startTime,
    endTime: state.time.endTime,
    global_modification: state.modification.global
});

const mapDispatchToProps = dispatch => ({
    createModification: opts => dispatch(createModification(opts)),
    updatePartials: partial_modification => dispatch({
        type: UPDATE_PARTIAL_MODIFICATIONS_ACTION,
        payload: partial_modification
    }),
    removePartialModification: index => dispatch({
        type: DELETE_PARTIAL_MODIFICATION_ACTION,
        payload: index
    })
});

/**
 * Composant permettant de créer/afficher des modifications partielles
 */
class PartialModificationsContent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            measureId: -1,
            startedDate: convertDateToString(props.startTime),
            endedDate: convertDateToString(props.endTime),
            operation: "ADD",
            operator: "",

            validate: false,
            validationButton: false,

            name: "",
            description: ""
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.name !== this.state.name || prevState.description !== this.state.description || prevProps.modifications !== this.props.modifications ) {
            if (this.state.name.trim() !== "" && this.state.description.trim() !== "" && this.props.modifications.length !== 0) {
                this.setState({ validationButton : true });
            } else {
                this.setState({ validationButton : false });
            }
        }
    }

    onValidatePartialModifications(e) {
        e.preventDefault();
        this.setState({ validate: false });

        const details = {
            name: this.state.name,
            creationDate: parseDate(Date.now()),
            lastUse: null,
            description: this.state.description,
            experimentId: this.props.experiment.id,
            partials: this.props.partial_modification.partials.map(partial => ({
                ...partial,
                startedDate: parseDate(partial.startedDate),
                endedDate: parseDate(partial.endedDate)
            })),
            remote: true,
            isRemote: true
        };

        this.props.createModification(details);
        this.props.onValidateCreation();
    }

    onAdd(e) {
        e.preventDefault();

        const checkHours = checkHoursGrowing(this.state.startedDate, this.state.endedDate);
        if (!checkHours) {
            toast.error("L'heure de début doit être inférieure à l'heure de fin.");
            return;
        }

        const measureId = parseInt(this.state.measureId === -1 ? this.props.selectedMeasures[0].id : this.state.measureId, 10);

        const form = event.currentTarget;
        if (form.checkValidity()) {
            const partials = {
                measureId,
                startedDate : getDateFromDateHours(this.props.experiment.begin, this.state.startedDate),
                endedDate : getDateFromDateHours(this.props.experiment.begin, this.state.endedDate),
                operation :  this.state.operation,
                operator :  this.state.operator,
                name: this.props.selectedMeasures.find(f => f.id === measureId).name
            };

            this.props.updatePartials(partials);
            this.setState({ validated: true });
        }
    }

    onChange(key) {
        return e => this.setState({
            [key]: e.target.value
        });
    }

    removePartialModification(index, e) {
        e.preventDefault();
        this.props.removePartialModification(index);
    }

    showPartialSettings() {
        return this.props.modifications.map( (e, i) =>
            <tr key={i}>
                <td>{e.name}</td>
                <td>{convertDateToString(e.startedDate)}</td>
                <td>{convertDateToString(e.endedDate)}</td>
                <td>{e.operation}</td>
                <td>{e.operator}</td>
                {
                    this.props.type === "partial" ?
                        <td><a href=""><FaTrashAlt onClick={this.removePartialModification.bind(this, i)} className="align-text-top float-right" /></a></td>
                        :
                        <React.Fragment />
                }
            </tr>
        );
    }

    showGlobalSettings() {
        return (
            <Row className="mb-2">
                <Col>
                    <InputGroup>
                        <FormControl onChange={this.onChange("name")} placeholder="Nom" type="text" required />
                        <FormControl onChange={this.onChange("description")} placeholder="Description" type="text" required />
                        <Button className="button-radius-right" onClick={this.onValidatePartialModifications.bind(this)} disabled={!this.state.validationButton} >Créer</Button>
                    </InputGroup>
                </Col>

            </Row>
        );
    }

    setMeasuresOptions() {
        return this.props.selectedMeasures.map((selectedMeasure, i) => <option key={i} value={selectedMeasure.id}>
            {selectedMeasure.name}
        </option>);
    }

    showAddLine() {
        return (
            <tr>
                <td>
                    <Form.Group onChange={this.onChange("measureId")} required>
                        <Form.Control as="select">
                            {this.setMeasuresOptions()}
                        </Form.Control>
                    </Form.Group>
                </td>
                <td><FormControl step="0.001" onChange={this.onChange("startedDate")} value={this.state.startedDate} required placeholder="Début" type="time" /></td>
                <td><FormControl step="0.001" onChange={this.onChange("endedDate")} value={this.state.endedDate} required placeholder="Fin" type="time" /></td>
                <td>
                    <Form.Group onChange={this.onChange("operation")} required>
                        <Form.Control as="select">
                            <option value={"ADD"}>ADD</option>
                            <option value={"CONSTANT"}>CONSTANT</option>
                            <option value={"MULTIPLY"}>MULTIPLY</option>
                        </Form.Control>
                    </Form.Group>
                </td>
                <td>
                    <InputGroup>
                        <FormControl onChange={this.onChange("operator")} required placeholder="Valeur" type="number" />
                    </InputGroup>
                </td>
                <td><Button type="submit">Ajouter</Button></td>
            </tr>
        );
    }

    render() {
        return (
            <Card className="my-2 p-2 bg-grey">
                {this.props.type === "partial" ? this.showGlobalSettings() : <React.Fragment />}

                <Form
                    noValidate
                    validated={this.state.validated}
                    onSubmit={e => this.onAdd(e)}
                >

                    <Table responsive="xl">
                        <thead>
                            <tr>
                                <th>Mesure</th>
                                <th>Début</th>
                                <th>Fin</th>
                                <th>Opération</th>
                                <th>Valeur</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.showPartialSettings()}

                            {this.props.type === "partial" ? this.showAddLine() : <React.Fragment />}
                        </tbody>
                    </Table>
                </Form>
            </Card>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(PartialModificationsContent);
