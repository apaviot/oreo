import React, { Component } from "react";
import { connect } from "react-redux";
import { Tabs, Tab, Table } from "react-bootstrap";

import { addTenSecond, checkDateGrowing, parseDate } from "../../services/date";
import { updateRealTimeTable } from "../../redux/sample";

const mapStateToProps = state => ({
    alarms: state.alarm.alarms,
    realTimeTable: state.sample.realTimeTable,
    currentTime: state.time.currentTime,
    samples: state.sample.samples,
    selectedMeasures: state.measure.selectedMeasures
});

const mapDispatchToProps = dispatch => ({
    updateRealTimeTable: measures => dispatch(updateRealTimeTable(measures)),
});

/**
 * Composant permettant l'affichage de deux tableaux : alarmes et données en temps réel
 */
class ExperimentTabs extends Component {
    lastTime = null;

    componentDidUpdate(prevProps) {
        if (prevProps.currentTime !== this.props.currentTime) {
            if (this.lastTime !== null && !checkDateGrowing(this.props.currentTime, this.lastTime)) {
                this.lastTime = null;
            }
            if (this.lastTime === null || checkDateGrowing(this.props.currentTime, addTenSecond(this.lastTime))) {
                console.log("koukou");
                this.props.updateRealTimeTable(this.props.selectedMeasures);
                this.lastTime = this.props.currentTime;
            }
        }
    }

    getAlarmsContent() {
        return this.props.alarms.map((alarm, i) =>
            <tr key={i}>
                <td>{alarm.id}</td>
                <td>{alarm.level}</td>
                <td>{alarm.message}</td>
                <td>{alarm.name}</td>
                <td>{alarm.order}</td>
                <td>{alarm.reference}</td>
                <td>{alarm.state}</td>
                <td>{alarm.time ? parseDate(alarm.time) : ""}</td>
            </tr>
        );
    }

    getAlarmsTab() {
        return (
            <Tab eventKey={"alarmes"} title={"Alarmes"}>
                <Table responsive="xl">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Level</th>
                            <th>Message</th>
                            <th>Name</th>
                            <th>Order</th>
                            <th>Référence</th>
                            <th>État</th>
                            <th>Time</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.getAlarmsContent()}
                    </tbody>
                </Table>
            </Tab>
        );
    }

    getMesuresContent() {
        return this.props.realTimeTable.map((e, i) =>
            <tr key={i}>
                <td>{e.measureId}</td>
                <td>{e.measure}</td>
                <td>{e.value}</td>
                <td>{e.unit}</td>
                <td>{parseDate(e.time)}</td>
            </tr>
        );
    }

    getPlayBarTab() {
        return (
            <Tab eventKey={"realTime"} title={"Informations détaillées (≈10s)"}>
                <Table responsive="xl">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nom de la mesure</th>
                            <th>Valeur</th>
                            <th>Unité</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.getMesuresContent()}
                    </tbody>
                </Table>
            </Tab>
        );
    }

    render() {
        return (
            <Tabs className="mb-0 mt-2">
                {this.getAlarmsTab()}
                {this.getPlayBarTab()}
            </Tabs>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(ExperimentTabs);
