// VENDORS
import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal, Button, InputGroup, Form } from "react-bootstrap";

// SERVICES
import { fetchProtocols, downloadProtocol, CHOOSE_PROTOCOL_ACTION, COMPUTE_PROTOCOL_ACTION } from "../../redux/protocol";

const mapStateToProps = state => ({
    experiment: state.experiment.experiment,
    protocols: state.protocol.protocols,
    chosenProtocol: state.protocol.chosenProtocol,
    computeTime : state.protocol.computeTime,
    selectedMeasures : state.measure.selectedMeasures
});

const mapDispatchToProps = dispatch => ({
    fetchProtocols: () => dispatch(fetchProtocols()),
    chooseProtocol: protocol => dispatch({
        type: CHOOSE_PROTOCOL_ACTION,
        payload: protocol
    }),
    computeTimeProtocol: () => dispatch({
        type: COMPUTE_PROTOCOL_ACTION
    }),
    downloadProtocol: (id, experiment, measures) => dispatch(downloadProtocol(id, experiment, measures))
});

/**
 * Modale permettant le parametrage d'envoi des données
 */
class SettingsModal extends Component {
    onDownload(e) {
        e.preventDefault();
        this.props.downloadProtocol(
            this.props.chosenProtocol,
            this.props.experiment.id,
            this.props.selectedMeasures.map(m => m.id),
        );
    }

    onComputeTime(e) {
        e.preventDefault();
        this.props.computeTime();
    }

    getProtocolsOptions() {
        return this.props.protocols
            .map((e, i) =>
                <option key={i} value={e.id}>{e.id}</option>
            );
    }

    render() {
        return (
            <Modal
                show={this.props.show}
                onHide={this.props.onHide}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                onEnter={this.props.fetchProtocols.bind(this)}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Réglage du protocole d'envoi
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group onChange={e => this.props.chooseProtocol(e.target.value)} required>
                            <Form.Label>Choisir le protocole d'envoi :</Form.Label>
                            <Form.Control as="select" defaultValue={this.props.chosenProtocol}>
                                <option value="">Aucun Protocole</option>
                                {this.getProtocolsOptions()}
                            </Form.Control>
                        </Form.Group>

                        {
                            this.props.chosenProtocol === "iena"
                                ?
                                    <Form.Group>
                                        <InputGroup className="mb-3">
                                            <Button id="download" type="submit" onClick={this.onDownload.bind(this)} >Télécharger</Button>
                                        </InputGroup>
                                    </Form.Group>
                                :
                                    null
                        }

                        {
                            this.props.chosenProtocol !== ""
                                ?
                                <Form.Group onChange={this.props.computeTimeProtocol.bind(this)}>
                                    <Form.Label>Choisir la date d'envoi :</Form.Label>
                                    <Form.Control as="select" defaultValue="">
                                        <option >Date d'origine</option>
                                        <option >Recalculer la date</option>
                                    </Form.Control>
                                </Form.Group>
                                :
                                    null
                        }


                    </Form>

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.onHideModal}>Jouer</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(SettingsModal);
