import React, { Component } from "react";
import { connect } from "react-redux";
import { Badge, Col, Jumbotron, Row } from "react-bootstrap";

const mapStateToProps = state => ({
    experiment: state.experiment.experiment,
    campaign: state.campaign.campaign,
    bench: state.bench.bench
});

/**
 * Composant permettant l'affichage des données du détail de l'essai
 */
class ExperimentInfo extends Component {
    render() {
        return (
            <Jumbotron className="py-4 mb-3">
                <Row>
                    <Col className="border-left border-dark">
                        <p className="mb-0">
                            ID : {this.props.experiment.id}<br />
                            Référence : {this.props.experiment.reference}<br />
                            Début : {new Date(this.props.experiment.begin).toLocaleString()}<br />
                            Fin : {new Date(this.props.experiment.end).toLocaleString()}<br />
                            Campagne : {this.props.campaign.id12c}<br />
                            Banc : {this.props.bench.name}
                        </p>
                    </Col>
                    <Col className="justify-content-end align-self-center text-right">
                        <h1>{this.props.experiment.name}</h1>
                        {
                            !this.props.experiment.isStoredOnline
                                ?
                                <h5><Badge variant="success">Locale</Badge></h5>
                                :
                                <h5><Badge variant="info">Globale</Badge></h5>
                        }
                    </Col>
                </Row>
            </Jumbotron>
        );
    }
}

export default connect(() => mapStateToProps, ({}))(ExperimentInfo);
