// VENDORS
import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal,
    Button,
    InputGroup,
    FormControl, Table, Form } from "react-bootstrap";

// SERVICES
import { searchMeasures, UPDATE_MEASURE_ACTION } from "../../redux/measure";

// COMPONENTS
import AppPagination from "../common/AppPagination";

const mapStateToProps = state => ({
    measures: state.measure.measures,
    pages: state.measure.page,
    selectedMeasures: state.measure.selectedMeasures,
    experimentId: state.experiment.experiment.id,
});

const mapDispatchToProps = dispatch => ({
    updateMeasures: selectedMeasures => dispatch({
        type: UPDATE_MEASURE_ACTION,
        payload: selectedMeasures
    }),
    searchMeasures: (experimentID, opts) => dispatch(searchMeasures(experimentID, opts))
});

const MAX_MEASURES = 3;

/**
 * Modale permettant de choisir les mesures à afficher (max 3)
 */
class MeasureModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedMeasures: [],
            searchBar: "",
            disableApply : false
        };
    }

    onClickAdd(measure) {
        return () => {
            const selectedMeasures = this.state.selectedMeasures.slice();
            selectedMeasures.push(measure);

            this.setState({
                selectedMeasures,
                disableApply: selectedMeasures.length >= MAX_MEASURES
            });
        };
    }

    onClearAll() {
        return () => {
            this.setState({
                selectedMeasures: [],
                disableApply: false
            });
        };
    }

    onClickRemove(measure) {
        return () => {
            const selectedMeasures = this.state.selectedMeasures.filter(selectedMeasure => measure.id !== selectedMeasure.id);
        
            this.setState({
                selectedMeasures,
                disableApply: selectedMeasures.length >= MAX_MEASURES
            });
        };
    }

    onEnter() {
        this.props.searchMeasures(this.props.experimentId);
        this.setState({ selectedMeasures: this.props.selectedMeasures.slice() });
    }

    onHide() {
        this.props.updateMeasures(this.state.selectedMeasures);
        this.props.chartService.update();
        this.props.onHide();
    }

    getTable() {
        return this.props.measures.map((measure, i) =>
            <tr key={i}>
                <td>{measure.id}</td>
                <td>{measure.name}</td>
                <td>{measure.unit}</td>
                <td className="text-right">
                    {
                        this.isInclude(measure) ?
                        <Button variant="outline-secondary" onClick={this.onClickRemove(measure).bind(this)}>Retirer</Button>
                        :
                        <Button variant="outline-primary" onClick={this.onClickAdd(measure).bind(this)} disabled={this.state.disableApply}>Ajouter</Button>
                    }
                </td>
            </tr>
        );
    }

    onSearch(e) {
        e.preventDefault();
        this.props.searchMeasures(this.props.experimentId, { name: this.state.searchBar });
    }

    onChange(key) {
        return e => this.setState({
            [key]: e.target.value
        });
    }

    onClickPage(page) {
        this.props.searchMeasures(this.props.experimentId, { page, name: this.state.searchBar });
    }

    isInclude(measure) {
        for (let selectedMeasure of this.state.selectedMeasures) {
            if (measure.id === selectedMeasure.id) {
                return true;
            }
        }
        return false;
    }

    render() {
        return (
            <Modal
                show={this.props.show}
                onEnter={this.onEnter.bind(this)}
                onHide={this.onHide.bind(this)}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Form>
                    <Form.Group>
                        <Modal.Header closeButton>
                            <Modal.Title id="contained-modal-title-vcenter">
                                Mesures
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>

                            <InputGroup className="mb-3">
                                <FormControl
                                    placeholder="..."
                                    type="text"
                                    onChange={this.onChange("searchBar").bind(this)}
                                />
                                <InputGroup.Append>
                                    <Button id="searchButton" type="submit" onClick={this.onSearch.bind(this)}>Chercher</Button>
                                </InputGroup.Append>
                            </InputGroup>

                            <Table responsive="xl">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nom</th>
                                    <th>Unité</th>
                                    <th className="text-right" ><Button variant="danger" onClick={this.onClearAll().bind(this)} disabled={this.state.selectedMeasures.length === 0}>Désélectionner tout</Button></th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.getTable()}
                                </tbody>
                            </Table>

                            <AppPagination pages={this.props.pages} onClick={this.onClickPage.bind(this)} />

                        </Modal.Body>
                    </Form.Group>
                </Form>
            </Modal>
        );
    }
}

export default connect(() => mapStateToProps, mapDispatchToProps)(MeasureModal);