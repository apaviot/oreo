// VENDORS
import React, { Component } from "react";
import { connect } from "react-redux";
import { Card, Container } from "react-bootstrap";

// SERVICES
import { fetchExperimentInfo } from "../../redux/experiment";
import ChartService from "../../services/chart";

// COMPONENTS
import ExperimentInfo from "./ExperimentInfo";
import ExperimentTabs from "./ExperimentTabs";
import ExperimentModal from "./ExperimentModal";
import ExperimentCard from "./ExperimentCard";

const mapDispatchToProps = dispatch => ({
    fetchExperimentInfo: id => dispatch(fetchExperimentInfo(id))
});

/**
 * Composant permettant l'affichage d'un essai
 */
class Experiment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            globalModificationModalShow: false,
            settingsModalShow: false,
            measureModalShow: false
        };
        this.chartService = new ChartService();
    }

    componentDidMount() {
        this.props.fetchExperimentInfo(this.props.match.params.id)
        .then(() => this.chartService.init());
    }

    componentWillUnmount() {
        this.chartService.destroy();
    }

    render() {
        return (
            <Container fluid className="my-4">
                <ExperimentInfo />
                <ExperimentModal
                    {...this.state}
                    onHide={key => () => this.setState({ [key]: false })}
                    chartService={this.chartService}
                />

                <Card body className="my-2">
                    <ExperimentCard
                        show={key => () => this.setState({ [key]: true })}
                        chartService={this.chartService}
                    />

                    <div id="chartdiv" style={{ width: "100%", height: "85vh" }} />

                    <ExperimentTabs />
                </Card>
            </Container>
        );
    }
}

export default connect(() => ({}), mapDispatchToProps)(Experiment);
