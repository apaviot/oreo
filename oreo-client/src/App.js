import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import AppNav from "./views/common/AppNav";
import "./App.css";
import Home from "./views/home/Home";
import Protocol from "./views/protocol/Protocol";
import SQL from "./views/sql/SQL";
import Experiment from "./views/experiment/Experiment";
import Config from "./views/config/Config";

const App = () => <Router>
    <div>
        <AppNav />

        <Route path="/" exact component={Home} />
        <Route path="/protocol"  component={Protocol} />
        <Route path="/sql" component={SQL} />
        <Route path="/experiment/:id" component={Experiment} />
        <Route path="/config" component={Config} />
    </div>
</Router>;

export default App;
