/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import ResourceOfBench from '../model/ResourceOfBench';
import ResourcesOfBench from '../model/ResourcesOfBench';

/**
* BenchEntity service.
* @module api/BenchEntityApi
* @version 1.0
*/
export default class BenchEntityApi {

    /**
    * Constructs a new BenchEntityApi. 
    * @alias module:api/BenchEntityApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }



    /**
     * findAllBench
     * @param {Object} opts Optional parameters
     * @param {String} opts.page page
     * @param {String} opts.size size
     * @param {String} opts.sort sort
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/ResourcesOfBench} and HTTP response
     */
    findAllBenchUsingGETWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = null;


      let pathParams = {
      };
      let queryParams = {
        'page': opts['page'],
        'size': opts['size'],
        'sort': opts['sort']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['application/json', 'application/hal+json', 'text/uri-list', 'application/x-spring-data-compact+json'];
      let returnType = ResourcesOfBench;

      return this.apiClient.callApi(
        '/api/benches', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * findAllBench
     * @param {Object} opts Optional parameters
     * @param {String} opts.page page
     * @param {String} opts.size size
     * @param {String} opts.sort sort
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/ResourcesOfBench}
     */
    findAllBenchUsingGET(opts) {
      return this.findAllBenchUsingGETWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * findOneBench
     * @param {Number} id id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/ResourceOfBench} and HTTP response
     */
    findOneBenchUsingGETWithHttpInfo(id) {
      let postBody = null;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling findOneBenchUsingGET");
      }


      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['*/*'];
      let returnType = ResourceOfBench;

      return this.apiClient.callApi(
        '/api/benches/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * findOneBench
     * @param {Number} id id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/ResourceOfBench}
     */
    findOneBenchUsingGET(id) {
      return this.findOneBenchUsingGETWithHttpInfo(id)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


}
