/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import ResourceOfAlarm from '../model/ResourceOfAlarm';
import ResourceOfPageOfAlarm from '../model/ResourceOfPageOfAlarm';
import ResourcesOfAlarm from '../model/ResourcesOfAlarm';

/**
* AlarmEntity service.
* @module api/AlarmEntityApi
* @version 1.0
*/
export default class AlarmEntityApi {

    /**
    * Constructs a new AlarmEntityApi. 
    * @alias module:api/AlarmEntityApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }



    /**
     * findByExperimentIdAlarm
     * @param {Object} opts Optional parameters
     * @param {Number} opts.experimentId experimentId
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/ResourcesOfAlarm} and HTTP response
     */
    findByExperimentIdAlarmUsingGETWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = null;


      let pathParams = {
      };
      let queryParams = {
        'experimentId': opts['experimentId']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['*/*'];
      let returnType = ResourcesOfAlarm;

      return this.apiClient.callApi(
        '/api/alarms/search/findByExperimentId', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * findByExperimentIdAlarm
     * @param {Object} opts Optional parameters
     * @param {Number} opts.experimentId experimentId
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/ResourcesOfAlarm}
     */
    findByExperimentIdAlarmUsingGET(opts) {
      return this.findByExperimentIdAlarmUsingGETWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * findByExperimentIdAndDateAlarm
     * @param {Object} opts Optional parameters
     * @param {Date} opts.end end
     * @param {Number} opts.experimentId experimentId
     * @param {String} opts.pageable pageable
     * @param {Date} opts.start start
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/ResourceOfPageOfAlarm} and HTTP response
     */
    findByExperimentIdAndDateAlarmUsingGETWithHttpInfo(opts) {
      opts = opts || {};
      let postBody = null;


      let pathParams = {
      };
      let queryParams = {
        'end': opts['end'],
        'experimentId': opts['experimentId'],
        'pageable': opts['pageable'],
        'start': opts['start']
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['*/*'];
      let returnType = ResourceOfPageOfAlarm;

      return this.apiClient.callApi(
        '/api/alarms/search/findByExperimentIdAndDate', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * findByExperimentIdAndDateAlarm
     * @param {Object} opts Optional parameters
     * @param {Date} opts.end end
     * @param {Number} opts.experimentId experimentId
     * @param {String} opts.pageable pageable
     * @param {Date} opts.start start
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/ResourceOfPageOfAlarm}
     */
    findByExperimentIdAndDateAlarmUsingGET(opts) {
      return this.findByExperimentIdAndDateAlarmUsingGETWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * findOneAlarm
     * @param {Number} id id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/ResourceOfAlarm} and HTTP response
     */
    findOneAlarmUsingGETWithHttpInfo(id) {
      let postBody = null;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling findOneAlarmUsingGET");
      }


      let pathParams = {
        'id': id
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = [];
      let contentTypes = [];
      let accepts = ['*/*'];
      let returnType = ResourceOfAlarm;

      return this.apiClient.callApi(
        '/api/alarms/{id}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * findOneAlarm
     * @param {Number} id id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/ResourceOfAlarm}
     */
    findOneAlarmUsingGET(id) {
      return this.findOneAlarmUsingGETWithHttpInfo(id)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


}
