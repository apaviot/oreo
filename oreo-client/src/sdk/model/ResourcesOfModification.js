/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */


import ApiClient from '../ApiClient';
import EmbeddedCollectionOfModification from './EmbeddedCollectionOfModification';
import Link from './Link';
import Page from './Page';





/**
* The ResourcesOfModification model module.
* @module model/ResourcesOfModification
* @version 1.0
*/
export default class ResourcesOfModification {
    /**
    * Constructs a new <code>ResourcesOfModification</code>.
    * Resources of Modification
    * @alias module:model/ResourcesOfModification
    * @class
    * @param embedded {module:model/EmbeddedCollectionOfModification} 
    * @param page {module:model/Page} 
    * @param links {Object.<String, module:model/Link>} Link collection
    */

    constructor(embedded, page, links) {
        

        
        

        this['_embedded'] = embedded;this['page'] = page;this['_links'] = links;

        
    }

    /**
    * Constructs a <code>ResourcesOfModification</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/ResourcesOfModification} obj Optional instance to populate.
    * @return {module:model/ResourcesOfModification} The populated <code>ResourcesOfModification</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ResourcesOfModification();

            
            
            

            if (data.hasOwnProperty('_embedded')) {
                obj['_embedded'] = EmbeddedCollectionOfModification.constructFromObject(data['_embedded']);
            }
            if (data.hasOwnProperty('page')) {
                obj['page'] = Page.constructFromObject(data['page']);
            }
            if (data.hasOwnProperty('_links')) {
                obj['_links'] = ApiClient.convertToType(data['_links'], {'String': Link});
            }
        }
        return obj;
    }

    /**
    * @member {module:model/EmbeddedCollectionOfModification} _embedded
    */
    _embedded = undefined;
    /**
    * @member {module:model/Page} page
    */
    page = undefined;
    /**
    * Link collection
    * @member {Object.<String, module:model/Link>} _links
    */
    _links = undefined;








}


