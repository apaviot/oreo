/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */


import ApiClient from '../ApiClient';
import EmbeddedCollectionOfExperiment from './EmbeddedCollectionOfExperiment';
import Link from './Link';
import Page from './Page';





/**
* The ResourcesOfExperiment model module.
* @module model/ResourcesOfExperiment
* @version 1.0
*/
export default class ResourcesOfExperiment {
    /**
    * Constructs a new <code>ResourcesOfExperiment</code>.
    * Resources of Experiment
    * @alias module:model/ResourcesOfExperiment
    * @class
    * @param embedded {module:model/EmbeddedCollectionOfExperiment} 
    * @param page {module:model/Page} 
    * @param links {Object.<String, module:model/Link>} Link collection
    */

    constructor(embedded, page, links) {
        

        
        

        this['_embedded'] = embedded;this['page'] = page;this['_links'] = links;

        
    }

    /**
    * Constructs a <code>ResourcesOfExperiment</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/ResourcesOfExperiment} obj Optional instance to populate.
    * @return {module:model/ResourcesOfExperiment} The populated <code>ResourcesOfExperiment</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ResourcesOfExperiment();

            
            
            

            if (data.hasOwnProperty('_embedded')) {
                obj['_embedded'] = EmbeddedCollectionOfExperiment.constructFromObject(data['_embedded']);
            }
            if (data.hasOwnProperty('page')) {
                obj['page'] = Page.constructFromObject(data['page']);
            }
            if (data.hasOwnProperty('_links')) {
                obj['_links'] = ApiClient.convertToType(data['_links'], {'String': Link});
            }
        }
        return obj;
    }

    /**
    * @member {module:model/EmbeddedCollectionOfExperiment} _embedded
    */
    _embedded = undefined;
    /**
    * @member {module:model/Page} page
    */
    page = undefined;
    /**
    * Link collection
    * @member {Object.<String, module:model/Link>} _links
    */
    _links = undefined;








}


