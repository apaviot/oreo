/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */


import ApiClient from '../ApiClient';
import Measure from './Measure';





/**
* The EmbeddedCollectionOfMeasure model module.
* @module model/EmbeddedCollectionOfMeasure
* @version 1.0
*/
export default class EmbeddedCollectionOfMeasure {
    /**
    * Constructs a new <code>EmbeddedCollectionOfMeasure</code>.
    * Embedded collection of Measure
    * @alias module:model/EmbeddedCollectionOfMeasure
    * @class
    * @param measures {Array.<module:model/Measure>} Resource collection
    */

    constructor(measures) {
        

        
        

        this['measures'] = measures;

        
    }

    /**
    * Constructs a <code>EmbeddedCollectionOfMeasure</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/EmbeddedCollectionOfMeasure} obj Optional instance to populate.
    * @return {module:model/EmbeddedCollectionOfMeasure} The populated <code>EmbeddedCollectionOfMeasure</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new EmbeddedCollectionOfMeasure();

            
            
            

            if (data.hasOwnProperty('measures')) {
                obj['measures'] = ApiClient.convertToType(data['measures'], [Measure]);
            }
        }
        return obj;
    }

    /**
    * Resource collection
    * @member {Array.<module:model/Measure>} measures
    */
    measures = undefined;








}


