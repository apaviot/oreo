/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */


import ApiClient from '../ApiClient';





/**
* The Bench model module.
* @module model/Bench
* @version 1.0
*/
export default class Bench {
    /**
    * Constructs a new <code>Bench</code>.
    * @alias module:model/Bench
    * @class
    */

    constructor() {
        

        
        

        

        
    }

    /**
    * Constructs a <code>Bench</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/Bench} obj Optional instance to populate.
    * @return {module:model/Bench} The populated <code>Bench</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Bench();

            
            
            

            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'Number');
            }
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('reference')) {
                obj['reference'] = ApiClient.convertToType(data['reference'], 'String');
            }
        }
        return obj;
    }

    /**
    * @member {Number} id
    */
    id = undefined;
    /**
    * @member {String} name
    */
    name = undefined;
    /**
    * @member {String} reference
    */
    reference = undefined;








}


