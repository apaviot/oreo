/**
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */


import ApiClient from '../ApiClient';
import Link from './Link';





/**
* The ResourceOfCampaign model module.
* @module model/ResourceOfCampaign
* @version 1.0
*/
export default class ResourceOfCampaign {
    /**
    * Constructs a new <code>ResourceOfCampaign</code>.
    * @alias module:model/ResourceOfCampaign
    * @class
    */

    constructor() {
        

        
        

        

        
    }

    /**
    * Constructs a <code>ResourceOfCampaign</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/ResourceOfCampaign} obj Optional instance to populate.
    * @return {module:model/ResourceOfCampaign} The populated <code>ResourceOfCampaign</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new ResourceOfCampaign();

            
            
            

            if (data.hasOwnProperty('_links')) {
                obj['_links'] = ApiClient.convertToType(data['_links'], [Link]);
            }
            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'Number');
            }
            if (data.hasOwnProperty('id12c')) {
                obj['id12c'] = ApiClient.convertToType(data['id12c'], 'String');
            }
        }
        return obj;
    }

    /**
    * @member {Array.<module:model/Link>} _links
    */
    _links = undefined;
    /**
    * @member {Number} id
    */
    id = undefined;
    /**
    * @member {String} id12c
    */
    id12c = undefined;








}


