import { FETCH_EXPERIMENT_INFO_ACTION } from "./experiment";

/*** CONSTS ***/
/*************/

/*** ACTIONS ***/
/***************/


/*** REDUCER ***/
/***************/
const INITIAL_ALARM_STATE = {
    alarms: []
};

export const alarm = (state = INITIAL_ALARM_STATE, action) => {
    switch (action.type) {
        case FETCH_EXPERIMENT_INFO_ACTION:
            return {
                ...state,
                alarms: action.payload.alarms
            };
        default:
            return state;
    }
};
