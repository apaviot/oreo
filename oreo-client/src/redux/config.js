import { getAPIURL, setAPIURL } from "../services/api";

/*** CONSTS ***/
/*************/
export const UPDATE_API_URL = "api:url:update";

/*** REDUCER ***/
/***************/
const INITIAL_CONFIG_STATE = {
    apiURL: getAPIURL()
};

export const config = (state = INITIAL_CONFIG_STATE, action) => {
    switch (action.type) {
        case UPDATE_API_URL:
            setAPIURL(action.payload);
            return {
                ...state,
                apiURL: action.payload
            };
        default:
            return state;
    }
};
