import { toast } from "react-toastify";
import { sampleAPI } from "../services/api";
import { OnError } from "../services/glamor";
import { addTenSecond, addOneHour } from "../services/date";

/*** CONSTS ***/
/*************/
export const FETCH_SAMPLES_ACTION = "sample:fetch";
export const RESET_SAMPLES_ACTION = "sample:reset";

export const UPDATE_REAL_TIME_TABLE_ACTION = "real:timetable:update";


/*** ACTIONS ***/
/***************/
export const fetchSamples = (startTime, endTime, measure, step, modificationId) => dispatch => {
    const toastId = toast.info("Récupération des échantillons", { autoClose: false });

    sampleAPI.apiClient.timeout = 5 * 60000; // TODO Trouver un autre moyen
    return sampleAPI.findAllUsingGET(endTime, step, measure.id, startTime, { modificationId })
    .then(samples => {
        dispatch({
            type: FETCH_SAMPLES_ACTION,
            payload: samples.map(sample => ({
                ...sample,
                measureId: measure.id,
                measure: measure.name,
                unit: measure.unit,
                frequency: measure.frequency
            }))
        });
        setTimeout(() => toast.dismiss(toastId), 1000);

        return Promise.resolve({ measure, samples });
    }).catch(e =>{
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de récupérer les échantillons, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

export const updateRealTimeTable = (measures) => (dispatch, getState) => {
    const { time, modification } = getState();
    const t = addOneHour(time.currentTime);
    sampleAPI.apiClient.timeout = 5 * 60000; // TODO Trouver un autre moyen

    const promisesTable = measures.map(measure => {
        return sampleAPI.findAllUsingGET(addTenSecond(t), 1, measure.id, t, {
            modificationId: modification.modificationID
        }).then(samples => Promise.resolve({ samples, measure }));
    });

    Promise.all(promisesTable)
        .then(values => {

            const samples = values.flatMap(value => {
                return value.samples.map(sample => ({
                    ...sample,
                    measureId: value.measure.id,
                    measure: value.measure.name,
                    unit: value.measure.unit,
                    frequency: value.measure.frequency
                }));
            });

            dispatch({
                type: UPDATE_REAL_TIME_TABLE_ACTION,
                payload: samples
            });
        })
        .catch(e => {
            console.error(e);
        });
};


/*** REDUCER ***/
/***************/
const INITIAL_SAMPLE_STATE = {
    samples: [],
    realTimeTable: []
};

export const sample = (state = INITIAL_SAMPLE_STATE, action) => {
    switch (action.type) {
        case FETCH_SAMPLES_ACTION:
            return {
                ...state,
                samples: state.samples.concat(action.payload)
            };
        case RESET_SAMPLES_ACTION:
            return {
                ...state,
                samples: []
            };
        case UPDATE_REAL_TIME_TABLE_ACTION:
            return {
                ...state,
                realTimeTable: action.payload
            };
        default:
            return state;
    }
};
