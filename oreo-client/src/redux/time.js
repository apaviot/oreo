import { FETCH_EXPERIMENT_INFO_ACTION } from "./experiment";

/*** CONSTS ***/
/*************/
export const UPDATE_TIME_ACTION = "time:update";

/*** ACTIONS ***/
/***************/

/*** REDUCER ***/
/***************/
const INITIAL_TIME_STATE = {
    startTime: new Date(),
    endTime: new Date(),
    currentTime: new Date()
};

export const time = (state = INITIAL_TIME_STATE, action) => {
    switch (action.type) {
        case FETCH_EXPERIMENT_INFO_ACTION:
            return {
                ...state,
                startTime: action.payload.startTime,
                endTime: action.payload.endTime
            };
        case UPDATE_TIME_ACTION:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
};
