import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";

import { alarm } from "./alarm";
import { bench } from "./bench";
import { campaign } from "./campaign";
import { config } from "./config";
import { experiment } from "./experiment";
import { measure } from "./measure";
import { modification } from "./modification";
import { protocol } from "./protocol";
import { sample } from "./sample";
import { sql } from "./sql";
import { time } from "./time";

const reducers = combineReducers({
    alarm,
    bench,
    campaign,
    config,
    experiment,
    measure,
    modification,    
    protocol,
    sample,
    sql,
    time
});

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
