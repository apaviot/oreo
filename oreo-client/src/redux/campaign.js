import { toast } from "react-toastify";
import { campaignAPI } from "../services/api";
import { OnError } from "../services/glamor";
import { FETCH_EXPERIMENT_INFO_ACTION } from "./experiment";

/*** CONSTS ***/
/**************/
export const FETCH_CAMPAIGN_ACTION = "campaign:fetch";

/*** ACTIONS ***/
/***************/
export const fetchCampaigns = () => dispatch => {
    const toastId = toast.info("Récupération des campagnes", { autoClose: false });

    campaignAPI.findAllCampaignUsingGET()
    .then(r => {
        dispatch({
            type: FETCH_CAMPAIGN_ACTION,
            payload: r._embedded.campaigns
        });
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de récupérer les campagnes, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

/*** REDUCER ***/
/***************/
const INITIAL_CAMPAIGN_STATE = {
    campaigns: [],
    campaign: {}
};

export const campaign = (state = INITIAL_CAMPAIGN_STATE, action) => {
    switch (action.type) {
        case FETCH_CAMPAIGN_ACTION:
            return {
                ...state,
                campaigns: action.payload
            };
        case FETCH_EXPERIMENT_INFO_ACTION:
            return {
                ...state,
                campaign: action.payload.campaign
            };
        default:
            return state;
    }
};
