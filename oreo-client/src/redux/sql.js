import { toast } from "react-toastify";
import { sqlAPI } from "../services/api";
import { OnError } from "../services/glamor";
import { downloadFile } from "../services/file";

/*** CONSTS ***/
/**************/
export const SEND_SQL_REQUEST_ACTION = "sql:send";

/*** ACTIONS ***/
/***************/
export const sendSQLRequest = request => dispatch => {
    const toastId = toast.info("Envoi de la requête SQL", { autoClose: false });
    
    sqlAPI.executeUsingPOST(request)
    .then(result => {
        dispatch({
            type: SEND_SQL_REQUEST_ACTION,
            payload: result
        });
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible d'envoyer la requête SQL, erreur serveur : " + e.response.body.message,
            className: OnError,
            autoClose: true
        });
    });
};

export const exportSQLRequest = request => dispatch => {
    const toastId = toast.info("Envoi de la requête SQL", { autoClose: false });

    sqlAPI.exportResultUsingPOSTWithHttpInfo(request)
    .then(result => {
        downloadFile(result, "file.csv", "text/csv");
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible d'envoyer la requête SQL, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

/*** REDUCER ***/
/***************/
const INITIAL_SQL_STATE = {
    sqlResult: null
};

export const sql = (state = INITIAL_SQL_STATE, action) => {
    switch (action.type) {
        case SEND_SQL_REQUEST_ACTION:
            return {
                ...state,
                sqlResult: action.payload
            };
        default:
            return state;
    }
};
