import { toast } from "react-toastify";
import { protocolAPI } from "../services/api";
import { OnError } from "../services/glamor";
import { downloadFile } from "../services/file";

/*** CONSTS ***/
/**************/
export const FETCH_PROTOCOL_ACTION = "protocol:fetch";
export const SAVE_PROTOCOL_ACTION = "protocol:save";
export const SEND_PROTOCOL_ACTION = "protocol:send";
export const CHANGE_PROTOCOL_ACTION = "protocol:change";
export const CHOOSE_PROTOCOL_ACTION = "protocol:choose";
export const DOWNLOAD_PROTOCOL_ACTION = "protocol:download";
export const COMPUTE_PROTOCOL_ACTION = "protocol:compute";

/*** ACTIONS ***/
/***************/
export const fetchProtocols = () => dispatch => {
    const toastId = toast.info("Récupération des protocoles", { autoClose: false });

    return protocolAPI.findAllProtocolUsingGET({ sort: "id,asc" })
    .then(r => {
        dispatch({
            type: FETCH_PROTOCOL_ACTION,
            payload: {
                protocols : r._embedded.protocols
            }
        });
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de récupérer les protocoles, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

export const saveProtocol = (body, id) => dispatch => {
    const toastId = toast.info("Modification du protocol", { autoClose: false });

    protocolAPI.saveUsingPUT(body, id)
    .then(() =>{
        dispatch({
            type: SAVE_PROTOCOL_ACTION,
            payload: {
                body,
                id
            }
        });
        setTimeout(() => toast.update(toastId, {
            type: toast.TYPE.SUCCESS,
            render: "Modification"
        }), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de modifier le protocol, erreur serveur : " + e.response.text,
            className: OnError,
            autoClose: true
        });
    });
};

export const sendProtocol = (experimentId, id, payload) => dispatch => {
    const toastId = toast.info("Envoi par protocol", { autoClose: false });
    
    protocolAPI.executeProtocolUsingPOST(experimentId, id, payload)
    .then(response => {
        dispatch({
            type: SEND_PROTOCOL_ACTION,
            payload: response
        });
        setTimeout(() => toast.update(toastId, {
            type: toast.TYPE.SUCCESS,
            render: "Envoyé",
            autoClose: true
        }), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible d'envoyer, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

export const stopProtocol = id => dispatch => {
    const toastId = toast.info("Arrêt du protocol", { autoClose: false });

    protocolAPI.pauseExecuteProtocolUsingPOST(id)
    .then(() => {
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible d'arréter le protocol, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

export const downloadProtocol = (id, experiment, measures) => dispatch => {
    const toastId = toast.info("Téléchargement", { autoClose: false });

    protocolAPI.exportProtocolUsingPOSTWithHttpInfo(experiment, id, { ids: measures })
    .then(result => {
        downloadFile(result, "file.xml", "application/xml");
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de télécharger, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

/*** REDUCER ***/
/***************/
const INITIAL_PROTOCOL_STATE ={
    protocols: [],
    chosenProtocol: "",
    computeTime: false,
};

export const protocol = (state = INITIAL_PROTOCOL_STATE, action) => {
    switch (action.type) {
        case FETCH_PROTOCOL_ACTION:
            return {
                ...state,
                protocols: action.payload.protocols
            };
        case CHANGE_PROTOCOL_ACTION:
            const protocols = state.protocols.slice(0).map(protocol => Object.assign({}, protocol));

            protocols[action.payload.index] = {
                ...protocols[action.payload.index],
                info: {
                    ...protocols[action.payload.index].info,
                    [action.payload.key]: action.payload.value
                }
            };
            return {
                ...state,
                protocols
            };
        case SEND_PROTOCOL_ACTION:
            return {
                ...state,
                ...action.payload
            };
        case CHOOSE_PROTOCOL_ACTION:
            return {
                ...state,
                chosenProtocol: action.payload
            };
        case COMPUTE_PROTOCOL_ACTION:
            return {
                ...state,
                computeTime : !state.computeTime
            };
        default:
            return state;
    }
};
