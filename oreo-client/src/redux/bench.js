import { toast } from "react-toastify";
import { benchAPI } from "../services/api";
import { OnError } from "../services/glamor";
import { FETCH_EXPERIMENT_INFO_ACTION } from "./experiment";

/*** CONSTS ***/
/**************/
export const FETCH_BENCH_ACTION = "bench:fetch";

/*** ACTIONS ***/
/***************/
export const fetchBenches = () => dispatch => {
    const toastId = toast.info("Récupération des bancs", { autoClose: false });

    benchAPI.findAllBenchUsingGET()
    .then(r => {
        dispatch({
            type: FETCH_BENCH_ACTION,
            payload: r._embedded.benches
        });
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de récupérer les bancs, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

/*** REDUCER ***/
/***************/
const INITIAL_BENCH_STATE = {
    benches: [],
    bench: {}
};

export const bench = (state = INITIAL_BENCH_STATE, action) => {
    switch (action.type) {
        case FETCH_BENCH_ACTION:
            return {
                ...state,
                benches: action.payload
            };
        case FETCH_EXPERIMENT_INFO_ACTION:
            return {
                ...state,
                bench: action.payload.bench
            };
        default:
            return state;
    }
};
