import { toast } from "react-toastify";
import { modificationAPI, measureAPI } from "../services/api";
import { OnError } from "../services/glamor";

/*** CONSTS ***/
/*************/
export const FETCH_MODIFICATION_ACTION = "modification:fetch";
export const DELETE_MODIFICATION_ACTION = "modification:delete";
export const UPDATE_PARTIAL_MODIFICATIONS_ACTION = "partial:modification:update";
export const DELETE_PARTIAL_MODIFICATION_ACTION = "partial:modification:delete";

export const TOGGLE_PARTIAL_MODIFICATION_ACTION = "partial:modification:toggle";
export const TOGGLE_DETAILS_MODIFICATION_ACTION = "details:modification:toggle";

export const UPDATE_MODIFICATION_ID = "modification:id:update";

export const FETCH_MEASURES_MODIFIED_NAMES_ACTION = "modified:measures:name:fetch";


/*** ACTIONS ***/
/***************/
export const fetchModifications = id => dispatch => {
    const toastId = toast.info("Récupération des modifications", { autoClose: false });

    modificationAPI.findByExperimentIdModificationUsingGET({ experimentId : id })
    .then(r => {
        dispatch({
            type: FETCH_MODIFICATION_ACTION,
            payload: r._embedded.modifications
        });
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de récupérer les modifications, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

export const deleteModification = id => dispatch => {
    const toastId = toast.info("Suppression de la modification", { autoClose: false });

    modificationAPI.deleteModificationUsingDELETE(id)
    .then(() => {
        dispatch({
            type: DELETE_MODIFICATION_ACTION,
            payload: id
        });
        setTimeout(() => toast.update(toastId, {
            type: toast.TYPE.SUCCESS,
            render: "Supprimée",
            autoClose: true
        }), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de supprimer la modification, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

export const createModification = opts => dispatch => {
    const toastId = toast.info("Création de la modification", { autoClose: false });
    modificationAPI.saveModificationUsingPOST(opts)
    .then(() => {
        dispatch(fetchModifications(opts.experimentId));
        setTimeout(() => toast.update(toastId, {
            type: toast.TYPE.SUCCESS,
            render: "Créée",
            autoClose: true
        }), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de supprimer la modification, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

export const fetchMeasuresModifiedNames = modifications => dispatch => {
    const toastId = toast.info("Récupération du nom des mesures", { autoClose: false });

    const promisesTable = modifications.flatMap(e =>
         e.partials.map(p =>
            measureAPI.findByIdMeasureUsingGET({ measureId: p.measureId })
        )
    );

    Promise.all(promisesTable)
    .then(values => {
        dispatch({
            type: FETCH_MEASURES_MODIFIED_NAMES_ACTION,
            payload: values
        });
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de récupérer les noms des mesures, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};


/*** REDUCER ***/
/***************/
const INITIAL_MODIFICATION_STATE = {
    partial: {
        showPartial : "",
        partials: []
    },
    global: [],
    modificationID: null
};

export const modification = (state = INITIAL_MODIFICATION_STATE, action) => {
    switch (action.type) {
        case FETCH_MODIFICATION_ACTION:
            action.payload.forEach(e => e.showPartial = "");
            return {
                ...state,
                global: action.payload
            };
        case DELETE_MODIFICATION_ACTION:
            return {
                ...state,
                global: state.global.filter(modification => modification.id !== action.payload),
                modificationID: state.modificationID === action.payload ? null : state.modificationID
            };
        case UPDATE_PARTIAL_MODIFICATIONS_ACTION:
            return {
                ...state,
                partial: {
                    ...state.partial,
                    partials: [...state.partial.partials, action.payload]
                }
            };
        case DELETE_PARTIAL_MODIFICATION_ACTION:
            return {
                ...state,
                partial: {
                    ...state.partial,
                    partials: state.partial.partials.filter(e => state.partial.partials.indexOf(e) !== action.payload)
                }
            };
        case TOGGLE_DETAILS_MODIFICATION_ACTION:
            const mod = state.global.map(e => {
                if (e === action.payload && e.showPartial === "") {
                    e.showPartial = "is-visible";
                } else {
                    e.showPartial = "";
                }
                return e;
            });
            
            return {
                ...state,
                partial: {
                    showPartial : "",
                    partials: []
                },
                global: mod
            };
        case TOGGLE_PARTIAL_MODIFICATION_ACTION:
            const mods = state.global.map(e => {
                e.showPartial = "";
                return e;
            });

            return {
                ...state,
                partial:{
                    showPartial : state.partial.showPartial === "is-visible" ?  "" : "is-visible",
                    partials: []
                },
                global: mods
            };
        case UPDATE_MODIFICATION_ID:
            return {
                ...state,
                modificationID: action.payload
            };
        case FETCH_MEASURES_MODIFIED_NAMES_ACTION:
            const modifs = state.global.map((modif, i) => {
                modif.partials = modif.partials.map(p => {
                    p.name = action.payload[i].name;
                    return p;
                });
                return modif;
            });

            return {
                ...state,
                global: modifs
            };
        default:
            return state;
    }
};
