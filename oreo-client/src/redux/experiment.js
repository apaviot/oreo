import { toast } from "react-toastify";
import { experimentAPI, campaignAPI, benchAPI, alarmAPI } from "../services/api";
import { OnError } from "../services/glamor";

/*** CONSTS ***/
/*************/
export const FETCH_EXPERIMENT_ACTION = "experiment:fetch";
export const DELETE_EXPERIMENT_ACTION = "experiment:delete";
export const FETCH_EXPERIMENT_INFO_ACTION = "experiment:info:fetch";
export const PLAY_STATE_ACTION = "play:set";

/*** ACTIONS ***/
/***************/
export const fetchExperimentInfo = id => dispatch => {
    const toastId = toast.info("Récupération des données du test", { autoClose: false });

    return experimentAPI.findOneExperimentUsingGET(id)
        .then(experiment => {
            const promiseCampaign = campaignAPI.findOneCampaignUsingGET(experiment.campaignId);
            const promiseBench = benchAPI.findOneBenchUsingGET(experiment.benchId);
            const promiseAlarm = alarmAPI.findByExperimentIdAlarmUsingGET({ experimentId: id });

            return Promise.all([promiseCampaign, promiseBench, promiseAlarm, experiment]);
        })
        .then(values => {
            const [ campaign, bench, alarms, experiment ] = values;
            const firstAlarm = alarms._embedded.alarms.length > 0 ? alarms._embedded.alarms[0].time : experiment.begin;
            const startTime = firstAlarm;

            dispatch({
                type: FETCH_EXPERIMENT_INFO_ACTION,
                payload: {
                    experiment,
                    campaign,
                    bench,
                    alarms: alarms._embedded.alarms,
                    startTime,
                    endTime: experiment.end
                }
            });
            setTimeout(() => toast.dismiss(toastId), 1000);
        })
        .catch(e => {
            console.error(e);
            toast.update(toastId, {
                type: toast.TYPE.ERROR,
                render: "Impossible de récupérer les informations du test, erreur serveur",
                className: OnError,
                autoClose: true
            });
        });
};

export const fetchExperiments = opts => dispatch => {
    const toastId = toast.info("Récupération des tests", { autoClose: false });
    
    experimentAPI.findAllExperimentUsingGETWithHttpInfo({ sort: "id", ...opts })
    .then(r => {
        const data = r.response.body;
        dispatch({
            type: FETCH_EXPERIMENT_ACTION,
            payload: {
                experiments: data._embedded.experiments,
                page: data.page
            }
        });
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de récupérer les tests, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

export const searchExperiments = opts => dispatch => {
    const toastId = toast.info("Récupération de la recherche", { autoClose: false });

    experimentAPI.advancedSearchUsingGET(opts)
    .then(result => {
        dispatch({
            type: FETCH_EXPERIMENT_ACTION,
            payload: {
                experiments: result._embedded.experiments,
                page: result.page
            }
        });
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de récupérer la recherche, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

export const deleteExperiment = id => dispatch => {
    const toastId = toast.info("Suppresion du test", { autoClose: false });

    experimentAPI.deleteExperimentUsingDELETE(id)
    .then(() => {
        dispatch({
            type: DELETE_EXPERIMENT_ACTION,
            payload: id
        });
        setTimeout(() => toast.update(toastId, {
            type: toast.TYPE.SUCCESS,
            render: "Suppression",
            autoClose: true
        }), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de supprimer le test, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

/*** REDUCER ***/
/***************/
const INITIAL_EXPERIMENT_STATE = {
    experiments: [],
    page: {
        totalPages: 1,
        number: 0
    },
    experiment: {},
    playState: false
};

export const experiment = (state = INITIAL_EXPERIMENT_STATE, action) => {
    switch (action.type) {
        case FETCH_EXPERIMENT_ACTION:
            return {
                ...state,
                experiments: action.payload.experiments,
                page: action.payload.page
            };
        case DELETE_EXPERIMENT_ACTION:
            return {
                ...state,
                experiments: state.experiments.filter(experiment => experiment.id !== action.payload)
            };
        case FETCH_EXPERIMENT_INFO_ACTION:
            return {
                ...state,
                experiment: action.payload.experiment
            };
        case PLAY_STATE_ACTION:
            return {
                ...state,
                playState: action.payload
            };
        default:
            return state;
    }
};
