import { toast } from "react-toastify";
import { measureAPI } from "../services/api";
import { OnError } from "../services/glamor";

/*** CONSTS ***/
/*************/
export const UPDATE_MEASURE_ACTION = "measure:update";
export const SEARCH_MEASURE_ACTION = "measure:search";

/*** ACTIONS ***/
/***************/
export const searchMeasures = (experimentID, opts) => dispatch => {
    const toastId = toast.info("Récupération des mesures", { autoClose: false });


    measureAPI.advancedSearchUsingGET1(experimentID, { ...opts, size: 10 })
    .then(result => {
        dispatch({
            type: SEARCH_MEASURE_ACTION,
            payload: {
                measures: result._embedded.measures,
                page: result.page
            }
        });
        setTimeout(() => toast.dismiss(toastId), 1000);
    })
    .catch(e => {
        console.error(e);
        toast.update(toastId, {
            type: toast.TYPE.ERROR,
            render: "Impossible de récupérer les mesures, erreur serveur",
            className: OnError,
            autoClose: true
        });
    });
};

/*** REDUCER ***/
/***************/
const INITIAL_MEASURE_STATE = {
    measures: [],
    page: {
        totalPages: 1,
        number: 0
    },
    selectedMeasures: []
};

export const measure = (state = INITIAL_MEASURE_STATE, action) => {
    switch (action.type) {
        case UPDATE_MEASURE_ACTION:
            return {
                ...state,
                selectedMeasures: action.payload.slice()
            };
        case SEARCH_MEASURE_ACTION:
            return {
                ...state,
                measures: action.payload.measures === undefined ? [] : action.payload.measures,
                page: action.payload.page
            };
        default:
            return state;
    }
};
