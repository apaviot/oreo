import * as ApiDoc from "../sdk";

const LOCAL_KEY = "API_URL";
const CLIENT = new ApiDoc.ApiClient();
CLIENT.basePath = localStorage.getItem(LOCAL_KEY) ? localStorage.getItem(LOCAL_KEY) : CLIENT.basePath;

export const experimentAPI = new ApiDoc.ExperimentEntityApi(CLIENT);
export const campaignAPI = new ApiDoc.CampaignEntityApi(CLIENT);
export const benchAPI = new ApiDoc.BenchEntityApi(CLIENT);
export const measureAPI = new ApiDoc.MeasureEntityApi(CLIENT);
export const alarmAPI = new ApiDoc.AlarmEntityApi(CLIENT);
export const protocolAPI = new ApiDoc.ProtocolEntityApi(CLIENT);
export const sqlAPI = new ApiDoc.SQLEntityApi(CLIENT);
export const sampleAPI = new ApiDoc.SampleEntityApi(CLIENT);
export const modificationAPI = new ApiDoc.ModificationEntityApi(CLIENT);

/**
 * Retourne l'url de l'API
 * @returns {*}
 */
export const getAPIURL = () => {
    return CLIENT.basePath;
};

/**
 * Defini l'url de l'API
 * @param apiURL
 */
export const setAPIURL = apiURL => {
    CLIENT.basePath = apiURL;
    localStorage.setItem(LOCAL_KEY, apiURL);
};
