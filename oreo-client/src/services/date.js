import { isEqual, format, parse, isAfter, addSeconds, addHours } from "date-fns";
import * as frenchLocale from "date-fns/locale/fr";

/**
 * Permet de convertir une Date en String (avec un T au milieu)
 * @param String
 * @returns {string | *}
 */
export const parseDate = date => {
    return format(date, "YYYY-MM-DDTHH:mm:ss.SSS", { locale: frenchLocale });
};

/**
 * Permet de créer une Date avec les jours/mois/années d'une date et les heures/minutes/secondes d'une autre
 * @param date On se servira du jour
 * @param time On se servira de l'heure
 * @returns {Date | *}
 */
export const getDateFromDateHours = (date, time) => {
    return parse(format(date, "YYYY-MM-DDT", { locale: frenchLocale }) + time);
};

/**
 * Permet de convertir une Date en String (en ne gardant que heure/minutes/secondes/millis)
 * @param date
 * @returns {string | *}
 */
export const convertDateToString = (date) => {
    return format(date, "HH:mm:ss.SSS", { locale: frenchLocale });
};

/**
 * Permet de comparer deux heures, sans comparer les milliseconds
 * @param time
 * @param newTime
 * @returns {boolean | *}
 */
export const compareTime = (time, newTime) => {
    return isEqual(new Date(time.getTime()).setMilliseconds(0), new Date(newTime.getTime()).setMilliseconds(0));
};

/**
 * Retour vrai si la premiere heure est inferieur a la seconde
 * @param time1
 * @param time2
 */
export const checkHoursGrowing = (time1, time2) => {
    const date1 = parse(format(new Date(), "YYYY-MM-DDT", { locale: frenchLocale }) + time1);
    const date2 = parse(format(new Date(), "YYYY-MM-DDT", { locale: frenchLocale }) + time2);
    return isAfter(date2, date1);
};

export const addTenSecond = date => {
    return parse(format(addSeconds(date, 10), "YYYY-MM-DDTHH:mm:ss.SSSZ", { locale: frenchLocale }));
};

export const addOneHour = date => addHours(date, 1);

export const checkDateGrowing = (date1, date2) => {
    return isAfter(date1, date2);
};

