/**
 * Recupère le nom du fichier a partir du header de la reponse HTTP
 * @param header
 * @param defaultFilename
 * @returns {*}
 */
const extractFilename = (header, defaultFilename) => {
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(header);
    if (matches != null && matches[1]) {
        return matches[1].replace(/['"]/g, "");
    }
    return defaultFilename;
};

/**
 * Telecharge le fichier issu d'une reponse HTTP
 * @param result
 * @param defaultFilename
 * @param type
 */
export const downloadFile = (result, defaultFilename, type) => {
    const response = result.response;
    const filename = extractFilename(response.headers["content-disposition"], defaultFilename);
    const url = window.URL.createObjectURL(new Blob(
        [response.text],
        { type }
    ));

    const a = document.createElement("a");
    a.href = url;
    a.download = filename;
    a.click();
    window.URL.revokeObjectURL(url);
};
