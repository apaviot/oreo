import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";

import store from "../redux/store";
import { PLAY_STATE_ACTION } from "../redux/experiment";
import { UPDATE_TIME_ACTION } from "../redux/time";
import { RESET_SAMPLES_ACTION, fetchSamples } from "../redux/sample";

import { parseDate } from "./date.js";

/**
 * Class permettant la gestion complete du graphique
 */
export default class ChartService {
    chart;
    alarmsSeries;
    measuresSeries = [];
    timelineSeries;
    interval;

    /**
     * Initialisation du graphique
     */
    init() {
        const state = store.getState();
        const { startTime, endTime } = state.time;
        const alarms = state.alarm.alarms;

        this._initChart();
        this._initAxes(startTime, endTime);
        this._initScrollbar();
        this._initTimeline(startTime);
        this._initAlarms(alarms);
    }

    /**
     * Met a jour les mesures sélectionnées dans le graphique
     */
    update() {
        if (!this.chart) {
            return;
        }

        this._flush();
        const state = store.getState();
        const experiment = state.experiment.experiment;
        const modificationID  = state.modification.modificationID;
        const selectedMeasures = state.measure.selectedMeasures;

        selectedMeasures.forEach(measure => {
            const begin = parseDate(experiment.begin);
            const end = parseDate(experiment.end);
            const time = new Date(end).getTime() - new Date(begin).getTime();
            const step = Math.round(time / this.chart.pixelWidth);

            store.dispatch(fetchSamples(parseDate(experiment.begin), parseDate(experiment.end), measure, step, modificationID))
            .then(({ measure, samples }) => this._initMeasure(measure, samples));
        });
    }

    /**
     * Demarre la timeline (actualisation toutes les secondes)
     */
    startTimeline() {
        store.dispatch({
            type: PLAY_STATE_ACTION,
            payload : true
        });
        const { startTime, endTime } = store.getState().time;
        const speed = 1000;

        this.timelineSeries.data[0].time = startTime;
        this.chart.scrollbarX.interactionsEnabled = false;
        this.chart.interactionsEnabled = false;
        this.timelineSeries.disabled = false;

        this.interval = setInterval(() => {
            const oldTime = this.timelineSeries.data[0].time;
            const newTime = new Date(oldTime);
            newTime.setSeconds(newTime.getSeconds() + 1);
            this.timelineSeries.data[0].time = newTime;
            this.timelineSeries.invalidateData();

            store.dispatch({
                type: UPDATE_TIME_ACTION,
                payload: {
                    currentTime: newTime
                }
            });

            if (newTime.getTime() >= endTime.getTime()) {
                this.stopTimeline();
            }
        }, speed);
    }

    /**
     * Arrete la timeline
     */
    stopTimeline() {
        clearInterval(this.interval);
        this.chart.scrollbarX.interactionsEnabled = true;
        this.chart.interactionsEnabled = true;
        store.dispatch({
            type: PLAY_STATE_ACTION,
            payload : false
        });

    }

    /**
     * Suppression du graphique
     */
    destroy() {
        if (this.chart) {
            this.chart.dispose();
        }
    }

    /**
     * Initialisation du graphique (couleurs, legende, curseur, ..) et de la scrollbar
     * @private
     */
    _initChart() {
        this.chart = am4core.create("chartdiv", am4charts.XYChart);
        this.chart.colors.list = [
            am4core.color("#357AB7"),
            am4core.color("#99512B"),
            am4core.color("#6C0277")
        ];
        this.chart.scrollbarX = new am4charts.XYChartScrollbar();
        this.chart.legend = new am4charts.Legend();
        this.chart.cursor = new am4charts.XYCursor();
        this.chart.series.events.on("removed", () => {
            this.timelineSeries.disabled = true;
            this.chart.invalidateData();
        });
    }

    /**
     * Initialisation de l'axe des abscisses
     * @param startTime
     * @param endTime
     * @private
     */
    _initAxes(startTime, endTime) {
        const dateAxis = this.chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.minGridDistance = 70;
        dateAxis.baseInterval = { count: 1, timeUnit : "second" };
        dateAxis.min = startTime;
        dateAxis.max = endTime;
        dateAxis.events.on("extremeschanged", this._onUp.bind(this));
    }

    /**
     * Initialisation de la scrollbar
     * @private
     */
    _initScrollbar() {
        this.chart.scrollbarX.marginBottom = 20;
        this.chart.scrollbarX.events.on("up", this._onUp.bind(this));
    }

    /**
     * Initialisation de la timeline
     * @param startTime
     * @private
     */
    _initTimeline(startTime) {
        const valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());

        this.timelineSeries = this.chart.series.push(new am4charts.ColumnSeries());
        this.timelineSeries.dataFields.valueY = "timeline";
        this.timelineSeries.dataFields.dateX = "time";
        this.timelineSeries.strokeWidth = 2;
        this.timelineSeries.yAxis = valueAxis;
        this.timelineSeries.name = "Timeline";
        this.timelineSeries.tooltipText = "{name}";
        this.timelineSeries.interpolationDuration = 500;
        this.timelineSeries.defaultState.transitionDuration = 0;
        this.timelineSeries.columns.template.width = am4core.percent(4);
        this.timelineSeries.data = [{
            time: startTime,
            timeline: 10
        }];
        this.timelineSeries.disabled = true;
        this.timelineSeries.fill = am4core.color("#59E82A");
        this.timelineSeries.stroke = am4core.color("#59E82A");

        valueAxis.visible = false;
        valueAxis.renderer.grid.template.disabled = true;
        valueAxis.renderer.opposite = true;
        valueAxis.interpolationDuration = 500;
        valueAxis.rangeChangeDuration = 500;
        valueAxis.min = 0;
        valueAxis.max = 5;
    }

    /**
     * Initialisation des alarmes
     * @param alarms
     * @private
     */
    _initAlarms(alarms) {
        const valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());

        this.alarmsSeries = this.chart.series.push(new am4charts.ColumnSeries());
        this.alarmsSeries.dataFields.valueY = "alarm";
        this.alarmsSeries.dataFields.dateX = "time";
        this.alarmsSeries.strokeWidth = 2;
        this.alarmsSeries.yAxis = valueAxis;
        this.alarmsSeries.name = "Alarm";
        this.alarmsSeries.tooltipHTML = `<center><strong>{name}</strong></center>
            <hr />
            <table>
            <tr>
              <th align="left">Level</th>
              <td>{valueY}</td>
            </tr>
            <tr>
              <th align="left">State</th>
              <td>{state}</td>
            </tr>
            <tr>
              <th align="left">Message</th>
              <td>{message}</td>
            </tr>
            <tr>
              <th align="left">Order</th>
              <td>{order}</td>
            </tr>
            </table>
            <hr />`;
        this.alarmsSeries.data = this._normalizeAlarms(alarms);
        this.alarmsSeries.columns.template.width = am4core.percent(4);
        this.alarmsSeries.columns.template.adapter.add("stroke", (stroke, target) => {
            return this._alarmColorPick(target);
        });

        this.alarmsSeries.columns.template.adapter.add("fill", (fill, target) => {
            return this._alarmColorPick(target);
        });

        valueAxis.renderer.line.strokeOpacity = 1;
        valueAxis.renderer.line.strokeWidth = 2;
        valueAxis.renderer.line.stroke = am4core.color("#000000");
        valueAxis.renderer.labels.template.fill = am4core.color("#000000");
        valueAxis.renderer.opposite = true;
        valueAxis.maxPrecision = 0;
        valueAxis.min = 0;
        valueAxis.max = 4;

        this.chart.scrollbarX.series.push(this.alarmsSeries);
    }

    /**
     * Choix de la couleur en fonction de la valeur de l'alarme
     * @param target
     * @returns {Color}
     * @private
     */
    _alarmColorPick(target) {
        switch (target.dataItem.valueY) {
            case 1:
                return am4core.color("#787878");
            case 2:
                return am4core.color("#e8e800");
            case 3:
                return am4core.color("#ff0000");
            default:
                return am4core.color("#000000");
        }
    }

    /**
     * Initialisation d'une mesure
     * @param measure
     * @param samples
     * @private
     */
    _initMeasure(measure, samples) {
        const valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());

        const series = this.chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = measure.name;
        series.dataFields.dateX = "time";
        series.strokeWidth = 2;
        series.yAxis = valueAxis;
        series.name = measure.name;
        series.tooltipHTML = `<center><strong>{name}</strong></br>
            <b align="center">{valueY}&nbsp;{unit}</b></center>`;
        series.tensionX = 1;
        series.data = this._normalizeSamples(measure.name, measure.unit, samples);
        series.fill = this.chart.colors.list[(this.chart.series.length - 3) % 3]; // -3 car on a Timeline, alarme de base
        series.stroke = this.chart.colors.list[(this.chart.series.length - 3) % 3]; // %3 car on a que 3 couleurs max

        valueAxis.renderer.line.strokeOpacity = 1;
        valueAxis.renderer.line.strokeWidth = 2;
        valueAxis.renderer.line.stroke = series.stroke;
        valueAxis.renderer.labels.template.fill = series.stroke;
        valueAxis.renderer.opposite = false;
        valueAxis.maxPrecision = 0;

        this.measuresSeries.push(series);
    }

    /**
     * Mise en forme de la liste des alarmes
     * @param alarms
     * @returns {*}
     * @private
     */
    _normalizeAlarms(alarms) {
        return alarms.map(alarm => ({
            time: new Date(alarm.time).setMilliseconds(alarm.time.getMilliseconds()),
            alarm: alarm.level,
            message: alarm.message,
            name: alarm.name,
            order: alarm.order,
            reference: alarm.reference,
            state: alarm.state
        }));
    }

    /**
     * Mise en forme de la liste des échantillons d'une mesure
     * @param name
     * @param unit
     * @param samples
     * @returns {*}
     * @private
     */
    _normalizeSamples(name, unit, samples) {
        return samples.map(sample => ({
            time: new Date(sample.time),
            [name]: sample.value,
            unit
        }));
    }

    /**
     * Vide les données liée aux mesures du graphique
     * @private
     */
    _flush() {
        // TODO : check if measure already loaded

        this.measuresSeries.forEach(series => {
            const i = this.chart.series.indexOf(series);
            this.chart.series.removeIndex(i).dispose();
            this.chart.yAxes.removeIndex(i).dispose();
        });
        this.measuresSeries = [];
        store.dispatch({ type: RESET_SAMPLES_ACTION });
    }

    /**
     * Mise a jour des input text via la scrollbar
     * @private
     */
    _onUp() {
        const xAxis = this.chart.xAxes.getIndex(0);
        const start = this.chart.scrollbarX.range.start;
        const end = this.chart.scrollbarX.range.end;
        const startTime = xAxis.positionToDate(start);
        const endTime = xAxis.positionToDate(end);

        if ((startTime instanceof Date && isNaN(startTime)) || (endTime instanceof Date && isNaN(endTime))) {
           return;
        }

        store.dispatch({
            type: UPDATE_TIME_ACTION,
            payload: {
                startTime,
                endTime
            }
        });
    }

    /**
     * Mise à jour des brones de la scrollbar via les input text
     */
    updateScrollbar() {
        const { startTime, endTime } = store.getState().time;
        if (this.chart !== undefined && this.chart.xAxes !== undefined ) {
            this.chart.xAxes.getIndex(0).zoomToDates(startTime, endTime);
        }
    }
}
