import { css } from "glamor";

/**
 * Permet d'affecter du CSS a une erreur
 * @type {StyleAttribute}
 */
export const OnError = css({
    transform: "rotateY(360deg)",
    transition: "transform 0.6s"
});
