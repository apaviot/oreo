# Oreo

Projet réalisé dans le cadre du Last Project de dernière année.

**Client** (Groupe SAFRAN)

- David MAILLARD
- Celine PERILLOUS

**Maîtrise d'oeuvre** (ESIPE)
- Eddy TRAN
- Robert OGRYZEK (responsable communication)
- Greg BEAUJOIS
- Jean Baptiste CECCALD
- Adrien PAVIOT (expert technique)
- Vincent RASQUIER
- Adrien ROUGIER

