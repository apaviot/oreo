C=oreo-client
S=oreo-server
D=oreo-db
R=${S}/src/main/resources
BUILD=${C}/build
TARGET=${S}/target
STATIC=${R}/static
URL=http://localhost:8888/v2/api-docs
SDK=${C}/src/sdk
USER=oreo
DATABASE=oreo
JAR=oreo-deploy.jar

help:
	@echo "services: all, client, server, db"
	@echo "*-up: start specified service in docker"
	@echo "*-down: stop specified service in docker"
	@echo "server-restart: restart server (with DB) in docker"
	@echo "*-log: log specified service"
	@echo "prepare: prepare for docker"
	@echo "build-docker: use docker to build jar"
	@echo "build-local: use local to build jar"
	@echo "build-clean: clean build folders and jar"
	@echo "sdk: build sdk, server should be start"

all-up:
	make client-up
	make server-up

client-up:
	cd ${C} && docker-compose up -d

server-up:
	cd ${S} && docker-compose up -d

db-up:
	cd ${S} && docker-compose -f docker-compose.db.yml up -d

all-down:
	make client-down
	make server-down

client-down:
	cd ${C} && docker-compose down

server-down:
	cd ${S} && docker-compose down

db-down:
	cd ${S} && docker-compose -f docker-compose.db.yml down

all-restart:
	make all-down
	make all-up

client-restart:
	make client-down
	make client-up

server-restart:
	docker stop ${S}
	make server-up

db-restart:
	make db-down
	make db-up

client-log:
	docker logs -f ${C}

server-log:
	docker logs -f ${S}

db-log:
	docker logs -f ${D}

db-init:
	make db-copy
	-make db-remove
	make db-create
	make db-insert

db-copy:
	docker cp ./scripts ${D}:./scripts

db-remove:
	docker exec ${D} psql -U ${USER} -d ${DATABASE} -f ./scripts/remove.sql

db-create:
	docker exec ${D} psql -U ${USER} -d ${DATABASE} -f ./scripts/create.sql

db-insert:
	docker exec ${D} psql -U ${USER} -d ${DATABASE} -f ./scripts/insert.sql

prepare:
	chmod +x ${S}/run.sh
	chmod +x ${S}/run-prod.sh
	cp ${R}/application.properties.dist ${R}/application.properties

build-docker:
	make build-clean
	cd ${C} && docker-compose -f docker-compose.prod.yml up --build
	cp -r ${BUILD} ${STATIC}
	cd ${S} && docker-compose -f docker-compose.prod.yml up --build
	cp ${TARGET}/oreo-0.0.1-SNAPSHOT.jar oreo-deploy.jar
	
build-local:
	make build-clean
	cd ${C} && npm run build
	cp -r ${BUILD} ${STATIC}
	cd ${S} && mvn package -Dmaven.test.skip=true
	cp ${TARGET}/oreo-0.0.1-SNAPSHOT.jar ${JAR}

build-clean:
	-rm -rf ${BUILD}
	-rm -rf ${TARGET}
	-rm -rf ${STATIC}
	-rm ${JAR}

sdk:
	java -jar oreo-server/swagger-codegen.jar generate -i ${URL} -l javascript -o swagger-cli --additional-properties usePromises=true,useES6=true
	rm -rf ${SDK}
	cp -r swagger-cli/src ${SDK}
	rm -rf swagger-cli

sdk-docker:
	docker exec oreo-server java -jar swagger-codegen.jar generate -i ${URL} -l javascript -o swagger-cli --additional-properties usePromises=true,useES6=true
	rm -rf ${SDK}
	cp -r oreo-server/swagger-cli/src ${SDK}
	rm -rf oreo-server/swagger-cli
