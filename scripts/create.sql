CREATE TABLE campaigns (
	id BIGSERIAL,
	id12c CHAR(12) NOT NULL,
	CONSTRAINT PK_Campaign PRIMARY KEY (id)
);

CREATE TABLE benches (
	id BIGSERIAL,
	reference VARCHAR(100),
	name VARCHAR(100) NOT NULL,
	CONSTRAINT PK_Bench PRIMARY KEY (id)
);

CREATE TABLE experiments (
	id BIGSERIAL,
	reference VARCHAR(100) NOT NULL,
	name VARCHAR(100) NOT NULL,
	last_used timestamp,
	begin_time timestamp,
	end_time timestamp,
	is_local BOOLEAN NOT NULL,
	campaign_id BIGINT NOT NULL,
	bench_id BIGINT NOT NULL,
	CONSTRAINT PK_Experiment PRIMARY KEY (id)
);

CREATE TABLE modifications (
	id BIGSERIAL,
	name VARCHAR(100) NOT NULL,
	creation_date timestamp NOT NULL,
	last_use timestamp,
	description VARCHAR(200) NOT NULL,
	is_remote BOOLEAN NOT NULL,
	experiment_id BIGINT NOT NULL,
	CONSTRAINT PK_Modification PRIMARY KEY (id)
);

CREATE TABLE partial_modifications (
	id BIGSERIAL,
	started_date timestamp NOT NULL,
	ended_date timestamp NOT NULL,
	operator decimal NOT NULL,
	operation VARCHAR(10) NOT NULL,
	measure_id BIGINT NOT NULL,
	modification_id BIGINT,
	CONSTRAINT PK_Partial_Modification PRIMARY KEY (id)
);

CREATE TABLE measures (
	id BIGSERIAL,
	experiment_increment BIGINT NOT NULL,
	name VARCHAR(100) NOT NULL,
	unit VARCHAR(100) NOT NULL,
	frequency INT NOT NULL,
	experiment_id BIGINT NOT NULL,
	CONSTRAINT PK_Measure PRIMARY KEY (id)
);

CREATE TABLE samples (
	id BIGSERIAL,
	value DECIMAL not null,
	sample_time timestamp NOT NULL,
	experiment_id BIGINT NOT NULL,
	experiment_increment BIGINT NOT NULL,
	CONSTRAINT PK_Sample PRIMARY KEY (id)
);


CREATE TABLE alarms (
	id BIGSERIAL,
	name VARCHAR(100),
	reference VARCHAR(100),
	state VARCHAR(20),
	message VARCHAR(100),
	level INT NOT NULL,
	alarm_time timestamp NOT NULL,
	alarm_order VARCHAR(100),
	experiment_id BIGINT NOT NULL,
	CONSTRAINT PK_Alarm PRIMARY KEY (id)
);

CREATE TABLE protocols (
	id VARCHAR(10) NOT NULL PRIMARY KEY,
	info TEXT NOT NULL
);

ALTER TABLE experiments ADD CONSTRAINT fk_campaigns FOREIGN KEY (campaign_id) REFERENCES campaigns(id); 
ALTER TABLE experiments ADD CONSTRAINT fk_benches FOREIGN KEY (bench_id) REFERENCES benches(id); 
ALTER TABLE modifications ADD CONSTRAINT fk_experiments FOREIGN KEY (experiment_id) REFERENCES experiments(id) ON DELETE CASCADE; 
ALTER TABLE alarms ADD CONSTRAINT fk_alarms FOREIGN KEY (experiment_id) REFERENCES experiments(id) ON DELETE CASCADE; 
ALTER TABLE measures ADD CONSTRAINT fk_experiment FOREIGN KEY (experiment_id) REFERENCES experiments(id) ON DELETE CASCADE; 
ALTER TABLE partial_modifications ADD CONSTRAINT fk_modification FOREIGN KEY (modification_id) REFERENCES modifications(id) ON DELETE CASCADE; 
ALTER TABLE partial_modifications ADD CONSTRAINT fk_measure FOREIGN KEY (measure_id) REFERENCES measures(id) ON DELETE CASCADE;

CREATE INDEX experiments_begin_index ON experiments (begin_time);
CREATE INDEX alarms_time_index ON alarms (alarm_time);  
CREATE INDEX samples_time_index ON samples (sample_time);
CREATE INDEX measure_experiment_increment_index ON measures (experiment_increment);
CREATE INDEX sample_experiment_increment_index ON samples (experiment_increment);
Set timezone = "Europe/Paris"
