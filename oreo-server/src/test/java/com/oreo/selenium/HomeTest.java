package com.oreo.selenium;

import com.oreo.OreoApplication;
import com.oreo.unitary.TestUtils;
import com.sun.jna.platform.win32.Sspi;
import org.hibernate.dialect.Dialect;
import org.hibernate.sql.Select;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class)
public class HomeTest {

    public ChromeDriver launchPage(){
        try {
            ChromeDriver chromeDriver = new ChromeDriver(new ChromeOptions().addArguments("--no-sandbox"));
            chromeDriver.manage().timeouts().pageLoadTimeout(1, TimeUnit.MINUTES);
            chromeDriver.get(TestUtils.clientUrl(3000, "/"));
            Thread.sleep(3000);
            return chromeDriver;
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        }
    }

    @Test
    public void testSearchFirstExperiment() throws InterruptedException {
        ChromeDriver chromeDriver = launchPage();
        List<WebElement> displayList = chromeDriver.findElements(By.className("experiments"));
        Thread.sleep(100);
        assertFalse(displayList.isEmpty());
        chromeDriver.findElement(By.className("form-control")).sendKeys(displayList.get(0).getText());
        chromeDriver.findElement(By.className("btn-primary")).click();
        WebElement searchDisplayElement = chromeDriver.findElement(By.className("experiments"));
        assertEquals(displayList.get(0).getText(),searchDisplayElement.getText());
        chromeDriver.close();
        chromeDriver.quit();
    }

    @Test
    public void testManySearch() throws InterruptedException {
        ChromeDriver chromeDriver = launchPage();
        List<String> displayList = new ArrayList<>();
        chromeDriver.findElements(By.className("experiments")).forEach(el->displayList.add(el.getText()));
        for(int i=0;i<displayList.size();i++){
            chromeDriver.findElement(By.className("form-control")).clear();
            chromeDriver.findElement(By.className("form-control")).sendKeys(displayList.get(i));
            Thread.sleep(2000);
            chromeDriver.findElement(By.className("btn-primary")).click();
            assertEquals(displayList.get(i),chromeDriver.findElement(By.className("experiments")).getText());
        }
        chromeDriver.close();
        chromeDriver.quit();
    }

    @Test
    public void testSearchGlobalExperimentAdvanced() {
        ChromeDriver chromeDriver = launchPage();
        long nb = chromeDriver.findElements(By.className("badge-info")).stream().filter(el -> el.getText().equals("Globale")).count();
        chromeDriver.findElements(By.className("float-right")).get(1).click();
        int size = chromeDriver.findElements(By.className("form-control")).size();
        chromeDriver.findElements(By.className("form-control")).get(size-1).findElements(By.tagName("option")).get(2).click();
        chromeDriver.findElement(By.className("btn-primary")).click();
        if(nb!=0) {
            assertEquals(nb, chromeDriver.findElements(By.className("badge-info")).stream().filter(el -> el.getText().equals("Globale")).count());
        }
        chromeDriver.close();
        chromeDriver.quit();
    }

    @Test
    public void testSearchLocaleExperimentAdvanced() {
        ChromeDriver chromeDriver = launchPage();
        long nb = chromeDriver.findElements(By.className("badge-info")).stream().filter(el -> el.getText().equals("Locale")).count();
        chromeDriver.findElements(By.className("float-right")).get(1).click();
        int size = chromeDriver.findElements(By.className("form-control")).size();
        chromeDriver.findElements(By.className("form-control")).get(size-1).findElements(By.tagName("option")).get(1).click();
        chromeDriver.findElement(By.className("btn-primary")).click();
        if(nb!=0){
            assertEquals(nb,chromeDriver.findElements(By.className("badge-info")).stream().filter(el -> el.getText().equals("Locale")).count());
        }
        chromeDriver.close();
        chromeDriver.quit();
    }
}