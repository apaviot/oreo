package com.oreo.selenium;

import com.oreo.OreoApplication;
import com.oreo.unitary.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class)
public class ImportTest {

    @Test
    public void testSucceded() {
        assertTrue(true);
    }


    public ChromeDriver launchPage() {
        try {
            ChromeDriver chromeDriver = new ChromeDriver(new ChromeOptions().addArguments("--no-sandbox"));
            chromeDriver.manage().timeouts().pageLoadTimeout(1, TimeUnit.MINUTES);
            chromeDriver.get(TestUtils.clientUrl(3000, "/"));
            Thread.sleep(3000);
            return chromeDriver;
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        }
    }

    @Test
    public void testInsertTextInAllFields() {
        ChromeDriver chromeDriver = launchPage();
        chromeDriver.findElement(By.className("float-right")).click();
        chromeDriver.findElement(By.id("benchTextName")).sendKeys("BancName");
        chromeDriver.findElement(By.id("campaignTextId")).sendKeys("ID12CYHGTY76");
        chromeDriver.findElement(By.id("experimentTextName")).sendKeys("experimentName");
        chromeDriver.findElement(By.id("experimentTextReference")).sendKeys("1452653456");
        chromeDriver.close();
        chromeDriver.quit();
    }

    @Test
    public void testWithoutFields() {
        ChromeDriver chromeDriver = launchPage();
        chromeDriver.findElement(By.className("float-right")).click();
        chromeDriver.findElement(By.id("buttonImport")).click();
        assertFalse(chromeDriver.findElements(By.className("is-visible")).isEmpty());
        chromeDriver.close();
        chromeDriver.quit();
    }

    @Test
    public void testValidImportWithFieldsAndWithExperimentFile() {
        ChromeDriver chromeDriver = launchPage();
        chromeDriver.findElement(By.className("float-right")).click();
        chromeDriver.findElement(By.id("benchTextName")).sendKeys("BancName");
        chromeDriver.findElement(By.id("campaignTextId")).sendKeys("ID12CYHGTY76");
        chromeDriver.findElement(By.id("experimentTextName")).sendKeys("experimentName");
        chromeDriver.findElement(By.id("experimentTextReference")).sendKeys("1452653456");
        chromeDriver.findElement(By.id("experimentFile")).sendKeys(Paths.get("").toAbsolutePath().toString() + "/testfile.csv");
        chromeDriver.findElement(By.id("buttonImport")).click();
        assertTrue(chromeDriver.findElement(By.id("chargement")).getText().length() > 0);
        chromeDriver.close();
        chromeDriver.quit();
    }

    @Test
    public void testValidImportWithFieldsButWithoutExperimentFile() {
        ChromeDriver chromeDriver = launchPage();
        chromeDriver.findElement(By.className("float-right")).click();
        chromeDriver.findElement(By.id("benchTextName")).sendKeys("BancName");
        chromeDriver.findElement(By.id("campaignTextId")).sendKeys("ID12CYHGTY76");
        chromeDriver.findElement(By.id("experimentTextName")).sendKeys("experimentName");
        chromeDriver.findElement(By.id("experimentTextReference")).sendKeys("1452653456");
        chromeDriver.findElement(By.id("buttonImport")).click();
        assertFalse(chromeDriver.findElement(By.id("chargement")).getText().length() > 0);
        chromeDriver.close();
        chromeDriver.quit();
    }


    @Test
    public void testValidImportWithoutAllFieldsFilled() {
        ChromeDriver chromeDriver = launchPage();
        chromeDriver.findElement(By.className("float-right")).click();
        WebElement buttonImport = chromeDriver.findElement(By.id("buttonImport"));
        chromeDriver.findElement(By.id("benchTextName")).sendKeys("BancName");
        buttonImport.click();
        assertFalse(chromeDriver.findElement(By.id("chargement")).getText().length() > 0);
        chromeDriver.findElement(By.id("campaignTextId")).sendKeys("ID12CYHGTY76");
        buttonImport.click();
        assertFalse(chromeDriver.findElement(By.id("chargement")).getText().length() > 0);
        chromeDriver.findElement(By.id("experimentTextName")).sendKeys("experimentName");
        buttonImport.click();
        assertFalse(chromeDriver.findElement(By.id("chargement")).getText().length() > 0);
        chromeDriver.findElement(By.id("experimentTextReference")).sendKeys("1452653456");
        buttonImport.click();
        assertFalse(chromeDriver.findElement(By.id("chargement")).getText().length() > 0);
        chromeDriver.close();
        chromeDriver.quit();
    }
}