package com.oreo.unitary.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oreo.OreoApplication;
import com.oreo.controller.SampleController;
import com.oreo.entity.Sample;
import com.oreo.unitary.TestUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SampleControllerTest {
    @Autowired
    SampleController controller;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private HttpHeaders headers = new HttpHeaders();

    @Test
    public void testFindByMeasureIdWithInterval() {
        List<Sample> samples = Arrays.asList(
                new Sample(1, 1, "2019-02-05T14:08:00.00"),
                new Sample(6, 1, "2019-02-05T14:08:00.50"),
                new Sample(11, 1, "2019-02-05T14:08:01.00"),
                new Sample(16, 1, "2019-02-05T14:08:01.50"),
                new Sample(21, 1, "2019-02-05T14:08:02.00"),
                new Sample(26, 1, "2019-02-05T14:08:02.50"),
                new Sample(31, 1, "2019-02-05T14:08:03.00"),
                new Sample(36, 1, "2019-02-05T14:08:03.50"),
                new Sample(41, 1, "2019-02-05T14:08:04.00"));
        List<Sample> response = (List<Sample>) controller.findAll(1, LocalDateTime.parse("2019-02-05T14:08:00.00"),
                LocalDateTime.parse("2019-02-05T14:08:04.00"), 500, null).getBody();
        assertEquals(samples, response);
    }

    @Test
    public void testFindByMeasureIdNonExistant() {
        assertEquals(HttpStatus.NOT_FOUND, controller.findAll(0, LocalDateTime.parse("2019-02-05T14:08:00.00"),
                LocalDateTime.parse("2019-02-05T14:08:04.00"), 500, null).getStatusCode());
    }

    @Test
    public void testFindByMeasureIdEmpty() {
        assertEquals(new ArrayList<Sample>(), controller.findAll(5, LocalDateTime.parse("2019-02-05T14:08:00.00"),
                LocalDateTime.parse("2019-02-05T14:08:04.00"), 500, null).getBody());
    }

    @Test
    public void testJsonFormatFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/samples?measureId=1&start=2019-02-05T14:08:00.00&end=2019-02-05T14:08:04.00&interval=500"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        JsonObject json = new JsonParser().parse(response.getBody()).getAsJsonArray().get(0).getAsJsonObject();
        HashSet<String> keys = new HashSet<>();
        keys.add("value");
        keys.add("time");
        assertEquals(keys, json.keySet());
    }

    @Test
    public void testGetSamplesByMeasureIdNotExistant() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.url(port, "/samples?measureId=99&start=2019-02-05T14:08:00.00&end=2019-02-05T14:08:04.00&interval=500"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testGetSamplesByMeasureIdEmpty() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.url(port, "/samples?measureId=5&start=2019-02-05T14:08:00.00&end=2019-02-05T14:08:04.00&interval=500"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        JsonArray json = new JsonParser().parse(response.getBody()).getAsJsonArray();
        assertEquals(0, json.size());
    }

    @Test
    public void testGetSamplesMissingParameterMeasureIdFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.url(port, "/samples?start=2019-02-05T14:08:00.00&end=2019-02-05T14:08:04.00&interval=500"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testGetSamplesMissingParameterStartFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.url(port, "/samples?measureId=1&end=2019-02-05T14:08:04.00&interval=500"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testGetSamplesMissingParameterEndFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.url(port, "/samples?measureId=1&start=2019-02-05T14:08:00.00&interval=500"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testGetSamplesMissingParameterIntervalFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.url(port, "/samples?measureId=1&start=2019-02-05T14:08:00.00&end=2019-02-05T14:08:04.00"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCountByMeasure() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Long> response2 = restTemplate.exchange(
                TestUtils.url(port, "/samples/search/countByMeasure/1"),
                HttpMethod.GET, entity, Long.class);
        assertEquals(72L, (long) response2.getBody());
    }

    @Test
    public void testCountByMeasureNotExistant() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<Long> response2 = restTemplate.exchange(
                TestUtils.url(port, "/samples/search/countByMeasure/99"),
                HttpMethod.GET, entity, Long.class);
        assertEquals(HttpStatus.NOT_FOUND, response2.getStatusCode());
    }

    @Test
    public void testFindByMeasureIdWithIntervalAndModification() {
        List<Double> expectedValues = Arrays.asList(3d, 3d, 3d, 3d, 15d, 15d, 15d, 5d, 5d);
        List<Sample> response = (List<Sample>) controller.findAll(1, LocalDateTime.parse("2019-02-05T14:08:00.00"),
                LocalDateTime.parse("2019-02-05T14:08:04.00"), 500, 1L).getBody();
        List<Double> result = response.stream().map(Sample::getValue).collect(Collectors.toList());
        assertEquals(expectedValues, result);
    }

    @Test
    public void testFindByMeasureIdWithIntervalAndBadModification() {
        ResponseEntity<String> response = (ResponseEntity<String>) controller.findAll(1, LocalDateTime.parse("2019-02-05T14:08:00.00"),
                LocalDateTime.parse("2019-02-05T14:08:04.00"), 500, 9L);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("No modification found with the given id", response.getBody());
    }

    @Test
    public void testFindByMeasureIdWithIntervalAndModificationForAnotherExperiment() {
        ResponseEntity<String> response = (ResponseEntity<String>) controller.findAll(1, LocalDateTime.parse("2019-02-05T14:08:00.00"),
                LocalDateTime.parse("2019-02-05T14:08:04.00"), 500, 2L);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("The given modification does not match the measure", response.getBody());
    }

    @Test
    public void testGetSamplesByMeasureIdAndModificationFromApi() {
        List<Double> expectedValues = Arrays.asList(3d, 3d, 3d, 3d, 15d, 15d, 15d, 5d, 5d);
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<List<Sample>> response = restTemplate.exchange(
                TestUtils.url(port, "/samples?measureId=1&start=2019-02-05T14:08:00.00&end=2019-02-05T14:08:04.00&interval=500&modificationId=1"),
                HttpMethod.GET, entity, new ParameterizedTypeReference<List<Sample>>(){});
        List<Double> result = response.getBody().stream().map(Sample::getValue).collect(Collectors.toList());
        assertEquals(expectedValues, result);
    }

    @Test
    public void testGetSamplesByMeasureIdAndModificationNonExistantFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.url(port, "/samples?measureId=1&start=2019-02-05T14:08:00.00&end=2019-02-05T14:08:04.00&interval=500&modificationId=99"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("No modification found with the given id", response.getBody());
    }

    @Test
    public void testFindByMeasureIdWithIntervalAndModificationForAnotherExperimentFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.url(port, "/samples?measureId=1&start=2019-02-05T14:08:00.00&end=2019-02-05T14:08:04.00&interval=500&modificationId=2"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("The given modification does not match the measure", response.getBody());
    }
}
