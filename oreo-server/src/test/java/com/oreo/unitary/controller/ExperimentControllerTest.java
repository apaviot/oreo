package com.oreo.unitary.controller;

import com.oreo.OreoApplication;
import com.oreo.controller.ExperimentController;
import com.oreo.entity.Experiment;
import com.oreo.unitary.RestResponsePage;
import com.oreo.unitary.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ExperimentControllerTest {
    @Autowired
    ExperimentController controller;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private HttpHeaders headers = new HttpHeaders();

    @Test
    public void testAdvancedSearchWithName() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?name=experiment1"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Experiment> exp = Collections.singletonList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1));
        assertEquals(exp, response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithUnknowName() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?name=hello"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new ArrayList<Experiment>(), response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithFuzzyName() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?name=experiment"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(3, "experiment3", "experiment3", "2019-02-05T14:10:00", false, 2, 3),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithDate() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?lastUse=2019-02-05T14:08:00"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(3, "experiment3", "experiment3", "2019-02-05T14:10:00", false, 2, 3),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithUnknowDate() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?lastUse=2000-01-01T00:00:00"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new ArrayList<Experiment>(), response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithDatabaseLocal() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?database=local"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new ArrayList<Experiment>(), response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithDatabaseRemote() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?database=remote"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(3, "experiment3", "experiment3", "2019-02-05T14:10:00", false, 2, 3),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithUnknowDatabase() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/experiments/advancedSearch?database=hello"),
                HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testAdvancedSearchWithBenchId() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?benchId=1"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1));
        assertEquals(experiments, response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithUnknowBenchId() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?benchId=0"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new ArrayList<Experiment>(), response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithCampaignId() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?campaignId=1"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        System.out.println(response.getBody().getContent());
        assertEquals(experiments, response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithUnknowCampaignId() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?campaignId=0"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new ArrayList<Experiment>(), response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithMultipleParameters() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?benchId=1&campaignId=1"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1));
        assertEquals(experiments, response.getBody().getContent().get("experiments"));
    }

    @Test
    public void testAdvancedSearchWithAllParameters() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Experiment>>> response = restTemplate.exchange(
                TestUtils.url(port, "/experiments/advancedSearch?lastUse=2019-02-05T14:08:00&database=remote&benchId=2&campaignId=1&name=5"),
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<RestResponsePage<List<Experiment>>>(){}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Experiment> experiments = Collections.singletonList(
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, response.getBody().getContent().get("experiments"));
    }
}
