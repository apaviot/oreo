package com.oreo.unitary.controller;

import com.oreo.OreoApplication;
import com.oreo.controller.SQLController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SQLControllerTest {

    @Autowired
    private SQLController controller;

    @Test
    public void testSelectAllCampaigns() {
        List<? extends Map<String, Object>> maps = controller.execute("SELECT * FROM campaigns");
        for (int i = 0; i < 5; i++) {
            Map<String, Object> map = maps.get(i);
            System.out.println(map);
            assertEquals(i + 1L, map.get("ID"));
            assertEquals("campaign" + (i + 1), map.get("ID12C"));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDelete() {
        controller.execute("DELETE FROM samples");
    }
}
