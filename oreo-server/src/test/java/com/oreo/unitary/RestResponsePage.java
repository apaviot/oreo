package com.oreo.unitary;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;

public class RestResponsePage<T> {
    private Map<String, T> content = new HashMap<>();

    public Map<String, T> getContent() {
        return content;
    }

    @JsonProperty("_embedded")
    private void unpackNameFromNestedObject(Map<String, T> brand) {
        content = brand;
    }
}
