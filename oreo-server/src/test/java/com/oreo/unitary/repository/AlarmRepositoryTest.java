package com.oreo.unitary.repository;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oreo.OreoApplication;
import com.oreo.entity.Alarm;
import com.oreo.repository.AlarmRepository;
import com.oreo.unitary.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AlarmRepositoryTest {

    @LocalServerPort
    private int port;

    @Autowired
    AlarmRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    private HttpHeaders headers = new HttpHeaders();

    @Test
    public void testCount() {
        assertEquals(10, repository.count());
    }

    @Test
    public void testFindById() {
        Alarm alarm = new Alarm(1, "alarm1", "alarm1", "ON", "msg", 3, "order", "2019-02-05T14:08:00", 1);
        assertEquals(alarm, repository.findOne(1L));
    }

    @Test
    public void testFindByIdNonExistant() {
        assertNull(repository.findOne(0L));
    }

    @Test
    public void testFindByExperimentId() {
        List<Alarm> experiments = Arrays.asList(
                new Alarm(1, "alarm1", "alarm1", "ON", "msg", 3, "order", "2019-02-05T14:10:00", 1),
                new Alarm(2, "alarm2", "alarm2", "ON", "msg", 1, "order", "2019-02-05T14:12:00", 1),
                new Alarm(3, "alarm3", "alarm3", "ON", "msg", 3, "order", "2019-02-05T14:14:00", 1),
                new Alarm(4, "alarm4", "alarm4", "ON", "msg", 4, "order", "2019-02-05T14:16:00", 1));
        assertEquals(experiments, repository.findByExperimentId(1));
    }

    @Test
    public void testFindByExperimentIdNonExistant() {
        assertEquals(new ArrayList<Alarm>(), repository.findByExperimentId(0));
    }

    @Test
    public void testFindByExperimentIdEmpty() {
        assertEquals(new ArrayList<Alarm>(), repository.findByExperimentId(5));
    }

    @Test
    public void testGetAllAlarmsIsNotAllowedFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/alarms"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testGetAlarmByIdFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/alarms/1"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetAlarmByIdNonExistantFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/alarms/0"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testJsonFormatFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/alarms/1"), HttpMethod.GET, entity, String.class);

        JsonObject json = new JsonParser().parse(response.getBody()).getAsJsonObject();
        HashSet<String> keys = new HashSet<>();
        keys.add("id");
        keys.add("reference");
        keys.add("name");
        keys.add("state");
        keys.add("message");
        keys.add("level");
        keys.add("order");
        keys.add("time");
        assertEquals(keys, json.keySet());
    }

    @Test
    public void testDeleteIsNotAllowedFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/alarms/1"), HttpMethod.DELETE, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testSavePutIsNotAllowedFromApi() {
        Alarm alarm = new Alarm(1, "alarm1", "alarm1", "ON", "msg", 3, "order", "2019-02-05T14:10:00", 1);
        HttpEntity<Alarm> entity = new HttpEntity<>(alarm, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/alarms/1"), HttpMethod.PUT, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testSavePostIsNotAllowedFromApi() {
        Alarm alarm = new Alarm(1, "alarm1", "alarm1", "ON", "msg", 3, "order", "2019-02-05T14:10:00", 1);
        HttpEntity<Alarm> entity = new HttpEntity<>(alarm, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/alarms"), HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testSavePatchIsNotAllowedFromApi() {
        Alarm alarm = new Alarm(1, "alarm1", "alarm1", "ON", "msg", 3, "order", "2019-02-05T14:10:00", 1);
        HttpEntity<Alarm> entity = new HttpEntity<>(alarm, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/alarms/1"), HttpMethod.PATCH, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }
}
