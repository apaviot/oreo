package com.oreo.unitary.repository;

import com.oreo.OreoApplication;
import com.oreo.entity.Campaign;
import com.oreo.repository.CampaignRepository;
import com.oreo.unitary.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Sort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CampaignRepositoryTest {

    @LocalServerPort
    private int port;

    @Autowired
    CampaignRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    private HttpHeaders headers = new HttpHeaders();

    @Test
    public void testCount() {
        assertEquals(5, repository.count());
    }

    @Test
    public void testFindById() {
        Campaign campaign = new Campaign(1, "campaign1");
        assertEquals(campaign, repository.findOne(1L));
    }

    @Test
    public void testFindByIdNonExistant() {
        assertNull(repository.findOne(0L));
    }

    @Test
    public void testFindAll() {
        List<Campaign> campaigns = Arrays.asList(
            new Campaign(1, "campaign1"),
            new Campaign(2, "campaign2"),
            new Campaign(3, "campaign3"),
            new Campaign(4, "campaign4"),
            new Campaign(5, "campaign5"));
        assertEquals(campaigns, repository.findAll());
    }

    @Test
    public void testFindAllSortByIdDesc() {
        List<Campaign> campaigns = Arrays.asList(
                new Campaign(5, "campaign5"),
                new Campaign(4, "campaign4"),
                new Campaign(3, "campaign3"),
                new Campaign(2, "campaign2"),
                new Campaign(1, "campaign1"));
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        assertEquals(campaigns, repository.findAll(sort));
    }

    @Test
    public void testFindAllSortById12cDesc() {
        List<Campaign> campaigns = Arrays.asList(
                new Campaign(5, "campaign5"),
                new Campaign(4, "campaign4"),
                new Campaign(3, "campaign3"),
                new Campaign(2, "campaign2"),
                new Campaign(1, "campaign1"));
        Sort sort = new Sort(Sort.Direction.DESC, "id12c");
        assertEquals(campaigns, repository.findAll(sort));
    }

    @Test
    public void testGetAllCampaignsFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/campaigns"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetCampaignByIdFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/campaigns/1"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetBenchByIdNonExistantFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/campaigns/0"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testJsonFormatFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/campaigns/1"), HttpMethod.GET, entity, String.class);

        JsonObject json = new JsonParser().parse(response.getBody()).getAsJsonObject();
        HashSet<String> keys = new HashSet<>();
        keys.add("id");
        keys.add("id12c");
        assertEquals(keys, json.keySet());
    }

    @Test
    public void testDeleteIsNotAllowedFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/campaigns/1"), HttpMethod.DELETE, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testSavePutIsNotAllowedFromApi() {
        Campaign campaign = new Campaign(1, "test");
        HttpEntity<Campaign> entity = new HttpEntity<>(campaign, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/campaigns/1"), HttpMethod.PUT, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testSavePostIsNotAllowedFromApi() {
        Campaign campaign = new Campaign(1, "test");
        HttpEntity<Campaign> entity = new HttpEntity<>(campaign, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/campaigns"), HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testSavePatchIsNotAllowedFromApi() {
        Campaign campaign = new Campaign(1, "test");
        HttpEntity<Campaign> entity = new HttpEntity<>(campaign, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/campaigns/1"), HttpMethod.PATCH, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }
}
