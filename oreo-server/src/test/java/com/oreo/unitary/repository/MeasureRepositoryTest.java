package com.oreo.unitary.repository;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oreo.OreoApplication;
import com.oreo.entity.Measure;
import com.oreo.repository.MeasureRepository;
import com.oreo.unitary.RestResponsePage;
import com.oreo.unitary.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MeasureRepositoryTest {

    @LocalServerPort
    private int port;

    @Autowired
    MeasureRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    private HttpHeaders headers = new HttpHeaders();

    @Test
    public void testFindByExprimentId() {
        List<Measure> measures = Arrays.asList(
            new Measure(1, "measure1", "°C", 10, 1),
            new Measure(2, "measure2", "km/h", 10, 1),
            new Measure(3, "measure3", "N", 10, 1),
            new Measure(4, "measure4", "kg", 10, 1),
            new Measure(5, "measure5", "°C", 10, 1),
            new Measure(6, "measure6", "km/h", 10, 1),
            new Measure(7, "measure7", "N", 10, 1),
            new Measure(8, "measure8", "kg", 10, 1),
            new Measure(9, "measure9", "°C", 10, 1));
        assertEquals(measures, repository.findByExperimentId(1, null).getContent());
    }

    @Test
    public void testFindByExprimentIdOneResultPerPage() {
        List<Measure> measures = Collections.singletonList(
                new Measure(1, "measure1", "°C", 10, 1));
        Pageable pageable = new PageRequest(0, 1);
        assertEquals(measures, repository.findByExperimentId(1, pageable).getContent());
    }

    @Test
    public void testFindByExperimentIdNonExistant() {
        assertEquals(new ArrayList<Measure>(), repository.findByExperimentId(0, null).getContent());
    }

    @Test
    public void testFindByExperimentIdEmptyExistant() {
        assertEquals(new ArrayList<Measure>(), repository.findByExperimentId(5, null).getContent());
    }

    @Test
    public void testAdvancedSearchAllResult() {
        List<Measure> measures = Arrays.asList(
                new Measure(1, "measure1", "°C", 10, 1),
                new Measure(2, "measure2", "km/h", 10, 1),
                new Measure(3, "measure3", "N", 10, 1),
                new Measure(4, "measure4", "kg", 10, 1),
                new Measure(5, "measure5", "°C", 10, 1),
                new Measure(6, "measure6", "km/h", 10, 1),
                new Measure(7, "measure7", "N", 10, 1),
                new Measure(8, "measure8", "kg", 10, 1),
                new Measure(9, "measure9", "°C", 10, 1));
        assertEquals(measures, repository.advancedSearch(1L, "measure", null).getContent());
    }

    @Test
    public void testAdvancedSearchOneResult() {
        List<Measure> measures = Collections.singletonList(
                new Measure(1, "measure1", "°C", 10, 1));
        assertEquals(measures, repository.advancedSearch(1L, "measure1", null).getContent());
    }

    @Test
    public void testJsonFormatFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/measures/search/findByExperimentId?experimentId=1"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        JsonObject json = new JsonParser().parse(response.getBody()).getAsJsonObject()
                .getAsJsonObject("_embedded").getAsJsonArray("measures").get(1).getAsJsonObject();
        HashSet<String> keys = new HashSet<>();
        keys.add("id");
        keys.add("name");
        keys.add("unit");
        keys.add("frequency");
        assertEquals(keys, json.keySet());
    }

    @Test
    public void testFindByExprimentIdOneResultPerPageFromApi() {
        List<Measure> measures = Collections.singletonList(
                new Measure(1, "measure1", "°C", 10, 1));
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Measure>>> response = restTemplate.exchange(TestUtils.url(port, "/measures/search/findByExperimentId?experimentId=1&page=0&size=1"),
                HttpMethod.GET, entity, new ParameterizedTypeReference<RestResponsePage<List<Measure>>>(){});
        assertEquals(measures, response.getBody().getContent().get("measures"));
    }
}
