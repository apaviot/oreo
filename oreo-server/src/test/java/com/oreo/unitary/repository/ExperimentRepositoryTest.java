package com.oreo.unitary.repository;

import com.oreo.OreoApplication;
import com.oreo.entity.Experiment;
import com.oreo.repository.ExperimentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class)
public class ExperimentRepositoryTest {

    @Autowired
    ExperimentRepository repository;

    @Test
    public void testCount() {
        assertEquals(5, repository.count());
    }

    @Test
    public void testFindById() {
        Experiment bench = new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1);
        assertEquals(bench, repository.findOne(1L));
    }

    @Test
    public void testFindByIdNonExistant() {
        assertNull(repository.findOne(0L));
    }

    @Test
    public void testFindByCampaignId() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, repository.findByCampaignId(1));
    }

    @Test
    public void testFindByCampaignIdNonExistant() {
        assertEquals(new ArrayList<Experiment>(), repository.findByCampaignId(0));
    }

    @Test
    public void testFindByCampaignIdEmpty() {
        assertEquals(new ArrayList<Experiment>(), repository.findByCampaignId(5));
    }

    @Test
    public void testFindByBenchId() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1));
        assertEquals(experiments, repository.findByBenchId(1L));
    }

    @Test
    public void testFindByBenchIdNonExistant() {
        assertEquals(new ArrayList<Experiment>(), repository.findByBenchId(0));
    }

    @Test
    public void testFindByBenchIdEmpty() {
        assertEquals(new ArrayList<Experiment>(), repository.findByBenchId(5));
    }

    @Test
    public void testFindAll() {
        List<Experiment> experiments = Arrays.asList(
            new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
            new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
            new Experiment(3, "experiment3", "experiment3", "2019-02-05T14:10:00", false, 2, 3),
            new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1),
            new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, repository.findAll());
    }

    @Test
    public void testFindAllSortByBeginDesc() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1),
                new Experiment(3, "experiment3", "experiment3", "2019-02-05T14:10:00", false, 2, 3),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1));
        Sort sort = new Sort(Sort.Direction.DESC, "begin");
        assertEquals(experiments, repository.findAll(sort));
    }

    @Test
    public void testFindTwoLast() {
        List<Experiment> benches = Arrays.asList(
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1));
        Sort sort = new Sort(Sort.Direction.DESC, "begin");
        Pageable pageable = new PageRequest(0, 2, sort);
        assertEquals(benches, repository.findAll(pageable).getContent());
    }

    @Test
    public void testFindByFuzzyName() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(3, "experiment3", "experiment3", "2019-02-05T14:10:00", false, 2, 3),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, repository.findByFuzzyName("experiment"));
    }

    @Test
    public void testFindByFuzzyNameSingleResult() {
        List<Experiment> experiments = Collections.singletonList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1));
        assertEquals(experiments, repository.findByFuzzyName("1"));
    }

    @Test
    public void testAdvancedSearchWithNoParameters() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(3, "experiment3", "experiment3", "2019-02-05T14:10:00", false, 2, 3),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, repository.advancedSearch(null, null, null, null, null, null).getContent());
    }

    @Test
    public void testAdvancedSearchWithName() {
        List<Experiment> experiments = Collections.singletonList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1));
        assertEquals(experiments, repository.advancedSearch(null, null, null, null, "experiment1", null).getContent());
    }

    @Test
    public void testAdvancedSearchWithUnknowName() {
        List<Experiment> experiments = Collections.emptyList();
        assertEquals(experiments, repository.advancedSearch(null, null, null, null, "hello", null).getContent());
    }

    @Test
    public void testAdvancedSearchWithFuzzyName() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(3, "experiment3", "experiment3", "2019-02-05T14:10:00", false, 2, 3),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, repository.advancedSearch(null, null, null, null, "exp", null).getContent());
    }

    @Test
    public void testAdvancedSearchWithDate() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(3, "experiment3", "experiment3", "2019-02-05T14:10:00", false, 2, 3),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, repository.advancedSearch(LocalDateTime.parse("2019-02-05T14:08:00"), null, null, null, null, null).getContent());
    }

    @Test
    public void testAdvancedSearchWithUnknowDate() {
        List<Experiment> experiments = Collections.emptyList();
        assertEquals(experiments, repository.advancedSearch(LocalDateTime.parse("2000-01-01T00:00:00"), null, null, null, null, null).getContent());
    }

    @Test
    public void testAdvancedSearchWithDatabaseLocal() {
        assertEquals(Collections.emptyList(), repository.advancedSearch(null, "local", null, null, null, null).getContent());
    }

    @Test
    public void testAdvancedSearchWithDatabaseRemote() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(3, "experiment3", "experiment3", "2019-02-05T14:10:00", false, 2, 3),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, repository.advancedSearch(null, "remote", null, null, null, null).getContent());
    }

    @Test(expected = InvalidDataAccessApiUsageException.class)
    public void testAdvancedSearchWithUnknowDatabase() {
        repository.advancedSearch(null, "hello", null, null, null, null).getContent();
    }

    @Test
    public void testAdvancedSearchWithBenchId() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(4, "experiment4", "experiment4", "2019-02-05T14:11:00", false, 4, 1));
        assertEquals(experiments, repository.advancedSearch(null, null, null, 1L, null, null).getContent());
    }

    @Test
    public void testAdvancedSearchWithUnknowBenchId() {
        assertEquals(Collections.emptyList(), repository.advancedSearch(null, null, null, 0L, null, null).getContent());
    }

    @Test
    public void testAdvancedSearchWithCampaignId() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1),
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, repository.advancedSearch(null, null, 1L, null, null, null).getContent());
    }

    @Test
    public void testAdvancedSearchWithUnknowCampaignId() {
        assertEquals(Collections.emptyList(), repository.advancedSearch(null, null, 0L, null, null, null).getContent());
    }

    @Test
    public void testAdvancedSearchWithMultipleParameters() {
        List<Experiment> experiments = Arrays.asList(
                new Experiment(1, "experiment1", "experiment1", "2019-02-05T14:08:00", false, 1, 1),
                new Experiment(2, "experiment2", "experiment2", "2019-02-05T14:09:00", false, 1, 1));
        assertEquals(experiments, repository.advancedSearch(null, null, 1L, 1L, null, null).getContent());
    }

    @Test
    public void testAdvancedSearchWithAllParameters() {
        List<Experiment> experiments = Collections.singletonList(
                new Experiment(5, "experiment5", "experiment5", "2019-02-05T14:12:00", false, 1, 2));
        assertEquals(experiments, repository.advancedSearch(LocalDateTime.parse("2019-02-05T14:12:00"), "remote", 1L, 2L, "5", null).getContent());
    }
}
