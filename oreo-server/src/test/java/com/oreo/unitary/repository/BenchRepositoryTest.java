package com.oreo.unitary.repository;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oreo.OreoApplication;
import com.oreo.entity.Bench;
import com.oreo.repository.BenchRepository;
import com.oreo.unitary.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.domain.Sort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BenchRepositoryTest {

    @LocalServerPort
    private int port;

    @Autowired
    BenchRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    private HttpHeaders headers = new HttpHeaders();

    @Test
    public void testCount() {
        assertEquals(5, repository.count());
    }

    @Test
    public void testFindById() {
        Bench bench = new Bench(1, "ref1", "bench1");
        assertEquals(bench, repository.findOne(1L));
    }

    @Test
    public void testFindByIdNonExistant() {
        assertNull(repository.findOne(0L));
    }

    @Test
    public void testFindAll() {
        List<Bench> benches = Arrays.asList(
            new Bench(1, "ref1", "bench1"),
            new Bench(2, "ref2", "bench2"),
            new Bench(3, "ref3", "bench3"),
            new Bench(4, "ref4", "bench4"),
            new Bench(5, "ref5", "bench5"));
        assertEquals(benches, repository.findAll());
    }

    @Test
    public void testFindAllSortByIdDesc() {
        List<Bench> benches = Arrays.asList(
                new Bench(5, "ref5", "bench5"),
                new Bench(4, "ref4", "bench4"),
                new Bench(3, "ref3", "bench3"),
                new Bench(2, "ref2", "bench2"),
                new Bench(1, "ref1", "bench1"));
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        assertEquals(benches, repository.findAll(sort));
    }

    @Test
    public void testFindAllSortByNameDesc() {
        List<Bench> benches = Arrays.asList(
                new Bench(5, "ref5", "bench5"),
                new Bench(4, "ref4", "bench4"),
                new Bench(3, "ref3", "bench3"),
                new Bench(2, "ref2", "bench2"),
                new Bench(1, "ref1", "bench1"));
        Sort sort = new Sort(Sort.Direction.DESC, "name");
        assertEquals(benches, repository.findAll(sort));
    }

    @Test
    public void testGetAllBenchesFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/benches"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetBenchByIdFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/benches/1"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetBenchByIdNonExistantFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/benches/0"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testJsonFormatFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/benches/1"), HttpMethod.GET, entity, String.class);

        JsonObject json = new JsonParser().parse(response.getBody()).getAsJsonObject();
        HashSet<String> keys = new HashSet<>();
        keys.add("id");
        keys.add("reference");
        keys.add("name");
        assertEquals(keys, json.keySet());
    }

    @Test
    public void testDeleteIsNotAllowedFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/benches/1"), HttpMethod.DELETE, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testSavePutIsNotAllowedFromApi() {
        Bench bench = new Bench(1, "refTest", "Test");
        HttpEntity<Bench> entity = new HttpEntity<>(bench, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/benches/1"), HttpMethod.PUT, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testSavePostIsNotAllowedFromApi() {
        Bench bench = new Bench(1, "refTest", "Test");
        HttpEntity<Bench> entity = new HttpEntity<>(bench, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/benches"), HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testSavePatchIsNotAllowedFromApi() {
        Bench bench = new Bench(1, "refTest", "Test");
        HttpEntity<Bench> entity = new HttpEntity<>(bench, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/benches/1"), HttpMethod.PATCH, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }
}
