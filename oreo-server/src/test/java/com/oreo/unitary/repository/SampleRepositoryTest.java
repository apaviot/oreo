package com.oreo.unitary.repository;

import com.oreo.OreoApplication;
import com.oreo.entity.Sample;
import com.oreo.repository.SampleRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SampleRepositoryTest {

    @Autowired
    SampleRepository repository;

    @Test
    public void testFindByMeasureId() {
        List<Sample> samples = Arrays.asList(
                new Sample(1, 1, "2019-02-05T14:08:00.00"),
                new Sample(2, 1, "2019-02-05T14:08:00.10"),
                new Sample(3, 1, "2019-02-05T14:08:00.20"),
                new Sample(4, 1, "2019-02-05T14:08:00.30"),
                new Sample(5, 1, "2019-02-05T14:08:00.40"),
                new Sample(6, 1, "2019-02-05T14:08:00.50"),
                new Sample(7, 1, "2019-02-05T14:08:00.60"),
                new Sample(8, 1, "2019-02-05T14:08:00.70"),
                new Sample(9, 1, "2019-02-05T14:08:00.80"),
                new Sample(10, 1, "2019-02-05T14:08:00.90"),
                new Sample(11, 1, "2019-02-05T14:08:01.00"));
        List<Sample> response = repository.findAll(1, LocalDateTime.parse("2019-02-05T14:08:00.00"), LocalDateTime.parse("2019-02-05T14:08:01.00"), 1000);
        assertEquals(samples, response);
    }

    @Test
    public void testCountByMeasureId() {
        assertEquals(72L, (long) repository.countByMeasure(1L).get());
    }

    @Test
    public void testCountByMeasureIdNonExistant() {
        assertFalse(repository.countByMeasure(99L).isPresent());
    }
}
