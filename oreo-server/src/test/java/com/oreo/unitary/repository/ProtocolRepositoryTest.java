package com.oreo.unitary.repository;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oreo.OreoApplication;
import com.oreo.entity.Alarm;
import com.oreo.entity.Protocol;
import com.oreo.repository.AlarmRepository;
import com.oreo.repository.ProtocolRepository;
import com.oreo.unitary.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProtocolRepositoryTest {

    @Autowired
    ProtocolRepository repository;


    @Test
    public void testFindById() {
        Protocol protocol = new Protocol("iena");
        assertEquals(protocol, repository.findOne("iena"));
        System.out.println(repository.findOne("iena"));
    }

    @Test
    public void testFindByIdNonExistant() {
        assertNull(repository.findOne("hello"));
    }

}
