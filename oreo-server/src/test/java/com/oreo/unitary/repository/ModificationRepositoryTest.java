package com.oreo.unitary.repository;

import com.oreo.OreoApplication;
import com.oreo.entity.Modification;
import com.oreo.entity.Operation;
import com.oreo.entity.PartialModification;
import com.oreo.repository.ModificationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.Assert.*;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class)
public class ModificationRepositoryTest {

    @Autowired
    ModificationRepository repository;

    @Test
    public void testFindById() {
        Modification modification = new Modification(1);
        System.out.println(repository.findOne(1L));

        assertEquals(modification, repository.findOne(1L));
    }

    @Test
    public void testFindByIdNotExistant() {
        assertNull(repository.findOne(99L));
    }

    @Test
    public void testDeleteById() {
        assertNotNull(repository.findOne(1L));
        repository.delete(1L);
        assertNull(repository.findOne(1L));
    }

    @Test
    public void testFindByExperimentId() {
        assertEquals(Collections.singletonList(new Modification(1)), repository.findByExperimentId(1));
    }

    @Test
    public void testFindByExperimentIdEmpty() {
        assertEquals(Collections.emptyList(), repository.findByExperimentId(5));
    }

    @Test
    public void testFindByExperimentIdNonExistant() {
        assertEquals(Collections.emptyList(), repository.findByExperimentId(99));
    }

    @Test
    public void testSaveCreate() {
        Modification modification = new Modification();
        PartialModification partial = new PartialModification(0, "2019-02-05T14:08:00.00", "2019-02-05T14:08:04.00", 10, Operation.ADD, 1);
        modification.addPartial(partial);
        repository.save(modification);
        System.out.println(repository.findOne(modification.getId()));
        assertEquals(modification, repository.findOne(modification.getId()));
    }

    @Test
    public void testSaveUpdate() {
        Modification modification = repository.findOne(1L);
        assertEquals("modification1", modification.getName());
        modification.setName("update");
        repository.save(modification);
        assertEquals("update", repository.findOne(1L).getName());
    }
}