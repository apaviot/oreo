package com.oreo.unitary;

public class TestUtils {
    public static String url(int port, String uri) {
        return "http://localhost:" + port + "/api" + uri;
    }

    public static String clientUrl(int port, String uri) {
        return "http://localhost:" + port + uri;
    }

}
