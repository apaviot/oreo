package com.oreo.unitary.protocol;

import com.oreo.OreoApplication;
import com.oreo.entity.Measure;
import com.oreo.entity.Modification;
import com.oreo.entity.Sample;
import com.oreo.protocol.SampleSupplier;
import com.oreo.repository.MeasureRepository;
import com.oreo.repository.ModificationRepository;
import com.oreo.repository.SampleRepository;
import com.oreo.utils.SampleModifier;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class)
public class SampleSupplierTest {

    @Autowired
    private SampleRepository sampleRepository;

    @Autowired
    private MeasureRepository measureRepository;

    @Autowired
    private ModificationRepository modificationRepository;

    @Test(expected = NullPointerException.class)
    public void testConstructorListNull() {
        new SampleSupplier(null, sampleRepository, LocalDateTime.parse("2019-02-05T14:08:00.00"), LocalDateTime.parse("2019-02-05T14:08:10.00"), null, 10);
    }

    @Test(expected = NullPointerException.class)
    public void testConstructorRepositoryNull() {
        new SampleSupplier(Collections.emptyList(), null, LocalDateTime.parse("2019-02-05T14:08:00.00"), LocalDateTime.parse("2019-02-05T14:08:10.00"), null, 10);
    }

    @Test(expected = NullPointerException.class)
    public void testConstructorStartNull() {
        new SampleSupplier(Collections.emptyList(), sampleRepository, null, LocalDateTime.parse("2019-02-05T14:08:10.00"), null, 10);
    }

    @Test(expected = NullPointerException.class)
    public void testConstructorEndNull() {
        new SampleSupplier(Collections.emptyList(), sampleRepository, LocalDateTime.parse("2019-02-05T14:08:00.00"), null, null, 10);
    }

    @Test
    public void testHasNextOneMeasure() {
        Measure measure = measureRepository.findById(1L);
        SampleSupplier supplier = new SampleSupplier(Collections.singletonList(measure), sampleRepository, LocalDateTime.parse("2019-02-05T14:08:00.00"), LocalDateTime.parse("2019-02-05T14:08:10.00"), null, 1);
        assertTrue(supplier.hasNext());
        for (int i = 0; i < 40; i++) {
            supplier.getValue(measure);
            assertTrue(supplier.hasNext());
        }
        supplier.getValue(measure);
        assertFalse(supplier.hasNext());
    }

    @Test
    public void testHasNextTwoMeasures() {
        Measure measure = measureRepository.findById(1L);
        Measure measure2 = measureRepository.findById(2L);
        SampleSupplier supplier = new SampleSupplier(Arrays.asList(measure, measure2), sampleRepository, LocalDateTime.parse("2019-02-05T14:08:00.00"), LocalDateTime.parse("2019-02-05T14:08:07.00"), null, 100);
        assertTrue(supplier.hasNext());
        for (int i = 0; i < 700; i++) {
            supplier.getValue(measure);
            supplier.getValue(measure2);
            assertTrue(supplier.hasNext());
        }
        supplier.getValue(measure);
        supplier.getValue(measure2);
        assertFalse(supplier.hasNext());
    }

    @Test
    public void testHasNextOneMeasureWithDates() {
        Measure measure = measureRepository.findById(1L);
        SampleSupplier supplier = new SampleSupplier(Collections.singletonList(measure), sampleRepository, LocalDateTime.parse("2019-02-05T14:08:01.00"), LocalDateTime.parse("2019-02-05T14:08:02.00"), null, 1);
        assertTrue(supplier.hasNext());
        for (int i = 0; i < 10; i++) {
            supplier.getValue(measure);
            assertTrue(supplier.hasNext());
        }
        supplier.getValue(measure);
        assertFalse(supplier.hasNext());
    }

    @Test
    public void testGetValuesMultipleMeasuresWithDatesAndModifier() {
        Measure measure1 = measureRepository.findById(1L);
        Measure measure2 = measureRepository.findById(2L);
        Modification modification = modificationRepository.findOne(1L);
        SampleModifier modifier = new SampleModifier(modification);

        List<Double> expectedMeasure1 = Arrays.asList(3d, 3d, 3d, 3d, 3d, 3d, 3d, 3d, 3d, 3d, 15d);
        List<Double> expectedMeasure2 = Arrays.asList(1d, 1d, 1d, 1d, 1d, 1d, 1d, 1d, 1d, 1d, 1d);

        SampleSupplier supplier = new SampleSupplier(Arrays.asList(measure1, measure2), sampleRepository, LocalDateTime.parse("2019-02-05T14:08:01.00"), LocalDateTime.parse("2019-02-05T14:08:02.00"), modifier, 1);
        assertTrue(supplier.hasNext());

        ArrayList<Double> resultMeasure1 = new ArrayList<>();
        ArrayList<Double> resultMeasure2 = new ArrayList<>();
        while (supplier.hasNext()) {
            List<Sample> samples = supplier.getValues();
            resultMeasure1.add(samples.get(0).getValue());
            resultMeasure2.add(samples.get(1).getValue());
        }
        assertEquals(expectedMeasure1, resultMeasure1);
        assertEquals(expectedMeasure2, resultMeasure2);
    }

    @Test
    public void testGetValuesOneMeasureWithDatesAndModifier() {
        Measure measure = measureRepository.findById(1L);
        Modification modification = modificationRepository.findOne(1L);
        SampleModifier modifier = new SampleModifier(modification);

        List<Double> expected = Arrays.asList(3d, 3d, 3d, 3d, 3d, 3d, 3d, 3d, 3d, 3d, 15d);

        SampleSupplier supplier = new SampleSupplier(Collections.singletonList(measure), sampleRepository, LocalDateTime.parse("2019-02-05T14:08:01.00"), LocalDateTime.parse("2019-02-05T14:08:02.00"), modifier, 1);
        assertTrue(supplier.hasNext());
        ArrayList<Double> result = new ArrayList<>();
        while (supplier.hasNext()) {
            result.add(supplier.getValue(measure).getValue());
        }
        assertEquals(expected, result);
    }
}
