package com.oreo.unitary.protocol;

import com.oreo.OreoApplication;
import com.oreo.protocol.AlarmSupplier;
import com.oreo.repository.AlarmRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class)
public class AlarmSupplierTest {

    @Autowired
    private AlarmRepository repository;

    @Test
    public void testHasNext() {
        AlarmSupplier supplier = new AlarmSupplier(1, repository, LocalDateTime.parse("2019-02-05T14:08:00"),
                LocalDateTime.parse("2019-02-05T14:08:20"));
        for (int i = 0; i < 4; i++) {
            supplier.next();
        }
        assertFalse(supplier.hasNext());
    }

    @Test(expected = NoSuchElementException.class)
    public void testNextNoSuchElement() {
        AlarmSupplier supplier = new AlarmSupplier(1, repository, LocalDateTime.parse("2019-02-05T14:08:00"),
                LocalDateTime.parse("2019-02-05T14:08:20"));
        for (int i = 0; i < 5; i++) {
            supplier.next();
        }
    }
}
