package com.oreo.unitary.api;

import com.oreo.OreoApplication;
import com.oreo.entity.Measure;
import com.oreo.entity.Modification;
import com.oreo.entity.Operation;
import com.oreo.entity.PartialModification;
import com.oreo.repository.MeasureRepository;
import com.oreo.repository.ModificationRepository;
import com.oreo.unitary.RestResponsePage;
import com.oreo.unitary.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MeasureApiTest {

    @LocalServerPort
    private int port;

    @Autowired
    MeasureRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    private HttpHeaders headers = new HttpHeaders();

    @Test
    public void testAdvancedSearchAllResultFromApi() {
        List<Measure> measures = Arrays.asList(
                new Measure(1, "measure1", "°C", 10, 1),
                new Measure(2, "measure2", "km/h", 10, 1),
                new Measure(3, "measure3", "N", 10, 1),
                new Measure(4, "measure4", "kg", 10, 1),
                new Measure(5, "measure5", "°C", 10, 1),
                new Measure(6, "measure6", "km/h", 10, 1),
                new Measure(7, "measure7", "N", 10, 1),
                new Measure(8, "measure8", "kg", 10, 1),
                new Measure(9, "measure9", "°C", 10, 1));

        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Measure>>> response = restTemplate.exchange(TestUtils.url(port, "/measures/advancedSearch?experimentId=1&name=measure"),
                HttpMethod.GET, entity, new ParameterizedTypeReference<RestResponsePage<List<Measure>>>(){});
        assertEquals(measures, response.getBody().getContent().get("measures"));
    }

    @Test
    public void testAdvancedSearchOneResultFromApi() {
        List<Measure> measures = Collections.singletonList(
                new Measure(1, "measure1", "°C", 10, 1));

        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Measure>>> response = restTemplate.exchange(TestUtils.url(port, "/measures/advancedSearch?experimentId=1&name=1"),
                HttpMethod.GET, entity, new ParameterizedTypeReference<RestResponsePage<List<Measure>>>(){});
        assertEquals(measures, response.getBody().getContent().get("measures"));
    }
}
