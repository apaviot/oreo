package com.oreo.unitary.api;

import com.oreo.OreoApplication;
import com.oreo.entity.Modification;
import com.oreo.entity.Operation;
import com.oreo.entity.PartialModification;
import com.oreo.repository.ModificationRepository;
import com.oreo.unitary.RestResponsePage;
import com.oreo.unitary.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ModificationApiTest {

    @LocalServerPort
    private int port;

    @Autowired
    ModificationRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    private HttpHeaders headers = new HttpHeaders();

    @Test
    public void testGetByIdFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        System.out.println(TestUtils.url(port, "/modifications/1"));
        ResponseEntity<Modification> response = restTemplate.exchange(TestUtils.url(port, "/modifications/1"),
                HttpMethod.GET, entity, Modification.class);
        assertEquals(new Modification(1L), response.getBody());
    }

    @Test
    public void testGetByExperimentIdFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Modification>>> response = restTemplate.exchange(TestUtils.url(port, "/modifications/search/findByExperimentId?experimentId=1"),
                HttpMethod.GET, entity, new ParameterizedTypeReference<RestResponsePage<List<Modification>>>(){});
        assertEquals(Collections.singletonList(new Modification(1)), response.getBody().getContent().get("modifications"));
    }

    @Test
    public void testGetByExperimentIdEmptyFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Modification>>> response = restTemplate.exchange(TestUtils.url(port, "/modifications/search/findByExperimentId?experimentId=5"),
                HttpMethod.GET, entity, new ParameterizedTypeReference<RestResponsePage<List<Modification>>>(){});
        assertEquals(Collections.emptyList(), response.getBody().getContent().get("modifications"));
    }

    @Test
    public void testGetByExperimentIdNonExistantFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<RestResponsePage<List<Modification>>> response = restTemplate.exchange(TestUtils.url(port, "/modifications/search/findByExperimentId?experimentId=99"),
                HttpMethod.GET, entity, new ParameterizedTypeReference<RestResponsePage<List<Modification>>>(){});
        assertEquals(Collections.emptyList(), response.getBody().getContent().get("modifications"));
    }

    @Test
    public void testDeleteByIdFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/modifications/1"), HttpMethod.DELETE, entity, String.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        ResponseEntity<String> response2 = restTemplate.exchange(TestUtils.url(port, "/modifications/1"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.NOT_FOUND, response2.getStatusCode());
    }

    @Test
    public void testSavePostFromApi() {
        Modification modification = new Modification();
        PartialModification partial = new PartialModification(0, "2019-02-05T14:08:00.00", "2019-02-05T14:08:04.00", 10, Operation.ADD, 1);
        modification.addPartial(partial);

        HttpEntity<Modification> entity = new HttpEntity<>(modification, headers);
        ResponseEntity<Modification> response = restTemplate.exchange(TestUtils.url(port, "/modifications"),
                HttpMethod.POST, entity, Modification.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(new Modification(2), repository.findOne(2L));
    }
}
