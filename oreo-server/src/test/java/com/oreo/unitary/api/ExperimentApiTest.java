package com.oreo.unitary.api;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oreo.OreoApplication;
import com.oreo.entity.Experiment;
import com.oreo.repository.ExperimentRepository;
import com.oreo.unitary.TestUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest(classes = OreoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ExperimentApiTest {

    @LocalServerPort
    private int port;

    @Autowired
    ExperimentRepository repository;

    @Autowired
    private TestRestTemplate restTemplate;

    private HttpHeaders headers = new HttpHeaders();

    @Test
    public void testGetAllExperimentsFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/experiments"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetExperimentByIdFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/experiments/1"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetExperimentByIdNonExistantFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/experiments/0"), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testJsonFormatFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/experiments/1"), HttpMethod.GET, entity, String.class);

        JsonObject json = new JsonParser().parse(response.getBody()).getAsJsonObject();
        HashSet<String> keys = new HashSet<>();
        keys.add("id");
        keys.add("reference");
        keys.add("name");
        keys.add("lastUse");
        keys.add("isLocal");
        keys.add("begin");
        keys.add("end");
        keys.add("campaignId");
        keys.add("benchId");
        assertEquals(keys, json.keySet());
    }

    @Test
    public void testDeleteFromApi() {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/experiments/1"), HttpMethod.DELETE, entity, String.class);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void testSavePutIsNotAllowedFromApi() {
        Experiment experiment = new Experiment(1, "experimentTest", "experimentTest", "2019-02-05T14:12:00", false, 1, 2);
        HttpEntity<Experiment> entity = new HttpEntity<>(experiment, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/experiments/1"), HttpMethod.PUT, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

    @Test
    public void testSavePostIsNotAllowedFromApi() {
        Experiment experiment = new Experiment(1, "experimentTest", "experimentTest", "2019-02-05T14:12:00", false, 1, 2);
        HttpEntity<Experiment> entity = new HttpEntity<>(experiment, headers);
        ResponseEntity<String> response = restTemplate.exchange(TestUtils.url(port, "/experiments"), HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());
    }

}
