package com.oreo.protocol;

public enum ProtocolEnum {
    IENA,
    DDSRTI,
    AVIS;

    public static final String IENA_KEY = "iena";
    public static final String DDSRTI_KEY = "ddsrti";
    public static final String AVIS_KEY = "avis";
}
