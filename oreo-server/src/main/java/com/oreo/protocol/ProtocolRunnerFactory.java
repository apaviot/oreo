package com.oreo.protocol;

import com.oreo.protocol.ddsrti.DDSRTIRunner;
import com.oreo.protocol.iena.IENARunner;

public class ProtocolRunnerFactory {
    private static IENARunner ienaRunner = new IENARunner();
    private static DDSRTIRunner ddsrtiExecutor = new DDSRTIRunner();

    private ProtocolRunnerFactory(){
    }

    /**
     * Renvoi un protocole runner à partir de l'id d'un protocol
     * @param protocolId l'identifiant du protocol
     * @return le protocol runner associé à l'identifiant du protocol. null si aucun runner ne correspond au protocol
     */
    public static ProtocolRunner get(String protocolId) {
        switch (protocolId) {
            case "iena":
                return ienaRunner;
            case "ddsrti":
                return ddsrtiExecutor;
            default:
                return null;
        }
    }

}
