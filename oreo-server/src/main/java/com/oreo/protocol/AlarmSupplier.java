package com.oreo.protocol;

import com.oreo.entity.Alarm;
import com.oreo.repository.AlarmRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Classe en charge de fournir les alarmes à envoyer.
 *
 * Toutes les alarmes ne sont pas chargées en mémoire. Elle sont récupérées au fur et à mesure.
 */
public class AlarmSupplier implements Iterator<Alarm> {
    private static final int NB_ALARM_IN_MEMORY = 100; // Nombre maximum d'alarmes que l'on monte en mémoire

    private final AlarmRepository alarmRepository;
    private final long experimentId;
    private final LocalDateTime start;
    private final LocalDateTime end;

    private List<Alarm> alarms = new ArrayList<>();
    private int offset = 0;
    private int currentIndex = 0;

    /**
     * Constructeur
     *
     * @param experimentId identifiant de l'essai auquel appartiennent les alarmes à charger
     * @param repository le repository des alarmes
     * @param start date à partir de laquelle récupérer les alarmes
     * @param end date limite jusqu'à laquele récupérer les alarmes
     */
    public AlarmSupplier(long experimentId, AlarmRepository repository, LocalDateTime start, LocalDateTime end) {
        this.alarmRepository = Objects.requireNonNull(repository);
        this.start = Objects.requireNonNull(start);
        this.end = Objects.requireNonNull(end);
        this.experimentId = experimentId;
    }

    /**
     * Renvoie la prochaine alarme
     * @return la prochaine alarme
     * @throws NoSuchElementException s'il n'y a plus d'alarmes à récupérer
     */
    @Override
    public Alarm next() {
        if (!hasNext()) {
            throw new NoSuchElementException("Il n'y a plus d'alarme à récupérer");
        }
        return alarms.get(currentIndex++);
    }

    /**
     * Retourne true s'il y a encore une alarme à récupérer, false sinon
     * @return true s'il y a encore une alarme à récupérer, false sinon
     */
    @Override
    public boolean hasNext() {
        if (currentIndex >= alarms.size()) {
            boolean filled = fill();
            currentIndex = filled ? 0 : currentIndex;
            return filled;
        }
        return true;
    }

    /**
     * Récupère les prochaines alarmes en base de données.
     * @return true si des alarmes ont été récupérées, false sinon
     */
    private boolean fill() {
        PageRequest page = new PageRequest(offset++, NB_ALARM_IN_MEMORY, new Sort(Sort.Direction.ASC, "time"));
        this.alarms = alarmRepository.findByExperimentIdAndDate(experimentId, start, end, page).getContent();
        return !alarms.isEmpty();
    }
}
