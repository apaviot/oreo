package com.oreo.protocol;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Classe qui définit le json représentant une configuration de protocol
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ProtocolConfiguration {
    @JsonProperty
    private String moduleName;

    @JsonProperty
    private String ip;

    @JsonProperty
    private int port;

    @JsonProperty
    private String mac;

    @JsonProperty
    private int domainID;

    @JsonProperty
    private String topic;

    @JsonProperty
    private String uri;

    @JsonProperty
    private int key;

    @JsonProperty
    private int frequency = 1;

    public String getModuleName() {
        return moduleName;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public String getMac() {
        return mac;
    }

    public String getTopic() {
        return topic;
    }

    public String getUri() {
        return uri;
    }

    public int getKey() {
        return key;
    }

    public int getFrequency() {
        return frequency;
    }

    public int getDomainID() {
        return domainID;
    }

    public void setDomainID(int domainID) {
        this.domainID = domainID;
    }

    @Override
    public String toString() {
        return "Configuration {moduleName: " + moduleName + ", ip: " + ip + ", port: " + port + ", mac: " + mac + ", topic: " + topic + ", uri: " + uri + "}";
    }
}
