package com.oreo.protocol.communication.appli;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oreo.utils.LocalDateTimeDeserializer;
import com.oreo.utils.LocalDateTimeSerializer;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Classe représentant le json décrivant les données à envoyer avec un protocol
 */
public class ProtocolExecutionPayload implements Serializable {
    /**
     * True si la date des échantillons à envoyer doit être recalculée sur la date courrante, False pour utiliser la date stockée en base
     */
    private boolean computeTime;
    private List<Long> measures;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime start;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime end;

    private Long modificationId;

    public ProtocolExecutionPayload() {
        this(false, Collections.emptyList(), LocalDateTime.now(), LocalDateTime.now(), null);
    }

    public ProtocolExecutionPayload(boolean computeTime, List<Long> measures, LocalDateTime start, LocalDateTime end, Long modificationId) {
        this.computeTime = computeTime;
        this.measures = Objects.requireNonNull(measures);
        this.start = Objects.requireNonNull(start);
        this.end = Objects.requireNonNull(end);
    }

    /**
     * True si la date des échantillons à envoyer doit être recalculée sur la date courrante, False pour utiliser la date stockée en base
     */
    public boolean isComputeTime() {
        return computeTime;
    }

    /**
     * Indique si la date des échantillons doit être recalculée sur la date courrante
     *
     * @param computeTime true si la date des échantillons à envoyer doit être recalculée sur la date courrante, false pour utiliser la date stockée en base
     */
    public void setComputeTime(boolean computeTime) {
        this.computeTime = computeTime;
    }

    public List<Long> getMeasures() {
        return measures;
    }

    public void setMeasures(List<Long> measures) {
        this.measures = Objects.requireNonNull(measures);
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = Objects.requireNonNull(start);
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = Objects.requireNonNull(end);
    }

    public Long getModificationId() {
        return modificationId;
    }

    public void setModificationId(Long modificationId) {
        this.modificationId = modificationId;
    }

}
