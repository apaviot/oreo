package com.oreo.protocol;

import org.apache.http.conn.util.InetAddressUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe utilitaire pour vérifier les configuration des protocoles
 */
public class ProtocolChecker {
    private final static int MILLIS_PER_SECOND = 1000;

    /**
     * Vérifie la configuration du protocol.
     *
     * @param id     l'identifiant du protocol
     * @param config la configuration du protocol
     */
    public static void check(String id, ProtocolConfiguration config) {
        switch (id) {
            case ProtocolEnum.IENA_KEY:
                checkIENA(config);
                break;
            case ProtocolEnum.DDSRTI_KEY:
                checkDDSRTI(config);
                break;
            case ProtocolEnum.AVIS_KEY:
                checkAVIS(config);
                break;
        }
    }

    /**
     * Vérifie la configuration de IENA
     * <p>
     * - module name de doit pas être null ni vide
     * - l'ip ne doit pas être null ni vide et doit respecter le pattern d'une adresse IP V4
     * - l'adresse MAC ne doit ni etre null ni vide
     * - la fréquence ne doit pas être inférieure à 0
     *
     * @param config la configuration du protocol
     */
    private static void checkIENA(ProtocolConfiguration config) {
        if (config.getModuleName() == null || config.getModuleName().isEmpty()) {
            throw new IllegalArgumentException("Module name shouldn't be null");
        }
        if (config.getIp() == null || config.getIp().isEmpty() || !InetAddressUtils.isIPv4Address(config.getIp())) {
            throw new IllegalArgumentException("IP shouldn't be null or should be valid");
        }

        if (config.getMac() == null || config.getMac().isEmpty()) {
            throw new IllegalArgumentException("Mac shouldn't be null");
        }

        Pattern pattern = Pattern.compile("^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$");
        Matcher matcher = pattern.matcher(config.getMac());

        if (!matcher.find()) {
            throw new IllegalArgumentException("Mac should be valid");
        }

        if (config.getFrequency() <= 0 || config.getFrequency() > MILLIS_PER_SECOND) {
            throw new IllegalArgumentException("Frequency must be positive and under 1000");
        }
    }

    /**
     * Vérifie la configuration du protocol DDS RTI
     * <p>
     * - l'ip ne doit pas être null ni vide et doit respecter le pattern d'une adresse IP V4
     * - le topic ne doit ni être null ni vide
     *
     * @param config la configuration du protocol
     */
    private static void checkDDSRTI(ProtocolConfiguration config) {
        if (config.getIp() == null || config.getIp().isEmpty() || !InetAddressUtils.isIPv4Address(config.getIp())) {
            throw new IllegalArgumentException("IP shouldn't be null or should be valid");
        }
        if (config.getTopic() == null || config.getTopic().isEmpty()) {
            throw new IllegalArgumentException("Topic shouldn't be null");
        }
    }

    /**
     * Vérifie la configuration du protocol AVIS
     * <p>
     * - l'uri ne doit ni être null ni vide
     *
     * @param config la configuration du protocol
     */
    private static void checkAVIS(ProtocolConfiguration config) {
        if (config.getUri() == null || config.getUri().isEmpty()) {
            throw new IllegalArgumentException("Mac shouldn't be null");
        }
    }
}
