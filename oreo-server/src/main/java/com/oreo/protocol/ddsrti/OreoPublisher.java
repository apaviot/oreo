package com.oreo.protocol.ddsrti;

import com.oreo.entity.Sample;
import com.oreo.protocol.SampleSupplier;
import com.rti.dds.domain.DomainParticipant;
import com.rti.dds.domain.DomainParticipantFactory;
import com.rti.dds.infrastructure.InstanceHandle_t;
import com.rti.dds.infrastructure.StatusKind;
import com.rti.dds.publication.Publisher;
import com.rti.dds.topic.Topic;

import java.util.List;
import java.util.Objects;

public class OreoPublisher {

    private final DomainParticipant participant;
    private final OreoDataWriter writer;
    private final SampleSupplier sampleSupplier;
    private Thread thread;

    private OreoPublisher(DomainParticipant participant, OreoDataWriter writer, SampleSupplier sampleSupplier) {
        this.participant = Objects.requireNonNull(participant);
        this.writer = Objects.requireNonNull(writer);
        this.sampleSupplier = Objects.requireNonNull(sampleSupplier);
    }

    public static OreoPublisher create(int domainId, SampleSupplier sampleSupplier) {
        System.out.println(domainId);
        DomainParticipant participant = DomainParticipantFactory.TheParticipantFactory.create_participant(
                domainId, DomainParticipantFactory.PARTICIPANT_QOS_DEFAULT, null /* listener */, StatusKind.STATUS_MASK_NONE);

        Publisher publisher = participant.create_publisher(DomainParticipant.PUBLISHER_QOS_DEFAULT, null /* listener */,
                StatusKind.STATUS_MASK_NONE);

        String typeName = OreoTypeSupport.get_type_name();
        OreoTypeSupport.register_type(participant, typeName);

        Topic topic = participant.create_topic("SAFRAN OREO", typeName, DomainParticipant.TOPIC_QOS_DEFAULT,
                null /* listener */, StatusKind.STATUS_MASK_NONE);

        OreoDataWriter writer = (OreoDataWriter) publisher.create_datawriter(topic, Publisher.DATAWRITER_QOS_DEFAULT,
                null /* listener */, StatusKind.STATUS_MASK_NONE);

        return new OreoPublisher(participant, writer, sampleSupplier);
    }

    public void start() {
        thread = new Thread(() -> {
            Oreo instance = new Oreo();
            InstanceHandle_t instance_handle = InstanceHandle_t.HANDLE_NIL;
            while (sampleSupplier.hasNext()) {
                List<Sample> samples = sampleSupplier.getValues();
                for (Sample s : samples) {
                    fillInstance(instance, s);
                    writer.write(instance, instance_handle);
                }
            }
            if (participant != null) {
                participant.delete_contained_entities();
                DomainParticipantFactory.TheParticipantFactory.delete_participant(participant);
            }
        });
        thread.start();
    }

    public void stop() {
        if (thread != null) {
            thread.interrupt();
        }
    }

    private void fillInstance(Oreo instance, Sample sample) {
        instance.date = sample.getTime().toString();
        instance.value = sample.getValue();
        instance.mesure = "mesure";
        instance.unit = "unit";
    }
}

