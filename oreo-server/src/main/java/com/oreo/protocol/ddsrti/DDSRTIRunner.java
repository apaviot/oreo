package com.oreo.protocol.ddsrti;

import com.oreo.entity.Measure;
import com.oreo.protocol.AlarmSupplier;
import com.oreo.protocol.ProtocolConfiguration;
import com.oreo.protocol.ProtocolRunner;
import com.oreo.protocol.SampleSupplier;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Implémentation de ProtocolRunner pour faire fonctionner DDS RTI
 */
public class DDSRTIRunner implements ProtocolRunner {
    private OreoPublisher publisher;

    /**
     * DDS RTI n'exporte pas de fichier de configuration. La réponse http renverra un code NOT IMPLEMENTED
     * La réponse renvoit un code http INTERNAL SERVER ERROR si l'écriture de le writer de la réponse échoue
     *
     * @param response      réponse http
     * @param configuration la configuration du protocol
     * @param measures      la liste des mesures à envoyer avec la configuration
     */
    @Override
    public void exportFile(HttpServletResponse response, ProtocolConfiguration configuration, List<Measure> measures) {
        try {
            response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
            response.getWriter().println("Impossible d'exporter un fichier de configuration pour le protocol DDS RTI");
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Démarre l'envoi de données avec DDS RTI.
     * <p>
     * Démarre un OreoPublisher.
     *
     * @param sampleSupplier fournisseur d'échantillons
     * @param alarmSupplier  fournisseur d'alarmes
     * @param configuration  la configuration du protocol
     * @param computeTime    boolean indiquant si la date des échantillon doit être recalculée à partir de la date courrante
     */
    @Override
    public void sendData(SampleSupplier sampleSupplier, AlarmSupplier alarmSupplier, ProtocolConfiguration configuration, boolean computeTime) {
        publisher = OreoPublisher.create(configuration.getDomainID(), sampleSupplier);
        publisher.start();
    }

    /**
     * Interrompt l'envoi de données avec DDS RTI.
     * <p>
     * Si aucun envoi n'était en cours, rien ne se passe.
     */
    @Override
    public void stop() {
        if (publisher != null) {
            publisher.stop();
        }
    }
}
