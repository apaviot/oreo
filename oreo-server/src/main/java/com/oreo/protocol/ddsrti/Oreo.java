package com.oreo.protocol.ddsrti;

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from .idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

import com.rti.dds.infrastructure.*;
import com.rti.dds.infrastructure.Copyable;
import java.io.Serializable;
import com.rti.dds.cdr.CdrHelper;

public class Oreo   implements Copyable, Serializable{

    public double value= 0;
    public String date=  "" ; /* maximum length = (128) */
    public String mesure=  "" ; /* maximum length = (128) */
    public String unit=  "" ; /* maximum length = (128) */

    public Oreo() {

    }
    public Oreo (Oreo other) {

        this();
        copy_from(other);
    }

    public static Object create() {

        Oreo self;
        self = new  Oreo();
        self.clear();
        return self;

    }

    public void clear() {

        value= 0;
        date=  ""; 
        mesure=  ""; 
        unit=  ""; 
    }

    public boolean equals(Object o) {

        if (o == null) {
            return false;
        }        

        if(getClass() != o.getClass()) {
            return false;
        }

        Oreo otherObj = (Oreo)o;

        if(value != otherObj.value) {
            return false;
        }
        if(!date.equals(otherObj.date)) {
            return false;
        }
        if(!mesure.equals(otherObj.mesure)) {
            return false;
        }
        if(!unit.equals(otherObj.unit)) {
            return false;
        }

        return true;
    }

    public int hashCode() {
        int __result = 0;
        __result += (int)value;
        __result += date.hashCode(); 
        __result += mesure.hashCode(); 
        __result += unit.hashCode(); 
        return __result;
    }

    /**
    * This is the implementation of the <code>Copyable</code> interface.
    * This method will perform a deep copy of <code>src</code>
    * This method could be placed into <code>OreoTypeSupport</code>
    * rather than here by using the <code>-noCopyable</code> option
    * to rtiddsgen.
    * 
    * @param src The Object which contains the data to be copied.
    * @return Returns <code>this</code>.
    * @exception NullPointerException If <code>src</code> is null.
    * @exception ClassCastException If <code>src</code> is not the 
    * same type as <code>this</code>.
    * @see com.rti.dds.infrastructure.Copyable#copy_from(Object)
    */
    public Object copy_from(Object src) {

        Oreo typedSrc = (Oreo) src;
        Oreo typedDst = this;

        typedDst.value = typedSrc.value;
        typedDst.date = typedSrc.date;
        typedDst.mesure = typedSrc.mesure;
        typedDst.unit = typedSrc.unit;

        return this;
    }

    public String toString(){
        return toString("", 0);
    }

    public String toString(String desc, int indent) {
        StringBuffer strBuffer = new StringBuffer();        

        if (desc != null) {
            CdrHelper.printIndent(strBuffer, indent);
            strBuffer.append(desc).append(":\n");
        }

        CdrHelper.printIndent(strBuffer, indent+1);        
        strBuffer.append("value: ").append(value).append("\n");  
        CdrHelper.printIndent(strBuffer, indent+1);        
        strBuffer.append("date: ").append(date).append("\n");  
        CdrHelper.printIndent(strBuffer, indent+1);        
        strBuffer.append("mesure: ").append(mesure).append("\n");  
        CdrHelper.printIndent(strBuffer, indent+1);        
        strBuffer.append("unit: ").append(unit).append("\n");  

        return strBuffer.toString();
    }

}
