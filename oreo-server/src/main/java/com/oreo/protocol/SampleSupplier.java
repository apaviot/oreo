package com.oreo.protocol;

import com.oreo.entity.Measure;
import com.oreo.entity.Sample;
import com.oreo.repository.SampleRepository;
import com.oreo.utils.SampleModifier;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

/**
 * Fournisseur d'échantillons.
 *
 * Pour ne pas charger tous les échantillons en mémoire, un nombre limité est chargé en mémoire. Une fois que ceux-ci ont été consommés
 * la suite des échantillons est chargée.
 *
 * Un SampleModifier peut également être fournit pour modifier les échantillons à la volée.
 */
public class SampleSupplier {
    private static final int NB_SAMPLES_IN_MEMORY = 10;
    private static final int MILLIS_PER_SECOND = 1000;

    private final SampleRepository sampleRepository;
    private final LinkedHashMap<Measure, MeasureState> states = new LinkedHashMap<>();
    private final LocalDateTime start;
    private final LocalDateTime end;
    private final SampleModifier modifier;
    private final long frequency;
    private int nbMeasuresInProgress;

    /**
     * Constructeur
     *
     * @param measures la liste des mesures pour lesquelle récupérer les échantillons
     * @param repository repository des échantillons
     * @param start date à partir de laquelle récupérer les échantillons
     * @param end date limite jusqu'à laquelle récupérer les échantillons
     * @param modifier modificateur d'échantillons. Peut être null
     * @param sampleByMilli interval en millisecondes entre deux échantillons d'un même mesure
     */
    public SampleSupplier(List<Measure> measures, SampleRepository repository, LocalDateTime start, LocalDateTime end, SampleModifier modifier, long sampleByMilli) {
        this.sampleRepository = Objects.requireNonNull(repository);
        this.start = Objects.requireNonNull(start);
        this.end = Objects.requireNonNull(end);
        this.frequency = (MILLIS_PER_SECOND / sampleByMilli);
        if (modifier == null) {
            modifier = SampleModifier.IDENTITY;
        }
        this.modifier = modifier;
        nbMeasuresInProgress = measures.size();
        measures.forEach(m -> states.put(m, new MeasureState(m, start)));
    }

    /**
     * Retourne le prochain échantillon pour la mesure spécifiée.
     *
     * Attention, il est préférable de ne pas mélanger l'utilisation de getValue et de getValues.
     *
     * @param measure la mesure pour laquelle récupérer le prochain échantillon
     * @return le prochain échantillon de la mesure renseignée
     * @throws IllegalStateException s'il n'y a plus d'échantillon pour la mesure renseignée
     */
    public Sample getValue(Measure measure) {
        MeasureState state = states.get(Objects.requireNonNull(measure));
        if (state == null) {
            throw new IllegalStateException("The given measure was not initialized. " + measure);
        }
        return modifier.apply(state.getValue(), measure.getId());
    }

    /**
     * Retourne la liste des prochains échantillons de chaque mesure
     * @return la liste des prochains échantillons de chaque mesure
     */
    public List<Sample> getValues() {
        ArrayList<Sample> values = new ArrayList<>();
        states.forEach((k, v) -> values.add(modifier.apply(v.getValue(), k.getId())));
        return values;
    }

    /**
     * Retourne true si au moins une mesure a encore des échantillons à retourner, false sinon
     * @return true si au moins une mesure a encore des échantillons à retourner, false sinon
     */
    public boolean hasNext() {
        return nbMeasuresInProgress > 0;
    }

    private class MeasureState {
        Measure measure;
        boolean isFinish = false;
        int offset = 0;
        Sample lastSample;
        int currentIndex = 0;
        List<Sample> samples = new ArrayList<>();

        MeasureState(Measure measure, LocalDateTime start) {
            lastSample = new Sample(0, Double.NaN, start);
            this.measure = Objects.requireNonNull(measure);
            fill();
        }

        /**
         * Remplit la liste des échantillons en faisant une requête à la base de données.
         * @return true si au moins un échantillon est récupéré, false sinon
         */
        boolean fill() {
            samples = sampleRepository.findAll(measure.getId(), start, end, offset, NB_SAMPLES_IN_MEMORY);
            if (samples.isEmpty()) {
                isFinish = true;
                SampleSupplier.this.nbMeasuresInProgress--;
                return false;
            }
            offset += NB_SAMPLES_IN_MEMORY;
            currentIndex = -1;
            return true;
        }

        Sample getValue() {
            LocalDateTime lastSamplePlusFrequency = lastSample.getTime().plus(SampleSupplier.this.frequency, ChronoUnit.MILLIS);

            // Si la mesure n'a plus d'échantillon
            if (isFinish) {
                // Je met à jour la date du dernier échantillon envoyé car la date du paquet
                // correspond à la plus petite date des échantillons du paquet.
                // Si on ne la met pas à jour, la date du paquet restera bloquée à la date où la première mesure n'aura plus d'échantillon
                lastSample.setTime(lastSamplePlusFrequency);
                return lastSample;
            }

            // Si je n'ai pas assez d'échantillons dans la liste, j'essaye de la remplir
            if (samples.size() <= currentIndex + 1) {
                System.out.println("refill");
                if (!fill()) {
                    System.out.println("not enought");
                    lastSample.setTime(lastSamplePlusFrequency);
                    return lastSample;
                }
            }

            Sample nextRealSample = samples.get(currentIndex + 1); // prochain vrai échantillon
            if (nextRealSample.getTime().isAfter(lastSamplePlusFrequency)) {
                // Le prochain vrai échantillon est trop loin pour respecter la fréquence
                // On prend l'ancienne valeur mais on met à jour sa date pour dire qu'on l'a renvoyée
                lastSample.setTime(lastSamplePlusFrequency);
            } else {
                // L'échantillon est assez proche pour respecter la fréquence.
                // On l'envoie et on le définit comme dernier échantillon envoyé.
                // On avance dans l'index
                lastSample = nextRealSample;
                currentIndex++;
            }

            if (samples.size() < NB_SAMPLES_IN_MEMORY && (currentIndex + 1) >= samples.size()) {
                isFinish = true;
                SampleSupplier.this.nbMeasuresInProgress--;
            }

            return lastSample;
        }
    }
}
