package com.oreo.protocol.iena.net.UDP;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

/**
 * Client UDP
 */
public class UDPClient {

    private final DatagramChannel dc;

    private final SocketAddress dest;

    private UDPClient(DatagramChannel dc, SocketAddress dest) {
        this.dc = dc;
        this.dest = dest;
    }

    /**
     * Crée un client udp et ouvre une socket.
     *
     * @param host host du client avec lequel ouvrir la connexion
     * @param port port du client avec lequel ouvrir la connexion
     * @return le client UDP
     */
    public static UDPClient create(String host, int port) {
        try {
            SocketAddress dest = new InetSocketAddress(host, port);
            DatagramChannel dc = DatagramChannel.open();
            return new UDPClient(dc, dest);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Envoi le contenu du bytebuffer
     *
     * @param bb le bytebuffer en mode écriture.
     */
    public void send(ByteBuffer bb) {
        try {
            bb.flip();
            dc.send(bb, dest);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
