package com.oreo.protocol.iena.xml;

import java.io.IOException;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.apache.xalan.processor.TransformerFactoryImpl;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.oreo.protocol.iena.util.Common;


/**
 * Cr�ation/Lecture d'un document XML.</br>
 * Permet l'ajout d'�l�ments et d'attributs XML depuis le noeud p�re avec leur valeurs.</br>
 * Permet la lecture et l'ajout d'�l�ments depuis un chemin d'�l�ments s�parar�s par '.'.</br>
 * Permet le parcours d'un document existant.
 */
public class XmlDocument {

	/** Logger de la classe */
    private static Logger logger = Logger.getLogger(XmlDocument.class);
	
	/** Chemin/nom du fichier XML */
	private String dtdFileUrl = null;
	/** S�parateur de tag '.' */
	protected final String SEPARATOR = "\\.";

	/** Document : fichier */
	private Document xmldoc;

	/**
	 * Constructeur.
	 */
	public XmlDocument() {
		try {
			xmldoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		} catch (ParserConfigurationException e) {
			logger.debug(Common.getStackTrace(e));
			logger.fatal("Erreur(1) de cr�ation du parser XML");
			System.exit(1);
		} catch (FactoryConfigurationError e) {
			logger.debug(Common.getStackTrace(e));
			logger.fatal("Erreur(2) de cr�ation du parser XML");
			System.exit(1);
		}
	}

	/**
	 * Cr�er un document DOM depuis une uri.
	 * 
	 * @param uri
	 *            l'uri du document
	 * @throws SAXException
	 * @throws IOException
	 */
	public XmlDocument(String uri) throws SAXException, IOException {

		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilderFactory.setValidating(true);
			xmldoc = documentBuilderFactory.newDocumentBuilder().parse(uri);
		} catch (ParserConfigurationException e) {
			logger.debug(Common.getStackTrace(e));
			logger.fatal("Erreur(1) de cr�ation du parser XML : " + uri);
			System.exit(1);			
		} catch (FactoryConfigurationError e) {
			logger.debug(Common.getStackTrace(e));
			logger.fatal("Erreur(2) de cr�ation du parser XML : " + uri);
			System.exit(1);
		}
	}

	/**
	 * Cr�er un �l�ment et l'ajoute au document.
	 * 
	 * @param tagName
	 *            nom de la balise.
	 * @return l'�l�ment ajout�
	 */
	public Element appendElement(String tagName) {
		Element elm;

		// Creates the element
		elm = xmldoc.createElement(tagName);

		// Append it as it child
		xmldoc.appendChild(elm);

		return elm;
	}

	/**
	 * Cr�er un �l�ment fils d'un autre �l�ment et l'ajoute au document.
	 * 
	 * @param father
	 *            l'�l�ment p�re
	 * @param tagName
	 *            le nom de l'�l�ment.
	 * @return l'�l�ment ajout�
	 */
	public Element appendElement(Element father, String tagName) {
		Element elm;

		// Creates the element
		elm = xmldoc.createElement(tagName);

		// Append it as it child
		father.appendChild(elm);

		return elm;
	}

	/**
	 * Cr�er un �l�ment fils avec une valeur et l'ajoute au document.
	 * 
	 * @param father
	 *            l'�l�ment p�re.
	 * @param tagName
	 *            le nom de l'�l�ment � ajouter.
	 * @param value
	 *            la valeur de l'�l�ment.
	 * @return le nouvel �l�ment.
	 */
	public Element appendElement(Element father, String tagName, String value) {
		Element elm;
		Text text;

		// Creates the element
		elm = xmldoc.createElement(tagName);

		// Sets the value
		if (value != null && value.length() > 0) {
			text = xmldoc.createTextNode(value);
			elm.appendChild(text);
		}
		// Append it as it child
		father.appendChild(elm);

		return elm;
	}
	
	/**
	 * Ajoute un texte � un noeud.<br>
	 *
	 * @param elem Noeud recevant le texte.
	 * @param value Texte � ajouter.
	 */
	public void setText(Element elem, String value){
		if (value != null && value.length() > 0) {
			Node child = elem.getFirstChild();
			if(child != null && child.getNodeType() == Node.TEXT_NODE){
				child.setTextContent(value);
			}else{
				Text text = xmldoc.createTextNode(value);
				elem.appendChild(text);
			}
		}
	}
	
	/**
	 * Retourne le texte d'un noeud.<br>
	 *
	 * @param elem Noeud du texte � retourner.
	 * @return Texte du noeud.
	 */
	public String getText(Element elem){
		Node child = elem.getFirstChild();
		if(child != null && child.getNodeType() == Node.TEXT_NODE){
			return child.getTextContent();
			}
		return "";
	}

	/**
	 * Cr�er un �l�ment fils uniquement s'il poss�de une valeur et l'ajoute au
	 * document.
	 * 
	 * @param father
	 *            l'�l�ment p�re.
	 * @param tagName
	 *            le nom du nouvel �l�ment.
	 * @param value
	 *            valeur de l'�l�ment.
	 * @return le nouvel �l�ment.
	 */
	public Element appendElementIfSet(Element father, String tagName, String value) {
		Element elm = null;
		Text text;

		// Sets the value
		if (value != null && value.length() > 0) {
			// Creates the elements
			elm = xmldoc.createElement(tagName);
			text = xmldoc.createTextNode(value);
			elm.appendChild(text);
			// Append it as it child
			father.appendChild(elm);
		}
		return elm;
	}

	/**
	 * Cr�er un commentaire dans un �l�ment et l'ajoute au document.
	 * 
	 * @param father
	 *            l'�l�ment.
	 * @param aComment
	 *            le commentaire.
	 * @return le commentaire en tant qu'�l�ment.
	 */
	public Comment appendComment(Element father, String aComment) {
		Comment elm;

		// Cr�er l'�l�ment
		elm = xmldoc.createComment(aComment);

		// l'ajoute � l'�l�ment p�re
		father.appendChild(elm);

		return elm;
	}

	/**
	 * Cr�er un commentaire dans le document.
	 * 
	 * @param aComment
	 *            le commentaire.
	 */
	public void appendComment(String aComment) {

		// Append it as it child
		xmldoc.appendChild(xmldoc.createComment(aComment));
	}

	/**
	 * Parse un fichier et retourne le Document
	 * 
	 * @param filename
	 *            le nom du fichier
	 * @return Document du fichier pars�.
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public Document readDocument(String filename) throws SAXException, IOException, ParserConfigurationException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		return builder.parse(filename);
	}

	/**
	 * Serialize le Document.
	 * 
	 * @param os
	 *            le outputstream o� placer le document.
	 * @throws IOException
	 */
	public void serializeDocument(OutputStream os) throws IOException {
		//
		// On passe par un XML Transformer...
		//
		TransformerFactory tf = new TransformerFactoryImpl();
		Transformer t;

		try {
			t = tf.newTransformer();
			t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		} catch (TransformerConfigurationException ex) {
			throw new IOException(ex.toString());
		}

		//
		// Propri�t�s du document XML produit...
		//
		t.setOutputProperty("method", "xml");
		t.setOutputProperty("standalone", "no");
		t.setOutputProperty("encoding", "UTF-8");
		t.setOutputProperty(OutputKeys.INDENT, "yes");
	    t.setOutputProperty("{http://xml.customer.org/xslt}indent-amount", "4");
		if (dtdFileUrl != null)
			t.setOutputProperty("doctype-system", dtdFileUrl);

		serializeDocument(os, t);

	}

	/**
	 * serialize un Document dans un fichier.
	 * 
	 * @param os Stream d'�criture
	 * @param t Formatage XML
	 * @throws IOException
	 */
	public void serializeDocument(OutputStream os, Transformer t) throws IOException {

		StreamResult resultFile = new StreamResult(os);
		try {
			t.transform(new DOMSource(xmldoc), resultFile);
		} catch (TransformerException ex) {
			throw new IOException(ex.toString());
		} finally {
			os.close();
		}
	}

	/**
	 * Retourne l'url de la DTD du document XML.<br>
	 *
	 * @return Url de la DTD.
	 */
	public String getDtdFileUrl() {
		return dtdFileUrl;
	}

	/**
	 * Positionne la DTD du document XML.<br>
	 *
	 * @param dtdFileUrl Url de la DTD.
	 */
	public void setDtdFileUrl(String dtdFileUrl) {
		this.dtdFileUrl = dtdFileUrl;
	}
	
	/**
	 * Ajout d'un noeud depuis un chemin de tags.<br>
	 *
	 * @param tag Tag � ajouter, avec son chemin.
	 * @param overwrite Si false, le noeud est ajout� que si ce denier n'exsite pas.
	 * @return Le noeud ajout�, remplac� ou existant.
	 */
	public Element appendElementFromTag(String tag, boolean overwrite){
		String tags[] = tag.split(SEPARATOR);

		if( tag != null){
			return appendChildElementFromTag( this.xmldoc.getDocumentElement(), tags, overwrite);
		}else{
			return null;
		}
	}
	
	/**
	 * Ajoute ou remplace un noeud depuis son chemin de tags.<br>
	 *
	 * @param tag Tag et son chemin � ajouter ou remplacer.
	 * @return Le noeud ajout� ou remplac�.
	 */
	public Element appendElementFromTag(String tag){
		return appendElementFromTag(tag, true);
	}
	
	/**
	 * Ajoute ou remplace (suivant overwrite) un noeud fils direct ou indirect.<br>
	 *
	 * @param elem Le noeud p�re directe ou indirecte.
	 * @param tags Le tag et son chemin du noeud � ajouter.
	 * @param overwrite Si false, le noeud ne peut �tre remplac� s'il existe d�j�.
	 * @return Le noeud ajout�, remplac� ou existant.
	 */
	public Element appendChildElementFromTag( Element elem, String[] tags, boolean overwrite){
		Element childFound = null;
		
		String[] endTags = new String[tags.length-1];
		int i;
		for( i=0; i<endTags.length; i++){
			endTags[i] = tags[i+1];
		}
		
	    NodeList childNodes = elem.getChildNodes();
	    for (int childNum = 0; childNum < childNodes.getLength(); childNum++){
	    	if ( childNodes.item(childNum).getNodeType() == Node.ELEMENT_NODE ) {
	    		Element child = (Element) childNodes.item( childNum ) ;
	    		if( child.getTagName().equals( tags[0] ) ){
	    			childFound = child;
	    			break;
	    		}
	    	}
	    }
	    if( childFound == null || (overwrite == false && tags.length == 1)){
	    	childFound = this.appendElement(elem, tags[0]);
	    }
		if(tags.length > 1){
			return appendChildElementFromTag(childFound, endTags, overwrite);
		}else{
			return childFound;
		} 
	}
	
	/**
	 * Retourne le noeud depuis un chemin de tags.<br>
	 * d�tails.
	 *
	 * @param tag Chemin de tags
	 * @return Le noeud trouv�, sinon null.
	 */
	public Element getElementFromTag(String tag){
		String tags[];

		if( tag != null){
			tags = tag.split(SEPARATOR);
			return getChildElementFromTag( this.xmldoc.getDocumentElement(), tags);
		}else{
			return null;
		}
	}
	
	/**
	 * Retourne le noeud fils (in)direct depuis un chemin de tags.<br>
	 *
	 * @param elem Element p�re direct ou indirect.
	 * @param tags Le chemin du noeud recherch�.
	 * @return Le noeud trouv� sinon null.
	 */
	public Element getChildElementFromTag( Element elem, String[] tags){
		
		// Node configNode = elem.getFirstChild();
	    NodeList childNodes = elem.getChildNodes();
	    for (int childNum = 0; childNum < childNodes.getLength(); childNum++){
	    	if ( childNodes.item(childNum).getNodeType() == Node.ELEMENT_NODE ) {
	    		Element child = (Element) childNodes.item( childNum ) ;
	    		if( child.getTagName().equals( tags[0] ) ){
	    			if(tags.length > 1){
	    				String[] endTags = new String[tags.length-1];
	    				int i;
	    				for( i=0; i<endTags.length; i++){
	    					endTags[i] = tags[i+1];
	    				}
	    				return getChildElementFromTag(child, endTags);
	    			}else{
	    				return child;
	    			}
	    		}
	    	}
	    }
	    return null;
	}

	/**
	 * Retourne le document XML.<br>
	 *
	 * @return Documet XML retourn�.
	 * @since 1.1.1.1
	 */
	public Document getXmldoc() {
		return xmldoc;
	}

	/**
	 * Associe le document XML en argument.<br>
	 *
	 * @param xmldoc Document XML.
	 * @since 1.1.1.1
	 */
	public void setXmldoc(Document xmldoc) {
		this.xmldoc = xmldoc;
	}
}

