package com.oreo.protocol.iena.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Common {

	/**
	 * Retourne la pile d'appel de l'exception en argument.<br>
	 *
	 * @param t L'exception.
	 * @return La pile d'appels.
	 */
	public static String getStackTrace(Throwable t)
	{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		t.printStackTrace(pw);
		pw.flush();
		sw.flush();
		return sw.toString();
	}
	
}
