package com.oreo.protocol.iena.appli;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@SuppressWarnings("all")
public class ConfigFile {
    private static Logger logger = Logger.getLogger(ConfigFile.class);
    private static ConfigFile gConfigFile;
    private Properties properties;

    public final static String IENA_XML_FILE = "ienaXmlFile";
    public final static String MAX_PARAM_BY_THREAD = "maxParamsByKey";
    public final static String MISTRAL_PARAM_PREFIX = "mistralParamPrefix";
    public final static String TIME_BEFORE_RETRY = "timeBeforeRetry";
    public final static String IENA_MAX_NUM_OF_PACKETS = "ienaMaxNumOfPaquets";
    public final static String IENA_PORT = "ienaPort";
    public final static String IENA_IP = "ienaIP";
    public final static String IENA_MAC = "ienaMAC";
    public final static String IENA_XML_HIREVISION = "ienaXml_HiRevision";
    public final static String IENA_XML_SIZE = "ienaXml_Size";
    public final static String IENA_XML_SYSTEMNAME = "ienaXml_SystemName";
    public final static String IENA_XML_ACRANAME = "ienaXml_ACRAName";
    public final static String IENA_XML_MODULENAME = "ienaXml_ModuleName";
    public final static String IENA_XML_SERIALNUMBER = "ienaXml_SerialNumber";
    public final static String IENA_XML_SLOT = "ienaXml_Slot";
    public final static String IENA_XML_HCQUALIFIER = "ienaXml_HCQualifier";
    public final static String IENA_XML_PORTNAME = "ienaXml_PortName";
    public final static String IENA_XML_PCMVALID = "ienaXml_PCMValid";
    // public final static String IENA_XML_PORTVALUE = "ienaXml_PortValue"; voir IENA_PORT
    public final static String IENA_XML_IPNAME = "ienaXml_IpName";
    // public final static String IENA_XML_IPVALUE = "ienaXml_IpValue"; voir SCC_CLIENT_IP
    public final static String IENA_XML_TIMETOLIVENAME = "ienaXml_TimeToLiveName";
    public final static String IENA_XML_TIMETOLIVEVALUE = "ienaXml_TimeToLiveValue";
    public final static String IENA_XML_ISMODE = "ienaXml_ISMode";
    public final static String IENA_XML_PARAMETERNAME = "ienaXml_ParameterName";
    public final static String IENA_XML_SOURCEBITS = "ienaXml_SourceBits";
    public final static String IENA_XML_ACQUISITIONCYCLE = "ienaXml_AcquisitionCycle";
    public final static String IENA_XML_MAJORFRAMESPARACQCYCL = "ienaXml_MajorFramesPerAcquisitionCycle";
    public final static String IENA_XML_PROTOCOL = "ienaXml_Protocol";
    public final static String IENA_XML_SAMPLESNAME = "ienaXml_SamplesName";
    public final static String IENA_XML_NUMBEROFBITSNAME = "ienaXml_NumberOfBitsName";
    public final static String IENA_XML_MSBNAME = "ienaXml_MSBName";
    public final static String IENA_XML_MASKNAME = "ienaXml_MaskName";
    public final static String IENA_XML_TYPEINFONAME = "ienaXml_TypeInfoName";
    public final static String IENA_XML_UNITNAME = "ienaXml_UnitName";
    public final static String IENA_XML_UDP_LAYOUT_REVISION = "ienaXml_UDP_LayoutRevision";
    public final static String IENA_XML_AGGREGATECUTOFF = "ienaXml_AggregateCutOff";
    public final static String IENA_XML_PACKETNAME = "ienaXml_PacketName";
    public final static String IENA_XML_DELAYUSED = "ienaXml_DelayUsed";
    public final static String IENA_XML_IDUSED = "ienaXml_IDUsed";
    public final static String IENA_XML_DATAORDER = "ienaXml_DataOrder";
    public final static String IENA_XML_STATUSMSB = "ienaXml_StatusMSB";
    public final static String IENA_XML_ENDOFPACKET = "ienaXml_EndOfPacket";
    public final static String IENA_XML_ACRA_LAYOUT_REVISION = "ienaXml_ACRA_LayoutRevision";

    /**
     * Charge le fichier de configuration.<br>
     * Le fichier est localis� depuis la racine de l'application.
     *
     * @param fileName Nom complet du fichier de configuration.
     * @since 1.1.1.1
     */
    private ConfigFile(InputStream stream) {
        try {
            properties = new Properties();
            properties.load(stream);
        } catch (IOException e) {
            logger.fatal("Fichier de config non lisible, fin de l'application : " + e.toString());
            System.exit(1);
        }
    }

    /**
     * Cr�e le singleton du fichier de configuration.<br>
     *
     * @param fileName Nom complet du fichier de configuration.
     * @return L'instance unique de la configuration.
     * @since 1.1.1.1
     */
    public static ConfigFile singleton(InputStream inputStream) {
        if (gConfigFile == null) {
            gConfigFile = new ConfigFile(inputStream);
        }
        return gConfigFile;
    }

    /**
     * Retourne la valeur de la cl� sp�cifi�e.<br>
     *
     * @param key La cl�.
     * @return La valeur de la cl�.
     * @since 1.1.1.1
     */
    public static String get(String key) {
        if (gConfigFile == null) {
            return "";
        }
        return gConfigFile.getProperty(key);
    }

    /**
     * Retourne la valeur associ�e � la cl� sp�cifi�e.
     *
     * @param key Cl� de la propri�t�.
     * @return La valeur de la cl�.
     * @see java.util.Properties#getProperty(java.lang.String)
     */
    public String getProperty(String key) {
        return properties.getProperty(key);
    }
}
