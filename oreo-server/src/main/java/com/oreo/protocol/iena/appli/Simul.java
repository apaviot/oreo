package com.oreo.protocol.iena.appli;

import com.oreo.entity.Measure;
import com.oreo.protocol.ProtocolConfiguration;
import com.oreo.protocol.SampleSupplier;
import com.oreo.protocol.iena.xml.XmlIena;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Timer;

public class Simul {
    private final static Logger logger = Logger.getLogger(Simul.class);
    private final static int paramByTimer = 350;
    private final static int MILLIS_PER_SECOND = 1000;


    /**
     * Crée un timer qui envoi les trames IENA périodiquement
     *
     * @param data le fournisseur d'échantillon
     * @param protocol la configuration du protocol
     * @param computeTime boolean indiquant si la date des échantillon doit être recalculée à partir de la date courrante
     * @return le timer
     */
    public static Timer sendPacket(SampleSupplier data, ProtocolConfiguration protocol, boolean computeTime) {
        ExperimentData experimentData = new ExperimentData(protocol.getKey(), data, computeTime);

        Timer t = new Timer();
        t.scheduleAtFixedRate(new BridgeMod(
                experimentData,
                protocol.getIp(),
                protocol.getPort(),
                protocol.getFrequency()
        ), 0, MILLIS_PER_SECOND/protocol.getFrequency());

        return t;
    }

    /**
     * Génère le fichier xml cd configuration.
     *
     * @param measures les mesures à envoyer
     * @param protocol la configuration du protocol
     * @return le byteArrayOutputStream du xml
     */
    public static ByteArrayOutputStream generateXML(List<Measure> measures, ProtocolConfiguration protocol) {
        int currParam = (measures.size() - getParamsByTimer()) > 0 ?
                getParamsByTimer() : measures.size();

        XmlIena xmlIena = new XmlIena(protocol.getModuleName(), protocol.getPort(), protocol.getMac());
        xmlIena.ajoutEntete(protocol.getIp(), protocol.getPort(), protocol.getFrequency());

        xmlIena.addPaquet(protocol.getKey(), currParam * 2, currParam);
        xmlIena.addOrrucencePacket(protocol.getKey());

        int offset = 0;
        for (Measure measure : measures) {
            xmlIena.addParametre(
                    measure.getName(),
                    XmlIena.T_SAMPLES,
                    XmlIena.T_NB_OF_BITS,
                    XmlIena.T_MSB,
                    XmlIena.T_MASK,
                    XmlIena.V_DT_DOUBL,
                    measure.getUnit()
            );
            xmlIena.addSinkParameter(measure.getName(), 2 * offset, protocol.getKey(), true);
            offset++;
        }

        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            xmlIena.serializeDocument(os);
            return os;
        } catch (IOException e) {
            logger.error("Erreur dans la creation du fichier IENA ");
            System.exit(1);
            return null;
        }
    }

    private static int getParamsByTimer() {
        try {
            return Integer.parseInt(ConfigFile.get(ConfigFile.MAX_PARAM_BY_THREAD));
        } catch (Exception e) {
            logger.error("Nombre de parametres maxi par cle incorrect, par defaut : " + paramByTimer);
            return paramByTimer;
        }
    }
}
