package com.oreo.protocol.iena;

import com.oreo.entity.Measure;
import com.oreo.protocol.AlarmSupplier;
import com.oreo.protocol.ProtocolConfiguration;
import com.oreo.protocol.ProtocolRunner;
import com.oreo.protocol.SampleSupplier;
import com.oreo.protocol.iena.appli.ConfigFile;
import com.oreo.protocol.iena.appli.Simul;
import org.springframework.util.FileCopyUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Timer;

/**
 * Implémentation de ProtocolRunner pour faire fonctionner IENA
 */
public class IENARunner implements ProtocolRunner {
    private static final String IENA_CONF = "iena.conf";

    private static InputStream getConfig() {
        return IENARunner.class.getClassLoader().getResourceAsStream(IENA_CONF);
    }

    private Timer timer;

    /**
     * Génère le xml de configuration et le retourne dans la réponse HTTP
     * <p>
     * Si la généraction du xml échoue la réponse renvoit un code INTERNAL ERROR
     *
     * @param response      la réponse http
     * @param configuration la configuration du protocol
     * @param measures      la liste des mesures à envoyer
     */
    @Override
    public void exportFile(HttpServletResponse response, ProtocolConfiguration configuration, List<Measure> measures) {
        try {
            ConfigFile.singleton(getConfig());
            ByteArrayOutputStream os = Simul.generateXML(measures, configuration);
            response.setContentType("application/xml");
            response.addHeader("Content-Disposition", "attachment; filename=iena.xml");
            response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
            FileCopyUtils.copy(os.toByteArray(), response.getOutputStream());
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Démarre l'envoi de données avec IENA.
     * <p>
     * Un timer est démarré. Celui-ci envoie périodiquement les trames IENA.
     *
     * @param sampleSupplier fournisseur d'échantillons
     * @param alarmSupplier  fournisseur d'alarmes. Inutile pour IENA
     * @param configuration  la configuration du protocol IENA
     * @param computeTime    true pour recalculer la date des échantillons à partir de la date courrante, false sinon
     */
    @Override
    public void sendData(SampleSupplier sampleSupplier, AlarmSupplier alarmSupplier, ProtocolConfiguration configuration, boolean computeTime) {
        ConfigFile.singleton(getConfig());
        timer = Simul.sendPacket(sampleSupplier, configuration, computeTime);
    }

    /**
     * Interrompt l'envoi en cours.
     * Si aucun envoi n'est en cours, rien ne se passe.
     */
    @Override
    public void stop() {
        if (timer != null) {
            timer.cancel();
        }
    }
}
