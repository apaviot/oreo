package com.oreo.protocol.iena.xml;

import com.oreo.protocol.iena.appli.ConfigFile;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;

import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 * Document XML Iena.</br>
 */
@SuppressWarnings("all")
public class XmlIena extends XmlDocument {
	// Nodes du fichier XML-IENA et atttibuts
	public static final String N_ACRAPCM = "ACRAPCM";
	public static final String N_FILE_INFORMATION = "FILE_INFORMATION";
	public static final String A_HI_REVISION = "HiRevision";
	public static final String N_HARDWARE_IDENTIFICATION = "HARDWARE_IDENTIFICATION";
	public static final String N_KAM_500 = "KAM-500";
	public static final String A_KAM_500_SIZE = "Size";
	public static final String A_KAM_500_SYSTEM_NAME = "SystemName";
	public static final String N_MODULE = "Module";
	public static final String A_MODULE_ACRA_NAME = "ACRAName";
	public static final String A_MODULE_NAME = "ModuleName";
	public static final String A_MODULE_SERIAL_NUMBER = "SerialNumber";
	public static final String A_MODULE_SLOT = "Slot";
	public static final String N_HARDWARE_CONFIGURATION = "HARDWARE_CONFIGURATION";
	public static final String N_HCMODULE = "HCMODULE";
	public static final String A_HCMODULE_HCQUALIFIER = "HCQualifier";
	public static final String A_HCMODULE_NAME = "ModuleName";
	public static final String N_ATTRIBUTE = "ATTRIBUTE";
	public static final String A_ATTRIBUTE_NAME = "Name";
	public static final String A_ATTRIBUTE_PCMVALID = "PCMValid";
	public static final String A_ATTRIBUTE_VALUE = "Value";
	public static final String N_INSTRUMENT_SETUP = "INSTRUMENT_SETUP";
	public static final String N_PARAMETER = "PARAMETER";
	public static final String A_PARAMETER_ISMODE = "ISMode";
	public static final String A_PARAMETER_MODULE_NAME = "ModuleName";
	public static final String A_PARAMETER_PARAMETER_NAME = "ParameterName";
	public static final String A_PARAMETER_SOURCE_BITS = "SourceBits";
	public static final String N_PROTOCOLS = "PROTOCOLS";
	public static final String N_PROTOCOL = "PROTOCOL";
	public static final String N_SYSTEMWIDE_CONFIGURATION = "SYSTEMWIDE_CONFIGURATION";
	public static final String A_SYS_WIDE_CONF_ACQ_CYC = "AcquisitionCycle";
	public static final String A_SYS_WIDE_CONF_MAJ_FRAM_PER_ACQ = "MajorFramesPerAcquisitionCycle";
	public static final String N_PROTOCOL_DEFINITION = "PROTOCOL_DEFINITION";
	public static final String A_PROTOCOL_DEF_MODULE_NAME = "ModuleName";
	public static final String A_PROTOCOL_DEF_PROTOCOL = "Protocol";
	public static final String N_FRAME_PLACEMENT = "FRAME_PLACEMENT";
	public static final String N_TRANSMISSION_PARAMETER = "TRANSMISSION_PARAMETER";
	public static final String A_PARAMETER_LABEL = "ParameterLabel";
	// N_ATTRIBUTE, A_ATTRIBUTE_NAME, A_ATTRIBUTE_VALUE deje definis.
	public static final String N_ETHERNET_UDP = "ETHERNET_UDP";
	public static final String A_LAYOUT_REV = "LayoutRevision";
	public static final String A_ETH_UDP_NAME = "Name";
	public static final String N_PACKET_DEFINITION = "PacketDefinition";
	public static final String N_IENA = "IENA";
	public static final String N_IENA_STD_PACKETS = "IENAStandardPackets";
	public static final String N_AGGREGATE_CUT_OFF = "AggregateCutOff";
	public static final String N_IENA_STD_PACKET = "IENAStandardPacket";
	public static final String A_IENA_STD_PACKET_NAME = "PacketName";
	public static final String N_DELAY_USED = "DelayUsed";
	public static final String N_ID_USED = "IDUsed";
	public static final String N_DATA_SIZE_IN_WORDS = "DataSizeInWords";
	public static final String N_PARAMETERS_SAMPLED = "ParametersSampled";
	public static final String N_PACKET_SETUP = "PacketSetup";
	public static final String N_KEY = "Key";
	public static final String N_DATA_ORDER = "DataOrder";
	public static final String N_STATUS_MSB = "StatusMSB";
	public static final String N_UDP_ROUTING = "UDPRouting";
	public static final String N_IP_ADDRESS = "IPAddress";
	public static final String N_PORT = "Port";
	public static final String N_MAC_ADDRESS = "MACAddress";
	public static final String N_IENA_SETUP = "IENA_Setup";
	public static final String N_END_OF_PACKET = "EndOfPacket";
	public static final String N_ACRA_PACKET = "ACRAPacket";
	// A_LAYOUT_REV deje defini
	public static final String N_OCCURENCES = "Occurences";
	public static final String N_PACKET = "Packet";
	public static final String N_NAME = "Name";
	public static final String N_INSTANCES = "Instances";
	public static final String N_SINK = "Sink";
	// A_PARAMETER_LABEL deje defini
	public static final String N_SINK_PARAMETER = "SinkParameter";
	public static final String N_SAMPLE_AT = "SampleAt";
	public static final String N_ENABLED = "Enabled";
	public static final String N_OFFSETS = "Offsets";
	public static final String N_OFFSET = "Offset";
	
	// Definition des types de donnees Iena
	public static final String V_DT_FLOT = "FLOT";
	public static final String V_DT_DOUBL = "DOUBL";
	public static final String V_DT_LONG = "LONG";
	public static final String V_DT_ULONG = "ULONG";
	public static final String V_DT_FIXE = "FIXE";
	public static final String V_DT_UFIXE = "UFIXE";
	public static final String V_DT_COUR = "COUR";
	public static final String V_DT_UCOUR = "UCOUR";
	public static final String V_DT_CAR = "CAR";
	public static final String V_DT_UCAR = "UCAR";

	// Transmission Parameter
	public static final String T_SAMPLES = "1";
	public static final String T_NB_OF_BITS = "64";
	public static final String T_MSB = "First";
	public static final String T_MASK = "0xFFFFFFFFFFFFFFFF";

	/** Logger de la classe */
    private static Logger logger = Logger.getLogger(XmlIena.class);
	
	/** Node racine du document */
	private Element rootEl;
	
	/** Noeud "FRAME PLACEMENT" : chaque fils defini un parametre */
	private Element framePlacementEL;
	
	/** Noeud pere des paquets Iena e declarer */
	private Element ienaPacketsEl;
	
	/** Noeud pere des occurences de paquets Iena e declarer */
	private Element packetOccurencesEl;
	
	/** Noeud Sink : permet de placer chaque parametre dans son paquet par l'ajout
	 * d'un fils 'SinkParameter' */
	private Element sinkEl;
	
	/** numero/nombre de paquets ajoute dans le fichier Iena */
	int numOfPackets;

	private String moduleName;
	private int port;
	private String mac;

	/**
	 * Creation d'un nouveau document.<br>
	 * details.
	 *
	 */
	public XmlIena(String moduleName, int port, String mac) {
		super();
		rootEl = appendElement(N_ACRAPCM);
		numOfPackets = 0;
		this.moduleName = moduleName;
		this.port = port;
		this.mac = mac;
	}
	
	/**
	 * Ajoute les informations 'fixes' au document.<br>
	 *
	 */
	public void ajoutEntete(String mistralServerIp, int mistralServerPort, int acquisitionCycle) {
		Element el;
		
		// FILE_INFORMATION
		el = this.appendElement(rootEl, N_FILE_INFORMATION);
		el.setAttribute(A_HI_REVISION, ConfigFile.get(ConfigFile.IENA_XML_HIREVISION));
		
		// HARDWARE_IDENTIFICATION, KAM-500
		el = this.appendElement(rootEl, N_HARDWARE_IDENTIFICATION);
		el = this.appendElement(el, N_KAM_500);
		el.setAttribute(A_KAM_500_SIZE, ConfigFile.get(ConfigFile.IENA_XML_SIZE));
		el.setAttribute(A_KAM_500_SYSTEM_NAME, ConfigFile.get(ConfigFile.IENA_XML_SYSTEMNAME));
		
		// MODULE
		el = this.appendElement(el, N_MODULE);
		el.setAttribute(A_MODULE_ACRA_NAME, ConfigFile.get(ConfigFile.IENA_XML_ACRANAME));
		el.setAttribute(A_MODULE_NAME, moduleName);
		el.setAttribute(A_MODULE_SERIAL_NUMBER, ConfigFile.get(ConfigFile.IENA_XML_SERIALNUMBER));
		el.setAttribute(A_MODULE_SLOT, ConfigFile.get(ConfigFile.IENA_XML_SLOT));
		
		// HARDWARE_CONFIGURATION, HCMODULE(s)
		el = this.appendElement(rootEl, N_HARDWARE_CONFIGURATION);
		this.addHcModule(el, ConfigFile.get(ConfigFile.IENA_XML_HCQUALIFIER),
				moduleName,
				ConfigFile.get(ConfigFile.IENA_XML_PORTNAME), 
				ConfigFile.get(ConfigFile.IENA_XML_PCMVALID), 
				Integer.toString(mistralServerPort));
		this.addHcModule(el, ConfigFile.get(ConfigFile.IENA_XML_HCQUALIFIER),
				moduleName,
				ConfigFile.get(ConfigFile.IENA_XML_IPNAME),
				ConfigFile.get(ConfigFile.IENA_XML_PCMVALID), 
				mistralServerIp);
		this.addHcModule(el, ConfigFile.get(ConfigFile.IENA_XML_HCQUALIFIER),
				moduleName,
				ConfigFile.get(ConfigFile.IENA_XML_TIMETOLIVENAME), 
				ConfigFile.get(ConfigFile.IENA_XML_PCMVALID), 
				ConfigFile.get(ConfigFile.IENA_XML_TIMETOLIVEVALUE));
		
		// INSTRUMENT_SETUP
		el=this.appendElement(rootEl, N_INSTRUMENT_SETUP);
		el=this.appendElement(el, N_PARAMETER);
		el.setAttribute(A_PARAMETER_ISMODE, ConfigFile.get(ConfigFile.IENA_XML_ISMODE));
		el.setAttribute(A_PARAMETER_MODULE_NAME, moduleName);
		el.setAttribute(A_PARAMETER_PARAMETER_NAME, 
				ConfigFile.get(ConfigFile.IENA_XML_PARAMETERNAME));
		el.setAttribute(A_PARAMETER_SOURCE_BITS, ConfigFile.get(ConfigFile.IENA_XML_SOURCEBITS));
		
		// PROTOCOLS, PROTOCOL, SYSTEMWIDE_CONFIGURATION, PROTOCOL_DEFINITION
		el=this.appendElement(rootEl, N_PROTOCOLS);
		el=this.appendElement(el, N_PROTOCOL);
		Element ef = this.appendElement(el, N_SYSTEMWIDE_CONFIGURATION);
		ef.setAttribute(A_SYS_WIDE_CONF_ACQ_CYC, 
				Integer.toString(acquisitionCycle*1000));
		ef.setAttribute(A_SYS_WIDE_CONF_MAJ_FRAM_PER_ACQ, 
				ConfigFile.get(ConfigFile.IENA_XML_MAJORFRAMESPARACQCYCL));
		ef = this.appendElement(el, N_PROTOCOL_DEFINITION);
		ef.setAttribute(A_PROTOCOL_DEF_MODULE_NAME, moduleName);
		ef.setAttribute(A_PROTOCOL_DEF_PROTOCOL, 
				ConfigFile.get(ConfigFile.IENA_XML_PROTOCOL));
		
		// FRAME PLACEMENT : chacun de ses fils sera ajoute ulterieurement par addParametre.
		framePlacementEL = this.appendElement(el, N_FRAME_PLACEMENT);
		
		// ETHERNET_UDP, PACKET_DEFINITION, IENA
		el = this.appendElement(el, N_ETHERNET_UDP);
		el.setAttribute(A_LAYOUT_REV, ConfigFile.get(ConfigFile.IENA_XML_UDP_LAYOUT_REVISION));
		el.setAttribute(A_ETH_UDP_NAME, moduleName);
		ef = this.appendElement(el, N_PACKET_DEFINITION);
		Element eff = this.appendElement(ef, N_IENA);
		
		// IENA_STD_PACKETS, AGGREGATE_CUT_OFF : chaque paquet sera ajoute en fils de
		// IENA_STD_PACKETS avec addPaquet()
		ienaPacketsEl = this.appendElement(eff, N_IENA_STD_PACKETS);
		this.appendElement(ienaPacketsEl, N_AGGREGATE_CUT_OFF, 
				ConfigFile.get(ConfigFile.IENA_XML_AGGREGATECUTOFF));
		eff = this.appendElement(eff, N_IENA_SETUP);
		this.appendElement(eff, N_END_OF_PACKET, "0x" + ConfigFile.get(ConfigFile.IENA_XML_ENDOFPACKET));
		
		// ACRAPacket, OCCURENCES, ses fils (Packet) seront ajoutes par
		el = this.appendElement(el, N_ACRA_PACKET);
		el.setAttribute(A_LAYOUT_REV, ConfigFile.get(ConfigFile.IENA_XML_ACRA_LAYOUT_REVISION));
		packetOccurencesEl = this.appendElement(el, N_OCCURENCES);
		
		// Sink : ses fils 'SinkParameter' seront ajoutes par addSinkParameter() pour placer
		// les parametres dans les paquets.
		sinkEl = this.appendElement(el, N_SINK);
				
		// el.setAttribute(A_, ConfigFile.get(ConfigFile.IENA_XML_));
	}
	
	/**
	 * Place un parametre dans son paquet.<br>
	 *
	 * @param paramLabel Le label du parametre e placer.
	 * @param offset Sa position dans le dernier paquet ajoute.
	 * @param key La cle dans laquelle se trouve le parametre
	 * @param enabled Statut du parametre.
	 */
	public void addSinkParameter(String paramLabel, int offset, int key, boolean enabled) {
		Element el = this.appendElement(sinkEl, N_SINK_PARAMETER);
		el.setAttribute(A_PARAMETER_LABEL, paramLabel);
		this.appendElement(el, N_SAMPLE_AT, "1");		// Toujours qu'un echantillon par parametre
		this.appendElement(el, N_PACKET, 
				ConfigFile.get( ConfigFile.IENA_XML_PACKETNAME) + 
				String.valueOf(key));		
		this.appendElement(el, N_ENABLED, (enabled==true)? "Yes":"No");
		el = this.appendElement(el, N_OFFSETS);
		this.appendElement(el, N_OFFSET, String.valueOf(offset));
	}
	
	/**
	 * Ajoute 'l'occurence' d'un paquet dans le document.<br>
	 *
	 *@param key La cle pour l'occurence du paquet
	 */
	public void addOrrucencePacket(int key){
		Element el = this.appendElement(packetOccurencesEl, N_PACKET);
		this.appendElement(el, N_NAME, 
				ConfigFile.get( ConfigFile.IENA_XML_PACKETNAME) +
				String.valueOf(key));
		this.appendElement(el, N_INSTANCES, "1");
	}
	
	/**
	 * Ajoute un paquet dans le document.<br>
	 *
	 * @param key La cle du paquet e ajouter
	 * @param dataSize La taille de donnees du paquet.
	 * @param numOfParams Le nombre de parametres du paquet.
	 */
	public void addPaquet(int key, int dataSize, int numOfParams) {
		Element el;
		// Verifie le depassement du nombre max de paquets
		if( numOfPackets+1 > Integer.parseInt(ConfigFile.get(ConfigFile.IENA_MAX_NUM_OF_PACKETS))){
			logger.fatal("Nombre de paquets max IENA depasse : " + String.valueOf(numOfPackets));
			return;
		}
		numOfPackets++;
		
		el = this.appendElement(ienaPacketsEl, N_IENA_STD_PACKET);
		el.setAttribute(A_IENA_STD_PACKET_NAME, 	
				ConfigFile.get( ConfigFile.IENA_XML_PACKETNAME) + String.valueOf(key));		
		this.appendElement(el, N_DELAY_USED, ConfigFile.get(ConfigFile.IENA_XML_DELAYUSED));
		this.appendElement(el, N_ID_USED, ConfigFile.get(ConfigFile.IENA_XML_IDUSED));
		this.appendElement(el, N_DATA_SIZE_IN_WORDS, String.valueOf(dataSize));
		this.appendElement(el, N_PARAMETERS_SAMPLED, String.valueOf(numOfParams));
		Element ef = this.appendElement(el, N_PACKET_SETUP);
		this.appendElement(ef, N_KEY, "0x" + Integer.toHexString(key));
		this.appendElement(ef, N_DATA_ORDER, ConfigFile.get(ConfigFile.IENA_XML_DATAORDER));
		this.appendElement(ef, N_STATUS_MSB, "0x" + ConfigFile.get(ConfigFile.IENA_XML_STATUSMSB));
		ef = this.appendElement(el, N_UDP_ROUTING);
		try {
			this.appendElement(ef, N_IP_ADDRESS, InetAddress.getLocalHost().getHostAddress().toString());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		this.appendElement(ef, N_PORT, port + "");
		this.appendElement(ef, N_MAC_ADDRESS, mac);
	}
	
	/**
	 * Ajoute un parametre au document.<br>
	 *
	 * @param label Le label du parametre.
	 * @param samples Son nombre d'echantillons.
	 * @param numOfBits Son nombre de bits.
	 * @param msb Son format LSB|MSB.
	 * @param mask Son masque.
	 * @param type Son type.
	 * @param unit Son unite.
	 */
	public void addParametre(
			String label,
			String samples,
			String numOfBits,
			String msb,
			String mask,
			String type,
			String unit
	){
		Element el = this.appendElement(framePlacementEL, N_TRANSMISSION_PARAMETER);
		el.setAttribute(A_PARAMETER_LABEL, label);
		
		Element ef;
		ef = this.appendElement(el, N_ATTRIBUTE);
		ef.setAttribute(A_ATTRIBUTE_NAME, ConfigFile.get(ConfigFile.IENA_XML_SAMPLESNAME));
		ef.setAttribute(A_ATTRIBUTE_VALUE, samples);
		
		ef = this.appendElement(el, N_ATTRIBUTE);
		ef.setAttribute(A_ATTRIBUTE_NAME, ConfigFile.get(ConfigFile.IENA_XML_NUMBEROFBITSNAME));
		ef.setAttribute(A_ATTRIBUTE_VALUE, numOfBits);
		
		ef = this.appendElement(el, N_ATTRIBUTE);
		ef.setAttribute(A_ATTRIBUTE_NAME, ConfigFile.get(ConfigFile.IENA_XML_MSBNAME));
		ef.setAttribute(A_ATTRIBUTE_VALUE, msb);
		
		ef = this.appendElement(el, N_ATTRIBUTE);
		ef.setAttribute(A_ATTRIBUTE_NAME, ConfigFile.get(ConfigFile.IENA_XML_MASKNAME));
		ef.setAttribute(A_ATTRIBUTE_VALUE, mask);
		
		ef = this.appendElement(el, N_ATTRIBUTE);
		ef.setAttribute(A_ATTRIBUTE_NAME, ConfigFile.get(ConfigFile.IENA_XML_TYPEINFONAME));
		ef.setAttribute(A_ATTRIBUTE_VALUE, type);
		
		ef = this.appendElement(el, N_ATTRIBUTE);
		ef.setAttribute(A_ATTRIBUTE_NAME, ConfigFile.get(ConfigFile.IENA_XML_UNITNAME));
		ef.setAttribute(A_ATTRIBUTE_VALUE, unit);
	}

	/**
	 * Ajoute un module de configutation hardware (HC MODULE).<br>
	 *
	 * @param pere Le pere du module e ajouter.
	 * @param hcQual Le 'HCM Qualifier'.
	 * @param ModName Le nom de module.
	 * @param attName Le nom d'attribut.
	 * @param attPCM Le PCM.
	 * @param attVal La valeur d'attribut.
	 */
	private void addHcModule(
			Element pere,
			String hcQual,
			String ModName,
			String attName,
			String attPCM,
			String attVal
	){
		Element el = this.appendElement(pere, N_HCMODULE);
		el.setAttribute(A_HCMODULE_HCQUALIFIER, hcQual);
		el.setAttribute(A_HCMODULE_NAME, ModName);

		el = this.appendElement(el, N_ATTRIBUTE);
		el.setAttribute(A_ATTRIBUTE_NAME, attName);
		el.setAttribute(A_ATTRIBUTE_PCMVALID, attPCM);
		el.setAttribute(A_ATTRIBUTE_VALUE, attVal);
	}
}
