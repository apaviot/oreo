package com.oreo.protocol.iena.appli;

import com.oreo.protocol.iena.net.UDP.IenaPaquet;
import com.oreo.protocol.iena.net.UDP.UDPClient;
import org.apache.log4j.Logger;

import java.io.UncheckedIOException;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class BridgeMod extends TimerTask {

    private int ienaEndPattern = 0;

    private UDPClient udpClient = null;

    private ExperimentData data;

    private IenaPaquet ienaPaquet;

    private String mistralIpAdress;

    private int mistralPort;

    private static Logger logger = Logger.getLogger(BridgeMod.class);

    private boolean status = false;

    private int numIterBeforeRetry = 1000;

    private int numIter = 0;

    private final AtomicInteger sequenceNumber = new AtomicInteger();


    public BridgeMod(ExperimentData data, String ip, int port, int paquetPeriod) {
        this.data = data;
        mistralIpAdress = ip;
        mistralPort = port;
        int period = paquetPeriod;
        try {
            ienaEndPattern = Integer.parseInt(ConfigFile.get(ConfigFile.IENA_XML_ENDOFPACKET), 16);
        } catch (NumberFormatException e) {
            logger.fatal("Pattern de fin de message IENA invalide : " + ConfigFile.get(ConfigFile.IENA_XML_ENDOFPACKET));
            throw new IllegalArgumentException("Pattern de fin de message IENA invalide : " + ConfigFile.get(ConfigFile.IENA_XML_ENDOFPACKET));
        }
        try {
            int time = Integer.parseInt(ConfigFile.get(ConfigFile.TIME_BEFORE_RETRY));
            numIterBeforeRetry = (int) Math.ceil((double) time * 1000 / (double) period);
        } catch (Exception e) {
            logger.error("Valeur timeBeforeRetry du fichier de configuration invalide");
        }
        status = networkSetup();
    }

    @Override
    public void run() {
        if (status) {
            if (data.isFinish()) {
                this.cancel();
                return;
            }
            try {
                udpClient.send(ienaPaquet.getTrame(sequenceNumber.getAndIncrement(), data));
                ienaPaquet.clearPacket();
            } catch (Exception e1) {
                status = false;
                udpClient = null;
                logger.error("Impossible d'envoyer une trame IENA e Mistral");
                logger.error(e1.toString());
                e1.printStackTrace();
            }
        } else {
            if (numIter < numIterBeforeRetry) {
                numIter++;
            } else {
                numIter = 0;
                status = networkSetup();
            }
        }
    }

    private boolean networkSetup() {
        if (udpClient == null) {
            try {
                udpClient = UDPClient.create(mistralIpAdress, mistralPort);
            } catch (UncheckedIOException e) {
                logger.fatal("Serveur Mistral (Hete ou IP) inconnu : " + mistralIpAdress);
                udpClient = null;
                return false;
            }
        }

        ienaPaquet = new IenaPaquet(data.getKey(), ienaEndPattern);
        return true;
    }
}
