package com.oreo.protocol.iena.net.UDP;


import com.oreo.protocol.iena.appli.ExperimentData;

import java.nio.ByteBuffer;

/**
 * Classe pour générer contruire les trames IENA
 */
public class IenaPaquet {

    private static final int MAX_PACKET_SIZE = 1472; // taille max d'un packet UDP

    private static final int KEY_BYTE = 2; // Taille en octets du champs KEY dans la trame IENA

    private static final int SIZE_BYTE = 2; // Taille en octets du champs SIZE dans la trame IENA

    private static final int TIME_BYTE = 6; // Taille en octets du champs TIME dans la trame IENA

    private static final int STATUS_BYTE = 2; // Taille en octets du champs STATUS dans la trame IENA

    private static final int SEQ_BYTE = 2; // Taille en octets du champs SEQ dans la trame IENA

    private static final int END_BYTE = 2; // Taille en octets du champs END dans la trame IENA

    // Taille en octets réservés dans la trame IENA par les champs obligatoires
    private static final int CAPSULE_SIZE = KEY_BYTE + SIZE_BYTE + TIME_BYTE + STATUS_BYTE + SEQ_BYTE + END_BYTE;

    private int paquetKey;

    private int endPattern;

    private final ByteBuffer bbData;

    /**
     * Constructeur
     *
     * @param paquetKey  clé des paquets IENA
     * @param endPattern indicateur fin de trame
     */
    public IenaPaquet(int paquetKey, int endPattern) {
        this.paquetKey = paquetKey;
        this.endPattern = endPattern;
        bbData = ByteBuffer.allocate(MAX_PACKET_SIZE);
    }

    public void clearPacket() {
        bbData.clear();
    }

    /**
     * Crée le bytebuffer.
     * Le bytebuffer est retourné en mode écriture.
     *
     * @param sequenceNumber le numéro de la trame dans l'envoi courrant
     * @param time           la date de l'échantillon le plus récent dans la trame
     * @return le bytebuffer créé
     */
    private ByteBuffer encapsule(int sequenceNumber, long time) {
        bbData.flip();
        int dataSize = bbData.limit();
        ByteBuffer bbTrame = ByteBuffer.allocate(CAPSULE_SIZE + dataSize);
        bbTrame.putShort((short) paquetKey);
        bbTrame.putShort((short) ((CAPSULE_SIZE + dataSize) / Short.BYTES));
        bbTrame.put((byte) (time >> 40));
        bbTrame.put((byte) (time >> 32));
        bbTrame.put((byte) (time >> 24));
        bbTrame.put((byte) (time >> 16));
        bbTrame.put((byte) (time >> 8));
        bbTrame.put((byte) (time));
        bbTrame.putShort((short) 0);
        bbTrame.putShort((short) sequenceNumber);
        bbTrame.put(bbData);
        bbTrame.putShort((short) endPattern);

        return bbTrame;
    }

    /**
     * Crée un ByteBuffer qui contient la trame IENA.
     * <p>
     * Le bytebuffer retourné est en mode écriture. Avant de l'écrire dans une socket il faut le flip.
     *
     * @param sequenceNumber numéro de la trame dans l'envoi courant
     * @param data           les données à inclure dans la trame
     * @return le ByteBuffer qui contient la trame IENA
     */
    public ByteBuffer getTrame(int sequenceNumber, ExperimentData data) {
        addData(data);
        return encapsule(sequenceNumber, data.getTime());
    }

    /**
     * Ajoute la partie DATA dans le bytebuffer
     *
     * @param experimentData la données à insérer
     */
    private void addData(ExperimentData experimentData) {
        experimentData.getMessage(bbData);
    }
}
