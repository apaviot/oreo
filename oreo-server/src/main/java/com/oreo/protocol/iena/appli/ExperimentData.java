package com.oreo.protocol.iena.appli;

import com.oreo.entity.Sample;
import com.oreo.protocol.SampleSupplier;

import java.nio.ByteBuffer;
import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.Objects;

/**
 * Classe en charge d'écrire les la partie DATA des trames IENA dans un byte buffer
 */
public class ExperimentData {
    /**
     * Clé de l'envoi IENA
     */
    private int key;

    /**
     * Nombre de microsecondes écoulées entre le premier janvier de l'année courrante et la date du premier échantillon de la trame
     */
    private long time;

    /**
     * True si le temps de premier échantillon doit être recalculé à partir de la date courrante, false sinon
     */
    private final boolean computeTime;

    /**
     * Fournisseur d'échantillons
     */
    private final SampleSupplier supplier;
    private List<Sample> values;

    public ExperimentData(int key, SampleSupplier data, boolean computeTime) {
        this.key = key;
        this.supplier = Objects.requireNonNull(data);
        this.time = localDateTimeToMillis(LocalDateTime.now());
        this.computeTime = computeTime;
    }

    public int getKey() {
        return key;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public boolean isFinish() {
        return !supplier.hasNext();
    }

    private long getPacketTime() {
        if (computeTime) {
            return localDateTimeToMillis(LocalDateTime.now()) * 1000;
        }
        return values.stream().mapToLong(s -> localDateTimeToMillis(s.getTime())).min().getAsLong() *1000;
    }

    /**
     * Retourne le nombre de microsecondes écoulées entre le premier janvier de l'année courrante et le jour de la date donnée en paramètre.
     * @param time
     * @return le nombre de microsecondes écoulées entre le premier janvier de l'année courrante et le jour de la date donnée en paramètre
     */
    private long localDateTimeToMillis(LocalDateTime time) {
        long result = 0;
        result += time.getLong(ChronoField.MILLI_OF_DAY);
        result += (time.getLong(ChronoField.DAY_OF_YEAR) - 1) * 1000 * 60 * 60 * 24;
        return result;
    }

    /**
     * Ecrit les données dans le byte buffer et prépare l'écriture de la prochaine trame
     * @param buffer
     */
    public void getMessage(ByteBuffer buffer) {
        if (isFinish()) {
            throw new IllegalStateException("The experiment have already been used");
        }
        values = supplier.getValues();
        this.time = getPacketTime();
        values.forEach(s -> buffer.putDouble(s.getValue()));
    }
}
