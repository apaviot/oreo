package com.oreo.protocol;

import com.oreo.entity.Measure;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Définit les méthodes que doit implémenter un protocol pour fonctionner avec l'application
 */
public interface ProtocolRunner {

    /**
     * Exporte le fichier de configuration du protocol.
     * <p>
     * Le fichier est inclut dans la réponse http
     *
     * @param response      la réponse http
     * @param configuration la configuration du protocol
     * @param measures      la liste des mesures à envoyer avec la configuration
     */
    void exportFile(HttpServletResponse response, ProtocolConfiguration configuration, List<Measure> measures);

    /**
     * Démarre l'envoi des données avec le protocole.
     *
     * @param sampleSupplier le fournisseur d'échantillons
     * @param alarmSupplier  le fournisseur d'alarmes
     * @param configuration  la configuration du protocol
     * @param computeTime    true si la date des échantillons doit être recalculée à partir de la date courrante, false sinon
     */
    void sendData(SampleSupplier sampleSupplier, AlarmSupplier alarmSupplier, ProtocolConfiguration configuration, boolean computeTime);

    /**
     * Interrompt l'envoi des données en cours
     */
    void stop();
}
