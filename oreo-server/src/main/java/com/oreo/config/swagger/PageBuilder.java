package com.oreo.config.swagger;

import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ModelPropertyBuilder;
import springfox.documentation.schema.ModelProperty;
import springfox.documentation.schema.ResolvedTypes;
import springfox.documentation.schema.TypeNameExtractor;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.ModelBuilderPlugin;
import springfox.documentation.spi.schema.contexts.ModelContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

import java.util.HashMap;

/**
 *  Permet de d'indiquer que le ressources retournées par les repository sont des pages.
 *  Utiles pour Swagger
 */
@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER + 1008)
public class PageBuilder implements ModelBuilderPlugin {
    @Autowired
    private TypeResolver resolver;

    @Autowired
    private TypeNameExtractor typeNameExtractor;

    private HashMap<String, ModelProperty> propertyAddinMap = new HashMap<>();

    @Override
    public void apply(ModelContext modelContext) {
        Class<?> modelClass = forClass(modelContext);
        if (modelClass == Resources.class) {
            ModelPropertyBuilder builder = new ModelPropertyBuilder();
            ModelProperty property = builder
                    .name("page")
                    .type(resolver.resolve(Page.class))
                    .required(true)
                    .isHidden(false)
                    .position(0)
                    .build();

            property.updateModelRef(ResolvedTypes.modelRefFactory(modelContext, typeNameExtractor));
            propertyAddinMap.put("page", property);

            modelContext
                    .getBuilder()
                    .properties(propertyAddinMap)
                    .build();
        }
    }

    @Override
    public boolean supports(DocumentationType documentationType) {
        return DocumentationType.SWAGGER_2.equals(documentationType);
    }

    private Class<?> forClass(ModelContext context) {
        return resolver.resolve(context.getType()).getErasedType();
    }
}
