package com.oreo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Classe de configuration.
 *
 * Permet de faire une redirection pour que Spring expose la partie front-end
 */
@Configuration
public class WebMVCConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/protocol").setViewName("forward:/index.html");
        registry.addViewController("/sql").setViewName("forward:/index.html");
        registry.addViewController("/experiment/*").setViewName("forward:/index.html");
        registry.addViewController("/config").setViewName("forward:/index.html");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedMethods("*").allowedHeaders("*", "Content-Disposition");
    }
}
