package com.oreo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.data.rest.webmvc.mapping.LinkCollector;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;

import java.util.List;

/**
 * Classe de configuration des repository
 */
@Configuration
public class RepositoryRestConfig extends RepositoryRestMvcConfiguration {

    /**
     * Enlève les attributs "link" des JSON retournés par les repository
     * @return
     */
    @Bean
    protected LinkCollector linkCollector() {
        return new LinkCollector(persistentEntities(), selfLinkProvider(), associationLinks()) {
            @Override
            public Links getLinksForNested(Object object, List<Link> existing) {
                return new Links();
            }

            @Override
            public Links getLinksFor(Object object) {
                return new Links();
            }

            @Override
            public Links getLinksFor(Object object, List<Link> existingLinks) {
                return new Links();
            }
        };
    }
}