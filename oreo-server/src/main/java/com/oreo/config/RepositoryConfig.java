package com.oreo.config;

import com.oreo.entity.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

/**
 * Classe de configuration des repository
 */
@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.setBasePath("/api");

        // Règle les problèmes de CORS origins
        config.getCorsRegistry()
                .addMapping("/**")
                .allowedOrigins("*")
                .allowedHeaders("*")
                .allowedMethods("*");

        // Indique que les identifiants des entités doivent apparaître dans les JSON
        config.exposeIdsFor(Bench.class);
        config.exposeIdsFor(Campaign.class);
        config.exposeIdsFor(Experiment.class);
        config.exposeIdsFor(Measure.class);
        config.exposeIdsFor(Modification.class);
        config.exposeIdsFor(PartialModification.class);
        config.exposeIdsFor(Alarm.class);
        config.exposeIdsFor(Protocol.class);
    }
}
