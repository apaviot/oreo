package com.oreo.helper.swagger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.oreo.entity.Alarm;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Classe utilitaire pour indiquer à Swagger que la ressource renvoyée est une liste d'alarmes
 */
public class ResourcesOfAlarm {
    private class EmbeddedCollectionOfAlarm {
        @JsonProperty
        List<Alarm> alarms;
    }

    @JsonProperty
    EmbeddedCollectionOfAlarm _embedded;

    @JsonProperty
    Page<Alarm> page;
}
