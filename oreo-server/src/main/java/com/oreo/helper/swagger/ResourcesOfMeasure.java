package com.oreo.helper.swagger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.oreo.entity.Measure;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Classe utilitaire pour indiquer à Swagger que la ressource renvoyée est une liste de mesures
 */
public class ResourcesOfMeasure {
    private class EmbeddedCollectionOfMeasure {
        @JsonProperty
        List<Measure> measures;
    }

    @JsonProperty
    EmbeddedCollectionOfMeasure _embedded;

    @JsonProperty
    Page<Measure> page;
}
