package com.oreo.helper.swagger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.oreo.entity.Sample;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Classe utilitaire pour indiquer à Swagger que la ressource renvoyée est une liste d'alarmes
 */
public class ResourcesOfSample {
    private class EmbeddedCollectionOfSample {
        @JsonProperty
        List<Sample> samples;
    }

    @JsonProperty
    EmbeddedCollectionOfSample _embedded;

    @JsonProperty
    Page<Sample> page;
}
