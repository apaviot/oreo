package com.oreo.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.oreo.protocol.ProtocolConfiguration;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;

/**
 * Convertit un ProtocolConfiguration en String et inversement
 */
@Converter(autoApply = true)
public class JsonAttributeConverter implements AttributeConverter<ProtocolConfiguration, String> {

    @Override
    public String convertToDatabaseColumn(ProtocolConfiguration conf) {
        try {
            return new ObjectMapper().writerFor(ProtocolConfiguration.class).writeValueAsString(conf);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public ProtocolConfiguration convertToEntityAttribute(String string) {
        try {
            return new ObjectMapper().readerFor(ProtocolConfiguration.class).readValue(string);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}