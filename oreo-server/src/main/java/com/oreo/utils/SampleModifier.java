package com.oreo.utils;

import com.oreo.entity.Modification;
import com.oreo.entity.PartialModification;
import com.oreo.entity.Sample;

import java.util.HashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Classe pour modifier des échantillons
 */
public class SampleModifier {

    public static final SampleModifier IDENTITY = new SampleModifier() {
        public Sample apply(Sample sample, long measureId) {
            return sample;
        }

        public List<Sample> apply(List<Sample> samples, long measureId) {
            return samples;
        }
    };

    // Pour chaque mesure est associé une fonction qui modifie les échantillons de cette mesure
    private final HashMap<Long, Function<Sample, Sample>> functions;

    /**
     * Constructeur
     *
     * @param modification
     */
    public SampleModifier(Modification modification) {
        functions = new HashMap<>();
        modification.getPartials().stream().collect(Collectors.groupingBy(PartialModification::getMeasureId))
                .forEach((id, partials) -> {
                    Function<Sample, Sample> fun = partials.stream().map(this::partialToFunction).reduce(Function.identity(), (acc, u) -> u.compose(acc));
                    functions.put(id, fun);
                });
    }

    private SampleModifier() {
        functions = null;
    }

    /**
     * Applique une modification à un échantillon.
     * <p>
     * Si la modification ne concerne pas la mesure spécifiée, l'échantillon est renvoyé non modifié.
     *
     * @param sample    l'échantillon à modifier
     * @param measureId la mesure à laquelle appartient l'échantillon
     * @return l'échantillon modifié
     */
    public Sample apply(Sample sample, long measureId) {
        if (sample.getValue() != Double.NaN) {
            return functions.getOrDefault(measureId, Function.identity()).apply(sample);
        }
        return sample;
    }

    /**
     * Applique une modification à une liste d'échantillons.
     * <p>
     * Si la modification ne concerne pas la mesure spécifiée, les échantillons sont renvoyés non modifiés.
     *
     * @param samples   les échantillons à modifier
     * @param measureId la mesure à laquelle appartiennent les échantillons
     * @return les échantillons modifiés
     */
    public List<Sample> apply(List<Sample> samples, long measureId) {
        return samples.stream().map(s -> apply(s, measureId)).collect(Collectors.toList());
    }

    private Function<Sample, Sample> partialToFunction(PartialModification partial) {
        return s -> {
            if (s.getTime().compareTo(partial.getStartedDate()) >= 0 && s.getTime().compareTo(partial.getEndedDate()) <= 0) {
                return partial.getOperation().getModificationFunction().apply(s, partial.getOperator());
            }
            return s;
        };
    }
}
