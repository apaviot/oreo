package com.oreo.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.oreo.entity.PartialModification;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;

public class PartialArrayDeserializer extends JsonDeserializer<List<PartialModification>> {

    private static final String PARTIALS = "partials";
    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public List<PartialModification> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException {

        ObjectNode objectNode = mapper.readTree(jsonParser);
        JsonNode nodePartials = objectNode.get(PARTIALS);

        if (null == nodePartials
                || !nodePartials.isArray()
                || !nodePartials.elements().hasNext())
            return null;

        ArrayList<PartialModification> partials = new ArrayList<>();
        nodePartials.forEach(p -> {
            try {
                partials.add(mapper.readerFor(PartialModification.class).readValue(p));
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        });
        return partials;
    }
}