package com.oreo.dataimport;

import com.oreo.dataimport.payload.ImportDataForm;
import com.oreo.dataimport.reader.ExperimentMetadata;
import com.oreo.dataimport.reader.Reader;
import com.oreo.entity.Bench;
import com.oreo.entity.Campaign;
import com.oreo.entity.Experiment;
import com.oreo.repository.AlarmRepository;
import com.oreo.repository.BenchRepository;
import com.oreo.repository.CampaignRepository;
import com.oreo.repository.ExperimentRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Classe qui orchestre l'import des fichiers CSV dans la base de données.
 */
public class InsertionDataManager {

    private static Path ROOT_PATH = Paths.get(Paths.get("").toAbsolutePath().toString() + "/tmp");

    private final ImportDataForm form;
    private final BenchRepository benchRepository;
    private final CampaignRepository campaignRepository;
    private final ExperimentRepository experimentRepository;
    private final AlarmRepository alarmRepository;
    private final MultipartFile sampleFile;
    private final MultipartFile alarmFile;

    /**
     * Constructeur
     *
     * @param form                 les informations saisies par l'utilisateur
     * @param benchRepository      benchRepository
     * @param campaignRepository   campaignRepository
     * @param experimentRepository experimentRepository
     * @param alarmRepository      alarmRepository
     * @param sampleFile           fichier contenant les mesures et les échantillons
     * @param alarmFile            fichier contenant les alarmes
     */
    public InsertionDataManager(ImportDataForm form, BenchRepository benchRepository, CampaignRepository campaignRepository,
                                ExperimentRepository experimentRepository, AlarmRepository alarmRepository,
                                MultipartFile sampleFile, MultipartFile alarmFile) {
        this.form = Objects.requireNonNull(form);
        this.benchRepository = Objects.requireNonNull(benchRepository);
        this.campaignRepository = Objects.requireNonNull(campaignRepository);
        this.experimentRepository = Objects.requireNonNull(experimentRepository);
        this.alarmRepository = Objects.requireNonNull(alarmRepository);
        this.sampleFile = Objects.requireNonNull(sampleFile);
        this.alarmFile = alarmFile;
    }

    /**
     * Insert les données en base
     *
     * @param template template JDBC
     * @throws SQLException
     */
    public void insert(JdbcTemplate template) throws SQLException {
        try {
            boolean createBench = form.getExistantBenchId() == 0;
            boolean createCampaign = form.getExistantCampaignId() == 0;
            long benchId = createBench ? this.insertBench() : form.getExistantBenchId();
            long campaignId = createCampaign ? this.insertCampaign() : form.getExistantCampaignId();
            long newExperimentId = insertExperiment(benchId, campaignId);

            ExperimentMetadata metadata = Reader.create(newExperimentId, sampleFile, alarmFile, ROOT_PATH).createCSVFiles();
            copyFiles(metadata.getNbMeasures(), template);
            Experiment experiment = experimentRepository.findOne(newExperimentId);
            experiment.setBegin(LocalDateTime.parse(metadata.getStartTime()));
            experiment.setEnd(LocalDateTime.parse(metadata.getEndTime()));
            experimentRepository.save(experiment);
            alarmRepository.save(metadata.getAlarmList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Insert les fichiers csv générés dans la base de données
     *
     * @param numberFile le nombre de fichiers générés
     * @param template   template JDBC
     * @throws SQLException
     */
    private void copyFiles(int numberFile, JdbcTemplate template) throws SQLException {
        this.removeIndex(template);
        this.copy(template, "measures (experiment_increment, name, unit, frequency, experiment_id)", ROOT_PATH + File.separator + "measure.csv");
        for (int i = 1; i <= numberFile; i++) {
            this.copy(template, "samples (value, sample_time, experiment_increment, experiment_id)", ROOT_PATH + File.separator + "s" + i + ".csv");
        }
        new Thread(() -> this.addIndex(template)).start(); // ATTENTION SI UNE AUTRE INSERTION SE LANCE AVANT LA FIN DE LA THREAD
    }

    /**
     * Insert un fichier csv en base de données
     *
     * @param template templace JDBC
     * @param table    table dans laquelle insérer le fichier
     * @param filePath chemin du fichier à insérer
     */
    private void copy(JdbcTemplate template, String table, String filePath) {
        template.execute("Copy " + table + " FROM '" + filePath + "' DELIMITER ';' CSV HEADER encoding 'windows-1251'");
    }

    /**
     * Enregistre l'essai en base de données
     *
     * @param benchId    l'identifiant du banc auquel relier l'essai
     * @param campaignId la campagne à laquelle relier l'essai
     * @return l'identifiant de l'essai
     */
    private long insertExperiment(long benchId, long campaignId) {
        return experimentRepository.save(new Experiment(0L, form.getExperimentReference(), form.getExperimentName(), true, campaignId,  benchId)).getId();
    }

    /**
     * Enregistre la campagne de test en base de données
     *
     * @return l'identifiant de la campagne
     */
    private long insertCampaign() {
        return campaignRepository.save(new Campaign(form.getCampaignId12c())).getId();
    }

    /**
     * Enregistre le banc d'essai en base de données
     *
     * @return l'identifiant
     */
    private void removeIndex(JdbcTemplate template){
        template.execute("DROP INDEX sample_experiment_increment_index;");
    }

    private void addIndex(JdbcTemplate template){
        template.execute("CREATE INDEX sample_experiment_increment_index ON samples (experiment_increment);");
    }
    
    private long insertBench() {
        return benchRepository.save(new Bench(form.getBenchName())).getId();
    }
}
