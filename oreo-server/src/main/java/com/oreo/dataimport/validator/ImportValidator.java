package com.oreo.dataimport.validator;

import com.oreo.dataimport.payload.ImportDataForm;

/**
 * Cette classe réalise la validation coté serveur du formulaire d'import
 */
public class ImportValidator {

    public static void validate(ImportDataForm form) {
        checkBench(form.getBenchName(), form.getExistantBenchId());
    }

    private ImportValidator() {
    }

    /**
     * Vérifie que le données du formulaire sont valides
     *
     * @param form les données du formulaire
     */
    public static void check(ImportDataForm form) {
        checkBench(form.getBenchName(), form.getExistantBenchId());
        checkCampaign(form.getCampaignId12c());
        checkExperiment(form.getExperimentName());
    }

    /**
     * Vérifie que l'id d'un banc d'essai existant ou un nom pour un nouveau banc a été saisi.
     *
     * @param benchName       le nom du banc à créer
     * @param existantBenchId l'id du banc d'essai existant
     * @throws IllegalStateException si l'id vaut 0 et que le nom est vide
     */
    private static void checkBench(String benchName, long existantBenchId) {
        if (benchName.isEmpty() && existantBenchId == 0) {
            throw new IllegalStateException("Un banc doit être sélectionné ou un nom doit être spécifié pour en créer un nouveau");
        }
    }

    /**
     * Vérifie que l'id12c de la campagne n'est pas null et qu'il fait 12 caractères
     *
     * @param id12c l'id12c à vérifier
     * @throws IllegalStateException si l'id12c ne fait pas 12 caractères
     */
    private static void checkCampaign(String id12c) {
        if (id12c != null && !id12c.isEmpty() && id12c.length() != 12) {
            throw new IllegalStateException("L'id12c de la campagne doit comporter 12 caractères");
        }
    }

    /**
     * Vérifie que le nom de l'essai n'est pas vide
     *
     * @param experimentName le nom de l'essai
     * @throws IllegalStateException si le nom de l'essai est vide
     */
    private static void checkExperiment(String experimentName) {
        if (experimentName.isEmpty()) {
            throw new IllegalStateException("Le nom de l'essai doit être spécifié");
        }
    }
}
