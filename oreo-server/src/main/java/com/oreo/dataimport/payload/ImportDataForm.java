package com.oreo.dataimport.payload;

/**
 * Cette classe reprend les données du formulaire d'insert saisies par l'utilisateur
 */
public class ImportDataForm {

    /**
     * L'identifiant d'un banc d'essai déjà existant
     */
    private long existantBenchId;

    /**
     * L'identifiant d'une campagne d'essai déjà existante
     */
    private long existantCampaignId;

    /**
     * Le nom du banc d'essai à créer
     */
    private String benchName;

    /**
     * L'id12C de la campagne à créer
     */
    private String campaignId12c;

    /**
     * Le nom de l'essai à créer
     */
    private String experimentName;

    /**
     * La référence de l'essai
     */
    private String experimentReference;

    public ImportDataForm(long existantBenchId, String benchName, long existantCampaignId, String campaignId12c, String experimentName, String experimentReference) {
        this.benchName = benchName;
        this.existantBenchId = existantBenchId;
        this.existantCampaignId = existantCampaignId;
        this.campaignId12c = campaignId12c;
        this.experimentName = experimentName;
        this.experimentReference = experimentReference;
    }

    public String getBenchName() {
        return benchName;
    }

    public void setBenchName(String benchName) {
        this.benchName = benchName;
    }

    public long getExistantBenchId() {
        return existantBenchId;
    }

    public void setExistantBenchId(long existantBenchId) {
        this.existantBenchId = existantBenchId;
    }

    public long getExistantCampaignId() {
        return existantCampaignId;
    }

    public void setExistantCampaignId(long existantCampaignId) {
        this.existantCampaignId = existantCampaignId;
    }

    public String getCampaignId12c() {
        return campaignId12c;
    }

    public void setCampaignId12c(String campaignId12c) {
        this.campaignId12c = campaignId12c;
    }

    public String getExperimentName() {
        return experimentName;
    }

    public String getExperimentReference() {
        return experimentReference;
    }
}
