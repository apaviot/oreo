package com.oreo.dataimport.reader;

import com.oreo.entity.Alarm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe donnant des informations sur l'insertion d'un jeu de données en base de données
 */
public class ExperimentMetadata {

    private int nbMeasures;

    /**
     * Date de début de l'essai
     */
    private String startTime;

    /**
     * Date de fin de l'essai
     */
    private String endTime;

    /**
     * Liste des alarmes comprises dans l'essai
     */
    private final List<Alarm> alarmList = new ArrayList<>();

    public ExperimentMetadata() {
    }

    public int getNbMeasures() {
        return nbMeasures;
    }

    public void setNbMeasures(int nbMeasures) {
        this.nbMeasures = nbMeasures;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean addAlarm(Alarm alarm) {
        return alarmList.add(alarm);
    }

    public List<Alarm> getAlarmList() {
        return Collections.unmodifiableList(alarmList);
    }
}
