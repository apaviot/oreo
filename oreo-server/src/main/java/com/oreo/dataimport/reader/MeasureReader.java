package com.oreo.dataimport.reader;

import java.util.Objects;

/**
 * Classe utilisée pour stockée les informations sur les mesures lors du parsing des fichiers CSV
 */
public class MeasureReader {
    private long id;
    private String name;
    private String type;
    private String unity;
    private int frequency;

    public MeasureReader(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setType(String type) {
        this.type = Objects.requireNonNull(type);
    }

    public void setUnity(String unity) {
        this.unity = Objects.requireNonNull(unity);
    }

    public String getType() {
        return type;
    }

    public String getUnity() {
        return unity;
    }
}
