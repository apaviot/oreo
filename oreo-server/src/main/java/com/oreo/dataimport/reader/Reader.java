package com.oreo.dataimport.reader;

import com.oreo.entity.Alarm;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Parse le fichier CSV des échantillons et le fichier CSV des alarmes
 */
@SuppressWarnings("Duplicates")
public class Reader {

    private final static int FIRST_COLUMN_SAMPLE_VALUE = 2;

    /**
     * Nombre de lignes maximales par fichier
     */
    private final static int MAX_LINE_PER_FILE = 100_000;

    private BufferedReader measureBufferReader;
    private BufferedReader alarmBufferReader;
    private ExperimentMetadata metadata = new ExperimentMetadata();
    private final long id;
    private final Path directoryPath;


    private Reader(long id, BufferedReader measureBufferReader, BufferedReader alarmBufferReader, Path directoryPath) {
        if (id < 0) {
            throw new IllegalArgumentException("experimentId less than 0");
        }

        this.id = id;
        this.measureBufferReader = measureBufferReader;
        this.alarmBufferReader = alarmBufferReader;
        this.directoryPath = directoryPath;
    }

    /**
     * Crée un objet reader pour parser un fichier csv de mesures et d'alarmes
     *
     * @param id              identifiant de l'essai créé
     * @param mesureMultipart le fichier csv des mesures
     * @param alarmMultipart  le fichier csv des alarmes. Peut être null
     * @param directoryPath   path dans lequel créer les fichiers temporaires
     * @return un objet reader pour parser les fichiers
     * @throws IOException si les fichiers ne peuvent pas être ouverts
     */
    public static Reader create(long id, MultipartFile mesureMultipart, MultipartFile alarmMultipart, Path directoryPath) throws IOException {
        BufferedReader essaiBufferReader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(mesureMultipart).getInputStream()));

        BufferedReader alarmBufferReader = null;
        if (alarmMultipart != null) {
            alarmBufferReader = new BufferedReader(new InputStreamReader(alarmMultipart.getInputStream()));
        }
        return new Reader(id, essaiBufferReader, alarmBufferReader, Objects.requireNonNull(directoryPath));
    }

    /**
     * Crée un nouveau path à partir du dossier où doivent être créer les fichiers temporaires et le nom d'un fichier
     *
     * @param fileName le nom du fichier
     * @return le path du fichier
     */
    private Path createFilePath(String fileName) {
        return Paths.get(directoryPath + File.separator + fileName);
    }


    /**
     * Récupère les champs dates de début et de fin contenus dans le fichier
     *
     * @return : Un string contetant une date
     * @throws IOException : Si erreur lors de la lecture
     */
    private String getTime() throws IOException {
        return changeDecimalNotation(measureBufferReader.readLine().split(";")[1]);
    }

    /**
     * Parse le fichier des mesures et renvoie une liste d'objets contenant les informations sur les mesures.
     *
     * @return la liste des informations sur les mesures parsées
     * @see MeasureReader
     * <p>
     * Les échantillons ne sont pas parser à ce niveaux.
     */
    private List<MeasureReader> createMeasureFiles() {
        try {
            int index = 0;
            if (!Files.exists(directoryPath)) {
                Files.createDirectory(directoryPath);
            }
            Path measureCsvPath = createFilePath("measure.csv");
            List<MeasureReader> list = getMeasureReaders();
            try (BufferedWriter bw = Files.newBufferedWriter(measureCsvPath)) {
                addTypeAndUnity(list); // Ajoute le type et l'unité des mesures dans les readers
                for (MeasureReader measureReader : list) {
                    bw.write(new StringBuilder().append(index++).append(";").append("'").append(measureReader.getName()).append("'").append(";")
                            .append("'").append(measureReader.getUnity()).append("'").append(";")
                            .append(10).append(";")
                            .append(id).toString());
                    bw.newLine();
                }

            }
            return list;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Parse les échantillons et crée des fichiers csv temporaires qui seront insérés en base de données
     *
     * @param measureReaders la liste des informations sur les mesures
     * @return le nombre de fichiers csv créés
     * @throws IOException
     */
    private int createSampleFiles(List<MeasureReader> measureReaders) throws IOException {
        String line;
        int indexFile = 1;

        BufferedWriter bw = Files.newBufferedWriter(createFilePath("s" + indexFile + ".csv"));

        int index = 0;
        while ((line = measureBufferReader.readLine()) != null) {
            List<String> samples = new ArrayList<>(Arrays.asList(line.split(";")));
            for (int i = 0; i < measureReaders.size(); i++) {

                // Le fichier csv atteint le nombre de lignes maximum que nous nous sommes fixé. On en crée un nouveau
                if (index == MAX_LINE_PER_FILE) {
                    bw.close();
                    bw = Files.newBufferedWriter(createFilePath("s" + (++indexFile) + ".csv"));
                    index = 0;
                }
                if (i + FIRST_COLUMN_SAMPLE_VALUE < samples.size()
                        && samples.get(i + FIRST_COLUMN_SAMPLE_VALUE).length() > 0
                        && !samples.get(i + FIRST_COLUMN_SAMPLE_VALUE).equalsIgnoreCase("nan")) {
                    bw.write(new StringBuilder()
                            .append(changeDecimalNotation(samples.get(i + FIRST_COLUMN_SAMPLE_VALUE))).append(";")
                            .append("'").append(metadata.getStartTime().split("T")[0]).append(" ").append(changeDecimalNotation(samples.get(1))).append("'").append(";")
                            .append(i).append(";").append(id).toString());
                    bw.newLine();
                    index++;
                }
            }
        }
        bw.close();
        return indexFile;
    }

    /**
     * Change le caractère ',' en '.' pour l'insertion en base
     *
     * @param string la chaîne de caractères dans laquelle modifier les caractères
     * @return la nouvelle chaîne de caractères
     */
    private static String changeDecimalNotation(String string) {
        return string.replace(',', '.');
    }

    /**
     * Crée une liste de MeasureReader pour récupérer les informations sur les mesures
     *
     * @return la liste des MeasureReader
     * @throws IOException
     */
    private List<MeasureReader> getMeasureReaders() throws IOException {
        return Arrays.stream(measureBufferReader.readLine().split(";"))
                .skip(FIRST_COLUMN_SAMPLE_VALUE).filter(el -> !el.isEmpty())
                .map(MeasureReader::new).collect(Collectors.toList());
    }

    /**
     * Ajout du type et unité dans la liste des mesures
     *
     * @param measureReaderList : La liste des mesures
     * @throws IOException : Si erreur lors de la lecture
     */
    private void addTypeAndUnity(List<MeasureReader> measureReaderList) throws IOException {
        measureBufferReader.readLine(); // ligne blanche
        List<String> listType = Arrays.stream(measureBufferReader.readLine().split(";")).skip(FIRST_COLUMN_SAMPLE_VALUE).collect(Collectors.toList());
        List<String> listUnity = Arrays.stream(measureBufferReader.readLine().split(";")).skip(FIRST_COLUMN_SAMPLE_VALUE).collect(Collectors.toList());
        for (int i = 0; i < listType.size(); i++) {
            MeasureReader m = measureReaderList.get(i);
            m.setType(listType.get(i));
            String unity = listUnity.get(i);
            if (!unity.isEmpty()) {
                m.setUnity(unity);
            }
        }
    }

    /**
     * Supprime tous les anciens fichiers csv générés lors du précédent import
     */
    private void deleteOldFiles() {
        try {
            Files.newDirectoryStream(directoryPath).forEach(p -> {
                try {
                    Files.delete(p);
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            });
        } catch (NoSuchFileException e) {
            // Nothing to do
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    /**
     * Parse les fichiers alarmes et mesures et crée les fichiers csv à insérer en base de données
     *
     * @return les informations récupérées dans le parsing du fichiers nécéssaires à l'insert en base
     * @throws IOException
     */
    public ExperimentMetadata createCSVFiles() throws IOException {
        deleteOldFiles();

        metadata.setStartTime(getTime());
        metadata.setEndTime(getTime());

        List<MeasureReader> measureReaders = createMeasureFiles();
        int nbMeasure = createSampleFiles(measureReaders);
        metadata.setNbMeasures(nbMeasure);

        if (alarmBufferReader != null) {
            writeAlarm();
        }
        return metadata;
    }

    /**
     * Création des objets alarmes
     *
     * @throws IOException : Si erreur dans la lecture du fichier
     */
    private void writeAlarm() throws IOException {
        String line;
        while ((line = alarmBufferReader.readLine()) != null) {
            String[] splitedLine = line.split(";");
            metadata.addAlarm(new Alarm(null, null, null, splitedLine[2], Integer.parseInt(splitedLine[1]), null, LocalDateTime.parse(metadata.getStartTime().split("T")[0] + "T" + splitedLine[0].split(" ")[1]), id));
        }
    }
}
