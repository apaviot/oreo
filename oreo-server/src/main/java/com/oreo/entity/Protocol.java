package com.oreo.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.oreo.protocol.ProtocolConfiguration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

/**
 * Mapping de la table protocols
 */
@Entity
@Table(name = "protocols")
public final class Protocol {
    @Id
    @Column(name = "id")
    @JsonProperty
    private String id;

    @Column(name = "info")
    @JsonProperty
    private ProtocolConfiguration info;

    public Protocol() {
    }

    public Protocol(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public ProtocolConfiguration getInfo() {
        return info;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Protocol)) return false;
        Protocol protocol = (Protocol) o;
        return Objects.equals(id, protocol.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Protocol {id: " + id + ", " + info + "}";
    }
}
