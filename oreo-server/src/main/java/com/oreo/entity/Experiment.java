package com.oreo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oreo.utils.LocalDateTimeDeserializer;
import com.oreo.utils.LocalDateTimeSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Mapping de la table experiments
 */
@Entity
@Table(name = "experiments")
public final class Experiment {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "reference")
    private String reference;

    @Column(name = "name")
    private String name;

    @Column(name = "last_used")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime lastUse;

    @Column(name = "begin_time")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime begin;

    @Column(name = "end_time")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime end;

    @Column(name = "is_local")
    private boolean isLocal;

    @Column(name = "campaign_id")
    private long campaignId;

    @Column(name = "bench_id")
    private long benchId;

    public Experiment() {
    }

    public Experiment(long id, String reference, String name, String lastUse, boolean isLocal, long campaignId, long benchId) {
        this(id, reference, name, LocalDateTime.parse(lastUse), isLocal, campaignId, benchId);
    }

    public Experiment(long id, String reference, String name, boolean isLocal, long campaignId, long benchId) {
        this.id = id;
        this.reference = reference;
        this.name = name;
        this.isLocal = isLocal;
        this.campaignId = campaignId;
        this.benchId = benchId;
    }

    public Experiment(long id, String reference, String name, LocalDateTime lastUse, boolean isLocal, long campaignId, long benchId) {
        this.id = id;
        this.reference = reference;
        this.name = name;
        this.lastUse = lastUse;
        this.isLocal = isLocal;
        this.campaignId = campaignId;
        this.benchId = benchId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getLastUse() {
        return lastUse;
    }

    public void setLastUse(LocalDateTime lastUse) {
        this.lastUse = lastUse;
    }

    public boolean getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(boolean isLocal) {
        this.isLocal = isLocal;
    }

    public long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(long testCampaignId) {
        this.campaignId = testCampaignId;
    }

    public long getBenchId() {
        return benchId;
    }

    public void setBenchId(long testBenchId) {
        this.benchId = testBenchId;
    }

    @JsonIgnore
    public List<Measure> getMeasures() {
        return new ArrayList<>();
    }

    @JsonIgnore
    public List<Alarm> getAlarms() {
        return new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Experiment)) {
            return false;
        }
        Experiment o = (Experiment) obj;
        return id == o.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Experiment {id: " + id + ", name: " + name + ", reference: " + reference + ", storedOnline: " + isLocal + "}";
    }

    public LocalDateTime getBegin() {
        return begin;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setBegin(LocalDateTime begin) {
        this.begin = begin;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }
}
