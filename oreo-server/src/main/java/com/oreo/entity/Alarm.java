package com.oreo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oreo.utils.LocalDateTimeDeserializer;
import com.oreo.utils.LocalDateTimeSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Mapping de la table alarm
 */
@Entity
@Table(name = "alarms")
public final class Alarm {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "reference")
    private String reference;

    @Column(name = "state")
    private String state;

    @Column(name = "message")
    private String message;

    @Column(name = "level")
    private int level;

    @Column(name = "alarm_time")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime time;

    @Column(name = "alarm_order")
    private String order;

    @Column(name = "experiment_id")
    @JsonIgnore
    private long experimentId;

    public Alarm() {
    }

    public Alarm(String reference, String name, String state, String message, int level, String order, LocalDateTime time, long experimentId) {
        this.reference = reference;
        this.name = name;
        this.state = state;
        this.message = message;
        this.level = level;
        this.order = order;
        this.time = time;
        this.experimentId = experimentId;
    }

    public Alarm(long id, String reference, String name, String state, String message, int level, String order, LocalDateTime time, int experimentId) {
        this.id = id;
        this.reference = reference;
        this.name = name;
        this.state = state;
        this.message = message;
        this.level = level;
        this.order = order;
        this.time = time;
        this.experimentId = experimentId;
    }

    public Alarm(long id, String reference, String name, String state, String message, int level, String order, String time, int experimentId) {
        this(id, reference, name, state, message, level, order, LocalDateTime.parse(time), experimentId);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public long getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(long experimentId) {
        this.experimentId = experimentId;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Alarm)) {
            return false;
        }
        Alarm o = (Alarm) obj;
        return id == o.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
