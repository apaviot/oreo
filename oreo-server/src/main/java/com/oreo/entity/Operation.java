package com.oreo.entity;

import java.util.Objects;
import java.util.function.BiFunction;

/**
 * Enumère les opérations à stocker dans la colonne 'operation' de la table partial_modification
 *
 * Chaque opération propose une fonction qui prend en paramètre un échantillon et un double et qui retourne un nouvel échantillon
 */
public enum Operation {
    ADD((s, v) -> {
        return new Sample(s.getId(), s.getValue() + v, s.getTime());
    }),
    MULTIPLY((s, v) -> {
        return new Sample(s.getId(), s.getValue() * v, s.getTime());
    }),
    CONSTANT((s, v) -> {
        return new Sample(s.getId(), v, s.getTime());
    });

    private BiFunction<Sample, Double, Sample> modificationFunction;

    Operation(BiFunction<Sample, Double, Sample> modificationFunction) {
        this.modificationFunction = Objects.requireNonNull(modificationFunction);
    }

    public BiFunction<Sample, Double, Sample> getModificationFunction() {
        return modificationFunction;
    }
}
