package com.oreo.entity;

import javax.persistence.*;
import java.util.Objects;

/**
 * Mapping de la table bench
 */
@Entity
@Table(name = "benches")
public final class Bench {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "reference")
    private String reference;

    public Bench() {
    }

    public Bench(String name) {
        this.name = name;
    }

    public Bench(int id, String reference, String name) {
        this.id = id;
        this.reference = reference;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Bench)) {
            return false;
        }
        Bench o = (Bench) obj;
        return id == o.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
