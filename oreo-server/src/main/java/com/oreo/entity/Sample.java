package com.oreo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oreo.utils.LocalDateTimeDeserializer;
import com.oreo.utils.LocalDateTimeSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Mapping de la table samples
 */
@Entity
@Table(name = "samples")
public final class Sample {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "value")
    private double value;

    @Column(name = "sample_time")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime time;

    @JsonIgnore
    @Column(name = "experiment_id")
    private long experimentId;

    @JsonIgnore
    @Column(name = "experiment_increment")
    private long experimentIncrement;

    public Sample() {
    }

    public Sample(long id, double value, LocalDateTime time) {
        this.id = id;
        this.value = value;
        this.time = time;
    }

    public Sample(long id, double value, String time) {
        this(id, value, LocalDateTime.parse(time));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public long getExperimentId() {
        return this.experimentId;
    }

    public void setExperimentId(long measureId) {
        this.experimentId = measureId;
    }

    public long getExperimentIncrement() {
        return experimentIncrement;
    }

    public void setExperimentIncrement(long experimentIncrement) {
        this.experimentIncrement = experimentIncrement;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Sample)) {
            return false;
        }
        Sample o = (Sample) obj;
        return id == o.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Sample {id: " + id + ", time: " + time + ", value: " + value + "}";
    }
}
