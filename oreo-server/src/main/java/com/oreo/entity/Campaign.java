package com.oreo.entity;

import javax.persistence.*;
import java.util.Objects;

/**
 * Mapping de la table campaign
 */
@Entity
@Table(name = "campaigns")
public final class Campaign {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "id12c")
    private String id12c;

    public Campaign() {
    }

    public Campaign(String id12c) {
        this.id12c = id12c;
    }

    public Campaign(long id, String id12c) {
        this.id = id;
        this.id12c = id12c;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getId12c() {
        return id12c;
    }

    public void setId12c(String id12c) {
        this.id12c = id12c;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Campaign)) {
            return false;
        }
        Campaign o = (Campaign) obj;
        return id == o.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
