package com.oreo.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oreo.utils.LocalDateTimeDeserializer;
import com.oreo.utils.LocalDateTimeSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Mapping de la table partial_modifications
 */
@Entity
@Table(name = "partial_modifications")
public final class PartialModification {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "started_date")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime startedDate;

    @Column(name = "ended_date")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime endedDate;

    @Column(name = "operator")
    private double operator;

    @Column(name = "operation")
    @Enumerated(EnumType.STRING)
    private Operation operation;

    @Column(name = "measure_id")
    private long measureId;

    public PartialModification() {
    }

    public PartialModification(long id, String startedDate, String endedDate, double operator, Operation operation, long measureId) {
        this(id, LocalDateTime.parse(startedDate), LocalDateTime.parse(endedDate), operator, operation, measureId);
    }

    public PartialModification(long id, LocalDateTime startedDate, LocalDateTime endedDate, double operator, Operation operation, long measureId) {
        this.id = id;
        this.startedDate = startedDate;
        this.endedDate = endedDate;
        this.operator = operator;
        this.operation = operation;
        this.measureId = measureId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(LocalDateTime startedDate) {
        this.startedDate = startedDate;
    }

    public LocalDateTime getEndedDate() {
        return endedDate;
    }

    public void setEndedDate(LocalDateTime endedDate) {
        this.endedDate = endedDate;
    }

    public double getOperator() {
        return operator;
    }

    public void setOperator(double operator) {
        this.operator = operator;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public long getMeasureId() {
        return measureId;
    }

    public void setMeasureId(long measureId) {
        this.measureId = measureId;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PartialModification)) {
            return false;
        }
        PartialModification o = (PartialModification) obj;
        return id == o.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Partial {id: " + id + ", operator: " + operator + ", operation: " + operation + "}";
    }
}
