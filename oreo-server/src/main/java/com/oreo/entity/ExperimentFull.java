package com.oreo.entity;

import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDateTime;
import java.util.List;

@Projection(name = "full", types = {Experiment.class})
public interface ExperimentFull {

    public long getId();

    public String getReference();

    public String getName();

    public LocalDateTime getLastUsed();

    public boolean getIsLocal();

    public long getCampaignId();

    public long getBenchId();

    List<Alarm> getAlarms();

    List<Measure> getMeasures();
}
