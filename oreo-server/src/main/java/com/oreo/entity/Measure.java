package com.oreo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

/**
 * Mapping de la table measure
 */
@Entity
@Table(name = "measures")
public final class Measure {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonIgnore
    @Column(name = "experiment_increment")
    private long experimentIncrement;

    @Column(name = "name")
    private String name;

    @Column(name = "unit")
    private String unit;

    @Column(name = "frequency")
    private double frequency;

    @JsonIgnore
    @Column(name = "experiment_id")
    private long experimentId;

    public Measure() {
    }

    public Measure(long id, String name, String unit, int frequency, long experimentId) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.frequency = frequency;
        this.experimentId = experimentId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getFrequency() {
        return frequency;
    }

    public void setFrequency(double frequency) {
        this.frequency = frequency;
    }

    public long getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(long experimentId) {
        this.experimentId = experimentId;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Measure)) {
            return false;
        }
        Measure o = (Measure) obj;
        return id == o.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }
}
