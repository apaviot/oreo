package com.oreo.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.oreo.utils.LocalDateTimeDeserializer;
import com.oreo.utils.LocalDateTimeSerializer;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Mapping de la table modifications
 */
@Entity
@Table(name = "modifications")
public final class Modification {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "creation_date")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime creationDate;

    @Column(name = "last_use")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime lastUse;

    @Column(name = "description")
    private String description;

    @Column(name = "isRemote")
    private boolean isRemote;

    @Column(name = "experiment_id")
    private long experimentId;

    @JoinColumn(name = "modification_id")
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<PartialModification> partials = new ArrayList<>();

    public Modification() {
    }

    public Modification(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastUse() {
        return lastUse;
    }

    public void setLastUse(LocalDateTime lastUse) {
        this.lastUse = lastUse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isRemote() {
        return isRemote;
    }

    public void setIsRemote(boolean isRemote) {
        this.isRemote = isRemote;
    }

    public long getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(long experimentId) {
        this.experimentId = experimentId;
    }

    public List<PartialModification> getPartials() {
        return this.partials;
    }

    public void setPartials(List<PartialModification> partials) {
        this.partials = partials;
    }


    public void addPartial(PartialModification partial) {
        if (partials == null) {
            this.partials = new ArrayList<>();
        }
        this.partials.add(Objects.requireNonNull(partial));
        //partial.setModification(this);
    }

    public void removePartial(PartialModification partial) {
        if (partials != null) {
            this.partials.remove(partial);
            //partial.setModification(null);
        }
    }


    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Modification)) {
            return false;
        }
        Modification o = (Modification) obj;
        return id == o.id;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Modification {id: " + id + ",  name: " + name + ", description: " + description + ", partials: " + partials + "}";
    }
}
