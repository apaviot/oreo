package com.oreo.controller;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UncheckedIOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Controller qui expose des méthodes API avancées pour effectuer des requêtes SQL
 */
@Api(tags = "SQL Entity", description = "Simple Jpa Repository")
@RestController
@RequestMapping("/api/sql")
public class SQLController {

    @Autowired
    private JdbcTemplate template;

    /**
     * Execute une requête SQL et renvoie le résultat.
     * <p>
     * Seule les opérations de lecture sont autorisées.
     *
     * @param query la requête SQL à exécuter
     * @return le résultat de la requête
     * @throws IllegalArgumentException si la requête sql ne se limite pas à de la lecture
     */
    @PostMapping(consumes = MediaType.TEXT_PLAIN_VALUE)
    @Transactional(readOnly = true)
    public ArrayList<HashMap<String, Object>> execute(@RequestBody() String query) {
        try {
            return template.query(query, rs -> {
                ArrayList<HashMap<String, Object>> list = new ArrayList<>();
                int columns = rs.getMetaData().getColumnCount();
                ResultSetMetaData metaData = rs.getMetaData();
                while (rs.next()) {
                    HashMap<String, Object> map = new HashMap<>();
                    for (int i = 1; i <= columns; i++) {
                        String key = metaData.getColumnName(i);
                        Object value = rs.getObject(i);
                        map.put(key, value);
                    }
                    list.add(map);
                }
                return list;
            });
        } catch (UncategorizedSQLException e) {
            throw new IllegalArgumentException("Seules les requêtes de lecture sont autorisées");
        }
    }

    /**
     * Effectue une requête SQL puis exporte le résultat dans un fichier CSV.
     * <p>
     * Seule les opérations de lecture sont autorisées.
     *
     * @param query    la requête SQL à exécuter
     * @param response le fichier CSV généré
     * @throws IllegalArgumentException si la requête sql ne se limite pas à de la lecture
     * @throws UncheckedIOException     si une erreur survient lors de l'écriture du fichier CSV
     */
    @PostMapping(value = "/export", consumes = MediaType.TEXT_PLAIN_VALUE)
    @Transactional(readOnly = true)
    public @ResponseBody
    void exportResult(@RequestBody() String query, HttpServletResponse response) {
        template.query(query, rs -> {
            try {
                int columns = rs.getMetaData().getColumnCount();
                ResultSetMetaData metaData = rs.getMetaData();
                PrintWriter writer = response.getWriter();

                // Write columns name
                if (columns >= 1) {
                    writer.print(metaData.getColumnName(1));
                    for (int i = 2; i <= columns; i++) {
                        writer.print(';');
                        writer.print(metaData.getColumnName(i));
                    }
                }

                // Write lines
                do {
                    writer.println();
                    writeLine(writer, rs, columns);
                } while (rs.next());

                response.setContentType("text/csv");
                response.addHeader("Content-Disposition", "attachment; filename=export_sql.csv");
                response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            } catch (UncategorizedSQLException e) {
                throw new IllegalArgumentException("Seules les requêtes de lecture sont autorisées");
            }
        });
    }

    /**
     * Ajoute une ligne dans le fichier CSV à partir du resultset
     *
     * @param writer    writer dans lequel écrire la ligne
     * @param rs        ResultSet le résultat de la requête SQL
     * @param nbColumns nombre de colonnes dans le fichier CSV
     * @throws SQLException
     */
    private void writeLine(PrintWriter writer, ResultSet rs, int nbColumns) throws SQLException {
        if (nbColumns >= 1) {
            writer.print(rs.getObject(1));
            for (int i = 2; i <= nbColumns; i++) {
                writer.print(';');
                writer.print(rs.getObject(i));
            }
        }
    }
}
