package com.oreo.controller;

import com.oreo.entity.Measure;
import com.oreo.entity.Modification;
import com.oreo.entity.Sample;
import com.oreo.repository.MeasureRepository;
import com.oreo.repository.ModificationRepository;
import com.oreo.repository.SampleRepository;
import com.oreo.utils.SampleModifier;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Controller qui expose des méthodes API avancées pour gérer les échantillons.
 */
@Api(tags = "Sample Entity", description = "Simple Jpa Repository")
@RestController
@RequestMapping("/api/samples")
public class SampleController {
    class Data {
        LocalDateTime former;

        Data(LocalDateTime start) {
            this.former = start;
        }
    }

    @Autowired
    private SampleRepository repository;

    @Autowired
    private MeasureRepository measureRepository;

    @Autowired
    private ModificationRepository modificationRepository;

    /**
     * Renvoi tous les échantillons d'une mesure avec un interval entre chaque échantillon.
     * <p>
     * Une modification peut être spécifiée pour récupérer les échantillons avec une valeur modifiée.
     *
     * @param measureId      id de la mesure pour laquelle récupérer les échantillons
     * @param start          date à partir de laquelle récupérer les échantillons
     * @param end            date jusqu'à laquelle récupérer les échantillons
     * @param interval       interval minimal en millisecondes entre chaque échantillon
     * @param modificationId l'identifiant de la mesure à appliquer. Peut être null
     * @return
     */
    @GetMapping
    @ApiResponses(
            @ApiResponse(code = 200, message = "OK", response = Sample.class, responseContainer = "list")
    )
    public ResponseEntity<?> findAll(
            @RequestParam() long measureId,
            @RequestParam() @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime start,
            @RequestParam() @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime end,
            @RequestParam() int interval,
            @RequestParam(required = false) Long modificationId
    ) {
        Measure measure = measureRepository.findById(measureId);
        if (measure == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        Data data = new Data(start);


        float intervalFloat = ((float) interval) / 1000;
        if (intervalFloat <= 0) {
            intervalFloat = 0.5f; // interval de 500 ms par défaut
        }
        List<Sample> samples = repository.findAll(measureId, start, end, intervalFloat);

        // On crée un objet en charge de modifier les échantillons
        if (modificationId != null) {
            Modification modification = modificationRepository.findOne(modificationId);
            if (modification == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No modification found with the given id");
            }
            if (modification.getExperimentId() != measure.getExperimentId()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The given modification does not match the measure");
            }
            SampleModifier modifier = new SampleModifier(modification);
            samples = modifier.apply(samples, measureId);
        }

        return ResponseEntity.status(HttpStatus.OK).body(samples);
    }

    /**
     * Retourne le nombre d'échantillons pour une mesure
     *
     * @param measureId l'identifiant de la mesure pour laquelle récupérer le nombre d'échantillons
     * @return le nombre déchantillons pour la mesure
     */
    @GetMapping("/search/countByMeasure/{measureId}")
    public ResponseEntity<Long> countByMeasure(@PathVariable long measureId) {
        Optional<Long> result = repository.countByMeasure(measureId);
        if (!result.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result.get());
    }
}
