package com.oreo.controller;

import com.oreo.dataimport.InsertionDataManager;
import com.oreo.dataimport.payload.ImportDataForm;
import com.oreo.dataimport.validator.ImportValidator;
import com.oreo.repository.AlarmRepository;
import com.oreo.repository.BenchRepository;
import com.oreo.repository.CampaignRepository;
import com.oreo.repository.ExperimentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.UncheckedIOException;
import java.sql.SQLException;

/**
 * Controller permettant l'insert des bancs, campagnes, essais, mesures, échantillons et alarmes des données saisies par l'utilisateur
 * et des fichiers issues du formulaire d'import
 */
@RestController
public class ImportFileController {

    /**
     * Template de la database pour native query
     */
    @Autowired
    private JdbcTemplate template;

    /**
     * Repository des bancs
     */
    @Autowired
    private BenchRepository benchRepository;

    /**
     * Repository des campagnes
     */
    @Autowired
    private CampaignRepository campaignRepository;

    /**
     * Repository des essais
     */
    @Autowired
    private ExperimentRepository experimentRepository;

    /**
     * Repository des alarmes
     */
    @Autowired
    private AlarmRepository alarmRepository;


    private final Object monitor = new Object();


    /**
     * Méthode pour insérer des données dans la base de données
     * <p>
     * benchName doit être renseigné OU benchChoice doit être différent de 0
     * Si benchChoice est différent de 0 alors un nouveau banc d'essai est créé en base de données avec le nom benchName
     * <p>
     * campaignId12C doit être reneigné OU campaignChoice doit être différent de 0
     * Si campaignChoice est différent de 0 alors une nouvelle campagne d'essai est créée en base de données avec l'id12c campaignId12C
     *
     * @param benchChoice         l'identifiant du banc d'essai auquel relier l'essai à importer
     * @param benchName           le nom du banc d'essai à créer
     * @param campaignChoice      l'identifiant de la campagne de test à laquelle relier l'essai à importer
     * @param campaignId12C       l'id12C de la campagne de tests à créer
     * @param experimentName      le nom de l'essai à créer
     * @param experimentReference la référence de l'essai à créer
     * @param sampleFile          le fichier CSV contenant les échantillons de l'essai
     * @param alarmFile           le fichier CSV contenant les alarmes
     * @return Code http 200 si l'import réussi; http BAD_REQUEST si les arguments de la requête no sont pas bons; http INTERNAL_ERROR si l'import échoue
     */
    @CrossOrigin("*")
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public ResponseEntity importFile(
            @RequestParam("benchChoice") int benchChoice,
            @RequestParam("benchName") String benchName,
            @RequestParam("campaignChoice") int campaignChoice,
            @RequestParam("campaignId") String campaignId12C,
            @RequestParam("experimentName") String experimentName,
            @RequestParam("experimentReference") String experimentReference,
            @RequestParam("sampleFile") MultipartFile sampleFile,
            @RequestParam(value = "alarmFile", required = false) MultipartFile alarmFile
    ) {
        try {
            ImportDataForm form = new ImportDataForm(benchChoice, benchName, campaignChoice, campaignId12C, experimentName, experimentReference);
            ImportValidator.validate(form);
            synchronized (monitor) {
                InsertionDataManager insertionDataManager = new InsertionDataManager(form, benchRepository, campaignRepository, experimentRepository, alarmRepository, sampleFile, alarmFile);
                insertionDataManager.insert(template);
            }
        } catch (UncheckedIOException | SQLException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Les données n'ont pas put être importées dans la base de données");
        } catch (IllegalStateException e) {
            return ResponseEntity.badRequest().body(e.getMessage());

        }
        return ResponseEntity.ok(null);
    }
}