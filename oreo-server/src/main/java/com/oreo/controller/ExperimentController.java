package com.oreo.controller;

import com.oreo.entity.Experiment;
import com.oreo.repository.ExperimentRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;

/**
 * Controller qui expose des méthodes API avancées pour gérer des essais.
 */
@Api(tags = "Experiment Entity", description = "Simple Jpa Repository")
@RepositoryRestController
@RequestMapping("/api/experiments")
public class ExperimentController {
    @Autowired
    private PagedResourcesAssembler pagedResourcesAssembler;

    @Autowired
    private ExperimentRepository experimentRepository;

    /**
     * Permet d'effectuer une recherche avancée sur des essais.
     * <p>
     * Tous les paramètres de recherche sont optionnels. S'ils ne sont pas renseignés alors il ne sont pas inclus dans la recherche.
     * La recherche sur le nom de l'essai est une recherche floue, c'est à dire que le filtre est validé si le nom de l'essai contient
     * la chaîne spécifiée.
     *
     * @param lastUse    date de dernière utilisation de l'essai
     * @param database   la base dans laquelle se trouve l'essai. 'remote' ou 'local'
     * @param campaignId l'identifiant de la campagne de l'essai
     * @param benchId    l'identifiant du banc d'essai
     * @param name       le nom de l'essai
     * @param pageable
     * @return
     */
    @GetMapping(path = "/advancedSearch")
    public ResponseEntity<Resources<Experiment>> advancedSearch(
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime lastUse,
            @RequestParam(required = false) String database,
            @RequestParam(required = false) Long campaignId,
            @RequestParam(required = false) Long benchId,
            @RequestParam(required = false) String name,
            Pageable pageable
    ) {
        try {
            Page<Experiment> page = experimentRepository.advancedSearch(
                    lastUse,
                    database,
                    campaignId,
                    benchId,
                    name,
                    pageable
            );
            if (page.getTotalPages() == 0) {
                return ResponseEntity.ok(pagedResourcesAssembler.toEmptyResource(page, Experiment.class, null));
            }
            return ResponseEntity.ok(pagedResourcesAssembler.toResource(page));
        } catch (IllegalArgumentException | InvalidDataAccessApiUsageException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(null);
        }
    }
}
