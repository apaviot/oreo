package com.oreo.controller;

import com.oreo.entity.Experiment;
import com.oreo.entity.Measure;
import com.oreo.repository.MeasureRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller qui expose des méthodes API avancées pour gérer des mesures.
 */
@Api(tags = "Measure Entity", description = "Simple Jpa Repository")
@RepositoryRestController
@RequestMapping("/api/measures")
public class MeasureController {
    @Autowired
    private PagedResourcesAssembler pagedResourcesAssembler;

    @Autowired
    private MeasureRepository measureRepository;

    /**
     * Méthode de recherche avancée pour rechercher une mesure à partir de l'identifiant d'un essai et d'un nom de mesure.
     * La recherche sur le nom de la mesure est une recherche floue, c'est à dire que pour valider le filtre, le nom de la mesure doit
     * inclure le nom recherché.
     *
     * @param experimentId l'identifiant d'un essai
     * @param name         le nom de la mesure
     * @param pageable
     * @return la liste des mesures repondant aux critères de recherche
     */
    @GetMapping(path = "/advancedSearch")
    public ResponseEntity<Resources<Measure>> advancedSearch(
            @RequestParam long experimentId,
            @RequestParam(required = false) String name,
            Pageable pageable
    ) {
        try {
            Page<Measure> page = measureRepository.advancedSearch(experimentId, name, pageable);
            if (page.getTotalPages() == 0) {
                return ResponseEntity.ok(pagedResourcesAssembler.toEmptyResource(page, Experiment.class, null));
            }
            return ResponseEntity.ok(pagedResourcesAssembler.toResource(page));
        } catch (IllegalArgumentException | InvalidDataAccessApiUsageException e) {
            return ResponseEntity.badRequest().body(null);
        }
    }
}
