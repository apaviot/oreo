package com.oreo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oreo.entity.Experiment;
import com.oreo.entity.Measure;
import com.oreo.entity.Modification;
import com.oreo.entity.Protocol;
import com.oreo.protocol.*;
import com.oreo.protocol.communication.appli.ProtocolExecutionPayload;
import com.oreo.repository.*;
import com.oreo.utils.SampleModifier;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller qui expose des méthodes API avancées pour gérer les protocoles et envoyer des données avec ceux-ci.
 */
@Api(tags = "Protocol Entity", description = "Simple Jpa Repository")
@RestController
@RequestMapping("/api/protocols")
public class ProtocolController {

    @Autowired
    private ExperimentRepository experimentRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private ProtocolRepository protocolRepository;

    @Autowired
    private MeasureRepository measureRepository;

    @Autowired
    private SampleRepository sampleRepository;

    @Autowired
    private AlarmRepository alarmRepository;

    @Autowired
    private ModificationRepository modificationRepository;

    /**
     * Modifie la configuration d'un protocol
     *
     * @param id            l'identifiant du protocol
     * @param configuration la configuration du protocol à enregistrer
     * @return HTTP OK si la modification est enregistrée, http BAD_REQUEST sinon
     */
    @PutMapping("/{id}/save")
    @Transactional
    public ResponseEntity<String> save(
            @PathVariable("id") String id,
            @RequestBody ProtocolConfiguration configuration
    ) {
        try {
            ProtocolChecker.check(id, configuration);

            em.joinTransaction();
            ObjectMapper mapper = new ObjectMapper();
            em.createNativeQuery("UPDATE protocols SET info=CAST(? AS json) WHERE id=?")
                    .setParameter(1, mapper.writeValueAsString(configuration))
                    .setParameter(2, id)
                    .executeUpdate();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body("ok");
    }

    /**
     * Lance l'envoi de données avec un protocol
     *
     * @param id         l'id du protocol à utiliser
     * @param experiment l'identifiant de l'essai à jouer
     * @param payload    les paramètres d'envoi
     * @return HTTP OK si l'envoie des données a commencé, HTTP BAD_REQUEST si la modification appliquée ne correspond pas à l'essai rejoué,
     * HTTP NOT FOUND si l'identifiant du protocol est inconnu ou si la modification renseignée n'existe pas, HTTP NO CONTENT si
     * l'envoi des données ne peut pas s'effectuer
     */
    @PostMapping(value = "/{id}/execute/{experiment}")
    public ResponseEntity executeProtocol(
            @PathVariable("id") String id,
            @PathVariable("experiment") long experiment,
            @RequestBody ProtocolExecutionPayload payload
    ) {
        try {
            ProtocolRunner executor = ProtocolRunnerFactory.get(id);
            if (executor == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Protocol inconnu: " + id);
            }

            List<Measure> measures = payload.getMeasures()
                    .stream()
                    .map(measureRepository::findById)
                    .limit(180) // Nombre maximum de mesures que l'on peut mettre dans un paquet udp
                    .collect(Collectors.toList());
            if (measures.isEmpty()) { // Si aucune mesure n'est renseignée alors on les récupère toutes
                // ATTENTION, SI PLUS DE 184 sont récupérées alors l'envoi en IENA échouera
                // A CAUSE DE LA TAILLE LIMITE D'UN PAQUET UDP
                measures = measureRepository.findListByExperimentId(experiment);
            }

            LocalDateTime start = payload.getStart();
            LocalDateTime end = payload.getEnd();
            checkExperiment(experiment, start, end);

            Protocol protocol = protocolRepository.findOne(id);
            if (protocol == null) {
                return ResponseEntity.notFound().build();
            }

            SampleModifier modifier = null;

            // Si une modification est spécifiée on crée un objet en charge de modifier les échantillons
            if (payload.getModificationId() != null) {
                Modification modification = modificationRepository.findOne(payload.getModificationId());
                if (modification == null) {
                    return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No modification found with the given id");
                }
                if (modification.getExperimentId() != experiment) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The given modification is not applicable to this experiment");
                }
                modifier = new SampleModifier(modification);
            }


            SampleSupplier data = new SampleSupplier(measures, sampleRepository, payload.getStart(), payload.getEnd(), modifier, protocol.getInfo().getFrequency());
            AlarmSupplier alarmSupplier = new AlarmSupplier(experiment, alarmRepository, start, end);
            executor.sendData(data, alarmSupplier, protocol.getInfo(), payload.isComputeTime());
            return ResponseEntity.ok(null);
        } catch (IllegalStateException e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        }
    }

    /**
     * Renvoi le fichier de configuration du protocol s'il en existe un.
     * Si le protocol renseigné n'a pas de fichier de configuration un code http NOT IMPLEMENTED est renvoyé.
     *
     * @param id         l'identifiant du protocol
     * @param experiment l'identifiant de l'essai
     * @param ids        les identifiants des mesures qui vont être envoyées avec la configuration
     * @param response
     */
    @PostMapping(value = "/{id}/export/{experiment}", consumes = {"application/json"})
    public @ResponseBody
    void exportProtocol(
            @PathVariable("id") String id,
            @PathVariable("experiment") long experiment,
            @RequestBody(required = false) List<Long> ids,
            HttpServletResponse response
    ) {
        try {
            ProtocolRunner runner = ProtocolRunnerFactory.get(id);
            if (runner == null) {
                response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
                response.getWriter().println("Impossible d'exporter un fichier de configuration pour le protocol " + id);
                return;
            }

            Protocol protocol = protocolRepository.findOne(id);
            if (protocol == null) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }

            List<Measure> measures = ids.stream().map(measureRepository::findById).collect(Collectors.toList());
            if (measures.isEmpty()) {
                measures = measureRepository.findListByExperimentId(experiment);
            }

            runner.exportFile(response, protocol.getInfo(), measures);
        } catch (IOException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Stop l'envoi des données avec le protocol spécifié.
     * Si aucune donnée n'était envoyée avec le protocol alors rien ne se passe.
     *
     * @param id l'identifiant du protocol pour lequel il faut interrompre l'envoi de données
     * @return HTTP OK
     */
    @PostMapping(value = "/{id}/stop")
    public ResponseEntity pauseExecuteProtocol(@PathVariable("id") String id) {
        ProtocolRunner executor = ProtocolRunnerFactory.get(id);
        if (executor == null) {
            return ResponseEntity.notFound().build();
        }
        executor.stop();
        return ResponseEntity.ok(null);
    }

    /**
     * Vérifie que la date de début et la date de fin spécifiée sont incluses entre la date de début et de fin du l'essai.
     * La date de début doit être avant la date de fin. La date de début doit être après la date de début de l'essai.
     * La date de fin doit être avant la date de fin de l'essai.
     *
     * @param experimentId l'identifiant de l'essai
     * @param start        la date de début
     * @param end          la date de fin
     * @throws IllegalArgumentException si les dates ne correspondent pas
     */
    private void checkExperiment(long experimentId, LocalDateTime start, LocalDateTime end) {
        Experiment experiment = experimentRepository.findOne(experimentId);
        if (start.isAfter(end)) {
            throw new IllegalArgumentException("Starting time is after ending time !");
        }
        if (start.isBefore(experiment.getBegin())) {
            throw new IllegalArgumentException("Starting time is before experiment starting time");
        }
        if (end.isAfter(experiment.getEnd())) {
            throw new IllegalArgumentException("Ending time is after experiment ending time");
        }
    }
}
