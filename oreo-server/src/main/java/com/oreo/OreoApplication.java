package com.oreo;

import com.oreo.config.RepositoryRestConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
@Import({RepositoryRestConfig.class})
public class OreoApplication {
	public static void main(String[] args) {
		SpringApplication.run(OreoApplication.class, args);
	}

    @Bean
    public MultipartConfigElement multipartConfigElement () {
        MultipartConfigFactory factory = new MultipartConfigFactory ();
        return factory.createMultipartConfig ();
    }
}
