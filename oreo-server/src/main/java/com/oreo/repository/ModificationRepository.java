package com.oreo.repository;

import com.oreo.entity.Modification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * Repository des modifications
 */
@RepositoryRestResource(collectionResourceRel = "modifications", path = "modifications")
public interface ModificationRepository extends PagingAndSortingRepository<Modification, Long> {
    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void deleteAll();

    /**
     * Retourne les modifications d'un essai
     * @param experimentId l'identifiant de l'essai
     * @return les modification de l'essai
     */
    List<Modification> findByExperimentId(@Param("experimentId") long experimentId);
}
