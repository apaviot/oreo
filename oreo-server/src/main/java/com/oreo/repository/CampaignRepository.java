package com.oreo.repository;

import com.oreo.entity.Campaign;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Repository des campagnes d'essai
 */
@RepositoryRestResource(collectionResourceRel = "campaigns", path = "campaigns")
public interface CampaignRepository extends PagingAndSortingRepository<Campaign, Long> {
    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void delete(Campaign campaign);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void delete(Long campaign);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void delete(Iterable<? extends Campaign> campaigns);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    <S extends Campaign> S save(S entity);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    <S extends Campaign> Iterable<S> save(Iterable<S> entity);
}
