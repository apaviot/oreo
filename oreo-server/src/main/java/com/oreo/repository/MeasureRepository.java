package com.oreo.repository;

import com.oreo.entity.Measure;
import com.oreo.helper.swagger.ResourcesOfAlarm;
import com.oreo.helper.swagger.ResourcesOfMeasure;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import springfox.documentation.annotations.ApiIgnore;
import java.util.List;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;

/**
 * Repository des mesures
 */
@Api(tags = "Measure Entity", description = "Simple Jpa Repository")
@RepositoryRestResource(collectionResourceRel = "measures", path = "measures")
public interface MeasureRepository extends Repository<Measure, Long>, JpaSpecificationExecutor<Measure> {

    /**
     * Recherche une mesure à partir de son identifiant
     * @param measureId l'identifiant d'une mesure
     * @return la mesure ou null si aucune ne correspond
     */
    @Query("from Measure m where m.id=:measureId")
    Measure findById(@Param("measureId") Long measureId);

    /**
     * Recherche les mesures d'un essai
     * @param experimentId l'identifiant de l'essai
     * @param pageable
     * @return les mesures de l'essai
     */
    @Query("from Measure m where m.experimentId=:experimentId")
    @ApiResponses(
            @ApiResponse(code = 200, message = "Ok", response = ResourcesOfMeasure.class)
    )
    Page<Measure> findByExperimentId(@Param("experimentId") long experimentId, Pageable pageable);

    @Query("from Measure m where m.experimentId=:experimentId")
    List<Measure> findListByExperimentId(@Param("experimentId") long experimentId);

    /**
     * Retourne true si une mesure avec l'identifiant donné existe, false sinon
     * @param measureId l'identifiant de la mesure
     * @return true si une mesure avec l'identifiant donné existe, false sinon
     */
    @ApiIgnore
    @RestResource(exported = false)
    boolean exists(Long measureId);

    /**
     * Recherche des mesures selon plusieurs critères.
     *
     * La recherche sur le nom de la mesure est une recherche floue, c'est-à-dire que le filtre vérifie si la chaîne est incluse
     * dans le nom de la mesure. ex: une mesure dont le nom est AZERTY  est renvoyé pour une recherche sur le nom "ERT"
     *
     * @param experimentId l'identifiant de l'essai
     * @param name nom de la mesure
     * @param pageable
     * @return les mesures répondant aux critères de sélection
     */
    default Page<Measure> advancedSearch(
            long experimentId,
            String name,
            Pageable pageable
    ) {
        return findAll((root, criteria, builder) -> {
            ArrayList<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get("experimentId"), experimentId));
            if (name != null) {
                predicates.add(builder.like(root.get("name"), "%" + name + "%"));
            }
            return builder.and(predicates.toArray(new Predicate[0]));
        }, pageable);
    }
}
