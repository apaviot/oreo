package com.oreo.repository;

import com.oreo.entity.Bench;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Repository des bancs d'essai
 */
@RepositoryRestResource(collectionResourceRel = "benches", path = "benches")
public interface BenchRepository extends PagingAndSortingRepository<Bench, Long> {
    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void delete(Bench bench);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void delete(Long bench);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void delete(Iterable<? extends Bench> benches);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    <S extends Bench> S save(S entity);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    <S extends Bench> Iterable<S> save(Iterable<S> entity);
}
