package com.oreo.repository;

import com.oreo.entity.Measure;
import com.oreo.entity.Sample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Repository des échantillons
 */
@Repository
@Transactional()
public class SampleRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private JdbcTemplate template;

    /**
     * Récupère tous les échantillons pour une mesure entre deux dates avec un interval.
     * <p>
     * A partir de la date de début on récupère un échantillon par interval. S'il y a plusieurs échantillons dans l'interval
     * on renvoit celui avec la plus grande valeur.
     *
     * @param measureId       l'identifiant de la mesure pour laquelle récupérer les échantillons
     * @param start           date à partir de laquelle récupérer les échantillons
     * @param end             date jusqu'à laquelle récupérer les échantillons
     * @param intervalSeconds interval en secondes entre deux échantillons
     * @return la liste des échantillons
     */
    public List<Sample> findAll(long measureId, LocalDateTime start, LocalDateTime end, float intervalSeconds) {
        String sql = "SELECT to_timestamp(floor((extract('epoch' from sample_time) / ?)) * ?) AT TIME ZONE 'UTC' AS sampleTime, MAX(s.value) val " +
                "FROM samples s " +
                "JOIN measures m ON m.id = ? AND s.experiment_increment = m.experiment_increment AND s.experiment_id = m.experiment_id " +
                "WHERE s.sample_time BETWEEN ? AND ? " +
                "GROUP BY sampleTime " +
                "ORDER BY sampleTime ASC";

        ArrayList<Sample> samples = new ArrayList<>();
        List<Map<String, Object>> result = template.queryForList(sql, intervalSeconds, intervalSeconds, measureId, Timestamp.valueOf(start), Timestamp.valueOf(end));

        for (Map<String, Object> row : result) {
            BigDecimal value = (BigDecimal) row.get("val");
            samples.add(new Sample(0, value.doubleValue(), ((Timestamp) row.get("sampletime")).toLocalDateTime()));
        }
        return samples;
    }

    /**
     * Récupère tous les échantillons pour une mesure.
     * <p>
     * Attention, l'utilisation de cette méthode peut être dangereuse pour la mémoire
     *
     * @param measureId l'identifiant de la mesure pour laquelle récupérer les échantillons
     * @return les échantillons de la mesure
     */
    public List<Sample> findAll(long measureId) {
        Query query = entityManager.createNativeQuery(
                "SELECT s.* FROM samples s JOIN experiments e " +
                        "ON s.experiment_id = e.id JOIN measures m on m.experiment_id = e.id WHERE m.id= ?1 " +
                        "AND m.experiment_increment=s.experiment_increment AND s.experiment_id = m.experiment_id ORDER BY s.sample_time ASC",
                Sample.class
        );
        query.setParameter(1, measureId);

        return query.getResultList();
    }

    /**
     * Récupère tous les échantillons pour une mesure entre deux dates.
     *
     * @param measureId l'identifiant de la mesure pour laquelle récupérer les échantillons
     * @param start     date à partir de laquelle récupérer les échantillons
     * @param end       date jusqu'à laquelle récupérer les échantillons
     * @param offset    nombre d'échantillons à ignorer
     * @param limit     nombre limite d'échantillons à retourner
     * @return les échantillons de la mesure
     */
    public List<Sample> findAll(long measureId, LocalDateTime start, LocalDateTime end, int offset, int limit) {
        Query query = entityManager.createNativeQuery(
                "SELECT s.* FROM samples s " +
                        "JOIN measures m on m.id = ?1 WHERE " +
                        "m.experiment_increment=s.experiment_increment " +
                        "AND s.experiment_id = m.experiment_id " +
                        "AND s.sample_time BETWEEN ?2 AND ?3 " +
                        "ORDER BY s.sample_time ASC LIMIT ?4 OFFSET ?5",
                Sample.class
        );
        query.setParameter(1, measureId);
        query.setParameter(2, Timestamp.valueOf(start));
        query.setParameter(3, Timestamp.valueOf(end));
        query.setParameter(4, limit);
        query.setParameter(5, offset);

        return query.getResultList();
    }

    /**
     * Retourne le nombre d'échantillons pour une mesure
     *
     * @param measureId l'identifiant de la mesure pour laquelle compter le nombre d'échantillons
     * @return Optional<Long> empty si l'identifiant ne correspond à aucune mesure
     */
    public Optional<Long> countByMeasure(long measureId) {
        if (entityManager.find(Measure.class, measureId) == null) {
            return Optional.empty();
        }
        Query query = entityManager.createNativeQuery(
                "SELECT COUNT(s.id) FROM samples s JOIN experiments e ON s.experiment_id = e.id JOIN measures m on m.experiment_id = e.id  AND s.experiment_id = m.experiment_id WHERE m.id= ?1"
        );
        query.setParameter(1, measureId);
        return Optional.of(((BigInteger) query.getSingleResult()).longValue());
    }
}
