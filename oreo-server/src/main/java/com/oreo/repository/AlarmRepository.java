package com.oreo.repository;

import com.oreo.entity.Alarm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Repository des alarmes
 */
@RepositoryRestResource(collectionResourceRel = "alarms", path = "alarms")
public interface AlarmRepository extends CrudRepository<Alarm, Long> {
    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void delete(Alarm alarm);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void delete(Long alarm);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    <S extends Alarm> S save(S entity);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    <S extends Alarm> Iterable<S> save(Iterable<S> entity);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    Iterable<Alarm> findAll();

    /**
     * Renvoie la liste des alarmes d'un essai.
     *
     * @param experimentId l'id de l'essai
     * @return les alarmes de l'essai
     */
    @Query("from Alarm a where a.experimentId=:experimentId")
    List<Alarm> findByExperimentId(@Param("experimentId") long experimentId);

    /**
     * Renvoie la liste des alarmes d'un essai comprises entre deux dates
     *
     * @param experimentId l'identifiant de l'essai pour lequel rechercher les alarmes
     * @param start        la date à partir de laquelle récupérer les alarmes
     * @param end          la date jusqu'à laquelle récupérer les alarmes
     * @param pageable
     * @return la liste des alarmes de l'essai comprises entre les deux dates
     */
    @Query("from Alarm a where a.experimentId=:experimentId AND a.time BETWEEN :start AND :end ORDER BY a.time ASC")
    Page<Alarm> findByExperimentIdAndDate(@Param("experimentId") long experimentId, @Param("start") LocalDateTime start,
                                          @Param("end") LocalDateTime end, @Param("pageable") Pageable pageable);
}
