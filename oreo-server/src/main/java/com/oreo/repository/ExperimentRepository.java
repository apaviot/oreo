package com.oreo.repository;

import com.oreo.entity.Experiment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import springfox.documentation.annotations.ApiIgnore;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Repository des essais
 */
@RepositoryRestResource(collectionResourceRel = "experiments", path = "experiments")
public interface ExperimentRepository extends PagingAndSortingRepository<Experiment, Long>, JpaSpecificationExecutor<Experiment> {
    @Override
    void delete(Experiment bench);

    @Override
    void delete(Long bench);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    <S extends Experiment> S save(S entity);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    <S extends Experiment> Iterable<S> save(Iterable<S> entity);

    /**
     * Renvoie la liste des essais appartenant à une campagne de tests.
     *
     * @param campaignId l'identifiant de la campagne de tests
     * @return les essais de la campagne de test
     */
    @Query("from Experiment e where e.campaignId=:campaignId")
    List<Experiment> findByCampaignId(@Param("campaignId") long campaignId);

    /**
     * Renvoie la liste des essais effectués sur un banc de tests.
     *
     * @param benchId l'identifiant du banc de test
     * @return les essais effectués sur le banc de test
     */
    @Query("from Experiment e where e.benchId=:benchId")
    List<Experiment> findByBenchId(@Param("benchId") long benchId);

    /**
     * Renvoie la liste des essais dont le nom contient la chaîne de caractères spécifiée.
     *
     * @param name la chaîne de caractères à chercher dans le nom
     * @return les essais dont le nom contient la chaîne de caractères
     */
    @Query("from Experiment e where e.name LIKE %:name%")
    List<Experiment> findByFuzzyName(@Param("name") String name);

    /**
     * Recherche des essais selon plusieurs critères de selections.
     *
     * La recherche sur le nom de l'essai est une recherche floue, c'est-à-dire que le filtre vérifie si la chaîne est incluse
     * dans le nom de l'essai. ex: un essai dont le nom est AZERTY  est renvoyé pour une recherche sur le nom "ERT"
     *
     * @param begin date de début de l'essai
     * @param database base local ou distante. 'remote' ou 'local'
     * @param campaignId identifiant d'une campagne d'essai
     * @param benchId identifiant d'un banc d'essai
     * @param name le nom de l'essai.
     * @param pageable
     * @return les essais répondant aux critères de sélection
     */
    default Page<Experiment> advancedSearch(
            LocalDateTime begin,
            String database,
            Long campaignId,
            Long benchId,
            String name,
            Pageable pageable
    ) {
        return findAll((root, criteria, builder) -> {
            ArrayList<Predicate> predicates = new ArrayList<>();
            if (begin != null) {
                LocalDateTime dayStart = begin.truncatedTo(ChronoUnit.DAYS);
                LocalDateTime dayEnd = dayStart.plus(1, ChronoUnit.DAYS).minus(1, ChronoUnit.NANOS);
                predicates.add(builder.between(root.get("begin"), dayStart, dayEnd));
            }
            if (database != null) {
                switch (database.toLowerCase()) {
                    case "local":
                        predicates.add(builder.equal(root.get("isLocal"), true));
                        break;
                    case "remote":
                        predicates.add(builder.equal(root.get("isLocal"), false));
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown database. Possible values are 'local' and 'remote': " + database);
                }
            }
            if (campaignId != null) {
                predicates.add(builder.equal(root.get("campaignId"), campaignId));
            }
            if (benchId != null) {
                predicates.add(builder.equal(root.get("benchId"), benchId));
            }
            if (name != null) {
                predicates.add(builder.like(root.get("name"), "%" + name + "%"));
            }
            if (!predicates.isEmpty()) {
                return builder.and(predicates.toArray(new Predicate[0]));
            }
            return builder.conjunction();
        }, pageable);
    }
}
