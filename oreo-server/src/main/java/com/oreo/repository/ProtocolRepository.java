package com.oreo.repository;

import com.oreo.entity.Protocol;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Repository des protocols
 */
@RepositoryRestResource(collectionResourceRel = "protocols", path = "protocols")
public interface ProtocolRepository extends PagingAndSortingRepository<Protocol, String> {
    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void delete(Protocol protocol);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void delete(String protocol);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    <S extends Protocol> Iterable<S> save(Iterable<S> entity);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    Protocol findOne(String s);

    @Override
    @ApiIgnore
    @RestResource(exported = false)
    <S extends Protocol> S save(S s);
}
